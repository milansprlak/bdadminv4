<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id')->unsigned();
            $table->integer('contracts_type_id')->unsigned();
            $table->integer('defined_task_id')->unsigned();
            $table->integer('subdefined_task_id')->unsigned()->nullable();
            $table->integer('employy_id')->unsigned();
            $table->integer('pocet_ks')->unsigned()->nullable();
            $table->integer('trvanie_hod')->unsigned();
            $table->integer('trvanie_min')->unsigned();
            $table->decimal('pocet_ks_material',8,2)->unsigned()->nullable();
            $table->decimal('cena_cinnost_hod',8,2)->unsigned()->nullable();
            $table->decimal('cena_material_kg',8,2)->unsigned()->nullable();
            $table->decimal('celkova_cena_cinnost',8,2)->unsigned()->nullable();
            $table->decimal('celkova_cena_material',8,2)->unsigned()->nullable();
            $table->decimal('celkova_cena',10,2)->unsigned()->nullable();
            $table->integer('stav')->unsigned()->nullable();
            $table->decimal('hodnota_tasku',10,2)->nullable();
            $table->string('poznamka')->nullable();
            $table->string('datum');

            $table->timestamps();


            $table->foreign('contract_id')
                ->references('id')
                ->on('contracts')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('defined_task_id')
                ->references('id')
                ->on('defined_tasks')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('employy_id')
                ->references('id')
                ->on('employees')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('contracts_type_id')
                ->references('id')
                ->on('contracts_types')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->index(['created_at']);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
