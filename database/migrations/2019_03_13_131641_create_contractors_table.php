<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nazov_firmy');
            $table->string('kontaktna_osoba')->nullable();
            $table->string('email')->nullable();
            $table->string('telefon')->nullable();
            $table->string('mobil')->nullable();
            $table->string('ico')->nullable();
            $table->string('dic')->nullable();
            $table->string('icdph')->nullable();
            $table->string('fa_adresa')->nullable();
            $table->string('fa_mesto')->nullable();
            $table->string('fa_psc')->nullable();
            $table->string('fa_krajina')->nullable();
            $table->string('www_site')->nullable();
            $table->string('nazov_banky')->nullable();
            $table->string('iban')->nullable();
            $table->string('swift')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractors');
    }
}
