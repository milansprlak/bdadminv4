<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('download_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nazov')->nullable();
            $table->string('url')->nullable();
            $table->integer('contract_id')->unsigned();
            $table->integer('employy_id')->unsigned()->nullable();
            $table->timestamps();


            $table->foreign('contract_id')
                ->references('id')
                ->on('contracts')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('employy_id')
                ->references('id')
                ->on('employees')
                ->onDelete('restrict')
                ->onUpdate('cascade');





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('download_files');
    }
}
