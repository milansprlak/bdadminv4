<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('mod_1')->unsigned()->nullable();
            $table->integer('mod_2')->unsigned()->nullable();
            $table->integer('mod_3')->unsigned()->nullable();
            $table->integer('mod_4')->unsigned()->nullable();
            $table->integer('mod_5')->unsigned()->nullable();
            $table->integer('mod_6')->unsigned()->nullable();
            $table->integer('mod_7')->unsigned()->nullable();
            $table->integer('mod_8')->unsigned()->nullable();
            $table->integer('mod_9')->unsigned()->nullable();
            $table->integer('mod_10')->unsigned()->nullable();
            $table->integer('mod_11')->unsigned()->nullable();
            $table->integer('mod_12')->unsigned()->nullable();
            $table->integer('mod_13')->unsigned()->nullable();
            $table->integer('mod_14')->unsigned()->nullable();
            $table->integer('mod_15')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
