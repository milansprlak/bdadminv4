<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->string('popis')->nullable();
            $table->integer('client_id')->unsigned();
            $table->integer('contracts_type_id')->unsigned();
            $table->integer('pocet_ks')->unsigned();
            $table->integer('nadradena_id')->unsigned()->nullable();
            $table->string('poradove_cislo')->nullable();
            $table->string('cislo_objednavky')->nullable();
            $table->string('cislo_faktury')->nullable();
            $table->integer('zodpovedna_osoba_id')->unsigned();
            $table->string('odhadovany_cas')->nullable();
            $table->integer('podiel_zodp_osoba_perc')->unsigned()->nullable();
            $table->integer('stav_zakazky')->unsigned()->nullable();
            $table->string('hodnota_zakazky_za_ks')->nullable();
            $table->integer('user_id')->unsigned();
            $table->date('datumodovzdania')->nullable();
            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')
                ->on('stocks')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('contracts_type_id')
                ->references('id')
                ->on('contracts_types')
                ->onDelete('restrict')
                ->onUpdate('cascade');


            $table->foreign('zodpovedna_osoba_id')
                ->references('id')
                ->on('employees')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('stav_zakazky')
                ->references('id')
                ->on('contracts_statuses')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->index(['created_at', 'updated_at']);



        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
