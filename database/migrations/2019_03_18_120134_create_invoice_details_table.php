<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned();
            $table->integer('stock_id')->unsigned()->nullable();
            $table->string('nazov_polozky');
            $table->decimal('pocet_ks',10,2)->nullable();
            $table->decimal('cena_bez_dph_ks',10,2)->nullable();
            $table->decimal('celkova_cena_bez_dph',10,2)->nullable();
            $table->decimal('celkova_cena',10,2)->nullable();
            $table->decimal('ciastka_dph',10,2)->nullable();
            $table->integer('vyska_dph')->unsigned()->nullable();
            $table->string('jednotka')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('invoice_id')
                ->references('id')
                ->on('invoices')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('stock_id')
                ->references('id')
                ->on('stocks')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_details');
    }
}
