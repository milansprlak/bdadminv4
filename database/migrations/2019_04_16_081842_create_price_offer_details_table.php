<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceOfferDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_offer_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price_offer_id')->unsigned();
            $table->string('nazov_polozky');
            $table->string('jednotka');
            $table->decimal('pocet_ks',10,2)->nullable();
            $table->decimal('cena_bez_dph_ks',10,2)->nullable();
            $table->decimal('celkova_cena_bez_dph',10,2)->nullable();
            $table->decimal('celkova_cena',10,2)->nullable();
            $table->decimal('ciastka_dph',10,2)->nullable();
            $table->integer('vyska_dph')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('price_offer_id')
                ->references('id')
                ->on('price_offers')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_offer_details');
    }
}
