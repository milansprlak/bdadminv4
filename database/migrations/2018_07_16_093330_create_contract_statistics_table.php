<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id')->unsigned()->nullable();
            $table->integer('contracts_type_id')->unsigned();
            $table->integer('zodpovedna_osoba_id')->unsigned();
            $table->integer('client_id')->unsigned()->nullable();
            $table->integer('pocet_ks')->unsigned()->nullable();
            $table->integer('podiel_zodp_osoba_perc')->unsigned()->nullable();
            $table->decimal('podiel_zodp_osoba_hodnota',15,2)->nullable();
            $table->decimal('hodnota_zakazky_ks',15,2)->nullable();
            $table->integer('celkovy_cas')->nullable();
            $table->decimal('celkova_cena_cinnost',15,2)->nullable();
            $table->decimal('celkova_cena_material',15,2)->nullable();
            $table->decimal('celkova_vyrobna_cena',15,2)->nullable();
            $table->decimal('celkova_predajna_cena_ks',15,2)->nullable();
            $table->decimal('celkova_predajna_cena',15,2)->nullable();
            $table->decimal('zisk',15,2)->nullable();
            $table->decimal('zisk_firmu',15,2)->nullable();
            $table->decimal('zisk_zamestnanci',15,2)->nullable();
            $table->decimal('podiel_zamestnanci',15,2)->nullable();
            $table->timestamps();

            $table->foreign('contract_id')
                ->references('id')
                ->on('contracts')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('contracts_type_id')
                ->references('id')
                ->on('contracts_types')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('zodpovedna_osoba_id')
                ->references('id')
                ->on('employees')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->index(['created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_statistics');
    }
}
