<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contractor_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('odoslane')->unsigned();
            $table->string('cislo')->nullable();
            $table->string('email')->nullable();
            $table->string('poznamka')->nullable();
            $table->date('datum_vystavenia')->nullable();
            $table->date('datum_splatnosti')->nullable();
            $table->string('filesurl')->nullable();
            $table->decimal('cena_bez_dph',10,2)->nullable();
            $table->decimal('cena_s_dph',10,2)->nullable();
            $table->decimal('ciastka_dph',10,2)->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('contractor_id')
                ->references('id')
                ->on('contractors')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_offers');
    }
}
