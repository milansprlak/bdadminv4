<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubdefinedTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdefined_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nazov')->nullable();
            $table->integer('nadradena_kat_id')->unsigned();
            $table->decimal('cena_material_kg',8,2)->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('nadradena_kat_id')
                ->references('id')
                ->on('defined_tasks')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subdefined_tasks');
    }
}
