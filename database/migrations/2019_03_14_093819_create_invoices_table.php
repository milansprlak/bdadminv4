<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('contractor_id')->unsigned();
            $table->integer('invoice_status_id')->unsigned();
            $table->integer('invoice_payment_id')->unsigned();
            $table->string('poradove_cislo')->nullable();
            $table->string('cislo_objednavky')->nullable();
            $table->string('variabilny_symbol')->nullable();
            $table->string('specificky_symbol')->nullable();
            $table->string('konstantny_symbol')->nullable();
            $table->date('datum_vystavenia')->nullable();
            $table->date('datum_splatnosti')->nullable();
            $table->date('datum_dodania')->nullable();
            $table->decimal('cena_bez_dph',10,2)->nullable();
            $table->decimal('cena_s_dph',10,2)->nullable();
            $table->decimal('ciastka_dph',10,2)->nullable();
            $table->string('poznamka')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('contractor_id')
                ->references('id')
                ->on('contractors')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('invoice_status_id')
                ->references('id')
                ->on('status_invoices')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('invoice_payment_id')
                ->references('id')
                ->on('invoice_payment_methods')
                ->onDelete('restrict')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
