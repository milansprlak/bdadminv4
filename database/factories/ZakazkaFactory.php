<?php

use App\Zakazka;
use Faker\Generator as Faker;

$factory->define(Zakazka::class, function (Faker $faker) {
    return [
        'nazov' => substr($faker->sentence(2), 0, -1),
        'pocet_ks' => str_random(5),
        'poradove_cislo' => str_random(10),
    ];
});
