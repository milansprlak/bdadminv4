<?php
use App\Zakazka;
use App\Polozka;
use Faker\Generator as Faker;

$factory->define(Polozka::class, function (Faker $faker) {
    return [
        'nazov' => substr($faker->sentence(2), 0, -1),
        'autor' => substr($faker->sentence(1), 0, -1),
        'zakazka_id' => function() {
            return factory(Zakazka::class)->create()->id;
        }
    ];
});
