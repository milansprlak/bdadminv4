<?php

use Monolog\Handler\StreamHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single','slack'],
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],
        // Log prihlasenia, zmeny hesla
        'uzivatel' => [
            'driver' => 'single',
            'path' => storage_path('logs/uzivatel.log'),

        ],

        // Log vytvorenia, editacie zakazky
        'contract' => [
            'driver' => 'single',
            'path' => storage_path('logs/contract.log'),

        ],
        // Log vytvorenia, editacie statistiky zakazky
        'contractstatistics' => [
            'driver' => 'single',
            'path' => storage_path('logs/contractstatistics.log'),

        ],

        // Log vytvorenia, editacie pokladne
        'cash' => [
            'driver' => 'single',
            'path' => storage_path('logs/cash.log'),

        ],

        // Log vytvorenia, editacie klientov
        'client' => [
            'driver' => 'single',
            'path' => storage_path('logs/client.log'),

        ],
        // Log vytvorenia, editacie dodavatela
        'contractor' => [
            'driver' => 'single',
            'path' => storage_path('logs/contractor.log'),

        ],
        // Log vytvorenia, editacie stav zakazky
        'contractstatus' => [
            'driver' => 'single',
            'path' => storage_path('logs/contractstatus.log'),

        ],
        // Log vytvorenia, editacie typ zakazky
        'contracttype' => [
            'driver' => 'single',
            'path' => storage_path('logs/contracttype.log'),

        ],
        // Log vytvorenia, editacie definovane ulohy zakazky
        'definedtask' => [
            'driver' => 'single',
            'path' => storage_path('logs/definedtask.log'),

        ],
        // Log vytvorenia, editacie definovane ulohy zakazky
        'subdefinedtask' => [
            'driver' => 'single',
            'path' => storage_path('logs/subdefinedtask.log'),

        ],
        // Log vytvorenia, editacie zaznamy v denniku
        'diary' => [
            'driver' => 'single',
            'path' => storage_path('logs/diary.log'),

        ],
        // Log vytvorenia, editacie upload suborov
        'uploadfiles' => [
            'driver' => 'single',
            'path' => storage_path('logs/uploadfiles.log'),

        ],
        // Log vytvorenia, editacie zamestnancov
        'employees' => [
            'driver' => 'single',
            'path' => storage_path('logs/employees.log'),

        ],
        // Log vytvorenia, editacie faktury
        'invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/invoice.log'),

        ],
        // Log vytvorenia, editacie invoice detail (polozky faktury)
        'invoicedetail' => [
            'driver' => 'single',
            'path' => storage_path('logs/invoicedetail.log'),

        ],
        // Log vytvorenia, editacie skladu
        'stock' => [
            'driver' => 'single',
            'path' => storage_path('logs/stock.log'),

        ],
        // Log vytvorenia, editacie metody platby faktury
        'invoicepayment' => [
            'driver' => 'single',
            'path' => storage_path('logs/invoicepayment.log'),

        ],
        // Log vytvorenia, editacie metody platby faktury
        'reminders' => [
            'driver' => 'single',
            'path' => storage_path('logs/reminders.log'),

        ],
        // Log vytvorenia, editacie metody platby faktury
        'sendstock' => [
            'driver' => 'single',
            'path' => storage_path('logs/sendstock.log'),

        ],
        // Log vytvorenia, editacie metody platby faktury
        'statisticsclient' => [
            'driver' => 'single',
            'path' => storage_path('logs/statisticsclient.log'),

        ],
        // Log vytvorenia, editacie metody platby faktury
        'statisticsemployees' => [
            'driver' => 'single',
            'path' => storage_path('logs/statisticsemployees.log'),

        ],
        // Log vytvorenia, editacie metody platby faktury
        'statisticstasks' => [
            'driver' => 'single',
            'path' => storage_path('logs/statisticstasks.log'),

        ],
        // Log vytvorenia, editacie metody platby faktury
        'invoicestatus' => [
            'driver' => 'single',
            'path' => storage_path('logs/invoicestatus.log'),

        ],
        // Log editovania tasku v zakazke
        'messages' => [
            'driver' => 'single',
            'path' => storage_path('logs/messages.log'),

        ],
        // Log editovania tasku v zakazke
        'priceoffer' => [
            'driver' => 'single',
            'path' => storage_path('logs/priceoffer.log'),

        ],
        // Log vytvorenia, editacie priceoffer detail (polozky cenovej ponuky)
        'priceofferdetail' => [
            'driver' => 'single',
            'path' => storage_path('logs/priceofferdetail.log'),

        ],
        // Log editovania tasku v zakazke
        'taskedit' => [
            'driver' => 'single',
            'path' => storage_path('logs/taskedit.log'),

        ],
        // Log vytvorenia, editacie metody platby faktury
        'users' => [
            'driver' => 'single',
            'path' => storage_path('logs/users.log'),

        ],
        // Log vytvorenia uzivatelov pre klient export
        'client_export' => [
            'driver' => 'single',
            'path' => storage_path('logs/client_export.log'),

        ],
        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 7,
        ],

        'slack' => [
            'driver' => 'slack',
          //  'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'url' => ('https://hooks.slack.com/services/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'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
          //  'level' => 'warning',
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
    ],

];
