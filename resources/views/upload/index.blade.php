@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <a href="/upload/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Nahrať súbory</a>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam súborov</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo</th>
                                    <th>Názov súboru</th>
                                    <th>Zamestnanec</th>
                                    <th>Zákazka</th>
                                    @if((Auth::check())&&((Auth::user()->mod_1)==1))
                                    <th>Odstrániť</th>
                                    @endif



                                </tr>
                                <div style="display: none;">
                                    {{ $i=1 }}
                                </div>

                                @foreach ($download_files as $download_file)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td><a href="{{ $download_file->url }}">  {{ $download_file->nazov }}</a></td>
                                        <td>{{ $download_file->employy->meno}}</td>
                                        <td><a href="contracts/{{ $download_file->contract->id }}">  {{ $download_file->contract->nazov }}</a></td>
                                        @if((Auth::check())&&((Auth::user()->mod_1)==1))
                                        <td><a href="#"  onclick="
                                                    var result = confirm('Skutočne vymazať súbor?');
                                                    if( result ){
                                                    event.preventDefault();

                                                    document.getElementById('delete-form-{{$download_file->id}}').submit();
                                                    }
                                                    ">Odstrániť</a>
                                            <form id="delete-form-{{$download_file->id}}" action="{{ route('upload.destroy',[$download_file->id])}}"
                                                  method="POST" style="display: none;">
                                                <input type="hidden" name="_method" value="delete">
                                                {{ csrf_field() }}
                                            </form>

                                        </td>
                                        @endif



                                    </tr>
                                @endforeach

                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection