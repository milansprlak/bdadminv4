@extends('admin_template')

@section('content')
    <div class="row">

        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">

            <!-- general form elements disabled -->
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Nahrávanie súborov</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body">
                    @if ($message = Session::get('success'))

                        <div class="alert alert-success alert-block">

                            <button type="button" class="close" data-dismiss="alert">×</button>

                            <strong>{{ $message }}</strong>

                        </div>

                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                @endif
                <!-- form start -->
                <form action="{{ route('upload.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label>Vyberte zákazku:</label>
                            <select name="zakazka_id" class="form-control">
                                @foreach ($contract as $contract2)
                                    <option value="{{$contract2->id}}">{{$contract2->nazov}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Vyberte zamestnanca:</label>
                            <select name="zamestnanec_id" class="form-control">
                                @foreach($employy as $employy2)
                                    <option value="{{$employy2->id}}">{{$employy2->meno}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="file" class="form-control-file" name="fileToUpload" id="exampleInputFile" aria-describedby="fileHelp">
                            <small id="fileHelp" class="form-text text-muted">Odovzdajte platný súbor. Veľkosť by nemala byť väčšia ako 90 MB.</small>
                        </div>


                    </div>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.tab-pane -->
    </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>



@endsection
