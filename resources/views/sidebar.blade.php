
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("bower_components/admin-lte/dist/img/avatar.png") }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>



        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="header">Administrácia</li>
            <!-- Optionally, you can add icons to the links -->
            @if (Auth::user()->mod_1 ==1)
            <li class="{{Request::is('contracts') ? 'active' : ''}}"><a href="/contracts/"><i class="fa fa-link"></i> <span>Zákazky</span></a></li>
            @endif
            @if (Auth::user()->mod_2 ==1)
            <li class="{{Request::is('deadline') ? 'active' : ''}}"><a href="/deadline/"><i class="fa fa-link"></i> <span>Zákazky na odovzdanie</span></a></li>
            @endif
            @if (Auth::user()->mod_10 ==1)
            <li class="{{Request::is('invoice') ? 'active' : ''}}"><a href="/invoice/"><i class="fa fa-link"></i> <span>Faktúry</span></a></li>
            @endif

            <li class="treeview {{( Request::is('contracts/create') || Request::is('invoice/create') || Request::is('price_offers')  ) ? 'active' : ''}}" >
                <a href="#"><i class="fa fa-link"></i> <span>Vytvoriť</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    @if (Auth::user()->mod_1 ==1)
                    <li class="{{Request::is('contracts/create') ? 'active' : ''}}"><a href="/contracts/create/">Vytvoriť zákazku</a></li>
                    @endif
                    @if (Auth::user()->mod_10 ==1)
                    <li class="{{Request::is('invoice/create') ? 'active' : ''}}"><a href="/invoice/create/">Vytvoriť faktúru</a></li>
                    <li class="{{Request::is('price_offers') ? 'active' : ''}}"><a href="/price_offers/">Cenové ponuky</a></li>
                    @endif
                </ul>
            </li>
            @if (Auth::user()->mod_10 ==1 ||  Auth::user()->mod_1 ==1 )
            <li class="{{Request::is('stock') ? 'active' : ''}}"><a href="/stock/"><i class="fa fa-link"></i> <span>Sklad</span></a></li>
            @endif


            <li class="treeview {{(Request::is('statisticsinvoice') ||  Request::is('statistics') || Request::is('statisticsclient') || Request::is('taskstatistics')  ) ? 'active' : ''}}">
                <a href="#"><i class="fa fa-link"></i> <span>Štatistiky</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    @if (Auth::user()->mod_10 ==1)
                    <li class="{{Request::is('statisticsinvoice') ? 'active' : ''}}"><a href="/statisticsinvoice/">Štatistika faktúr</a></li>
                    @endif
                    @if (Auth::user()->mod_1 ==1)
                    <li class="{{Request::is('statistics') ? 'active' : ''}}"><a href="/statistics/">Štatistiky zamestnancov</a></li>
                    @endif
                    @if (Auth::user()->mod_4 ==1)
                    <li class="{{Request::is('statisticsclient') ? 'active' : ''}}"><a href="/statisticsclient/">Štatistiky klientov</a></li>
                    @endif
                    @if (Auth::user()->mod_4 ==1)
                    <li class="{{Request::is('taskstatistics') ? 'active' : ''}}"><a href="/taskstatistics/">Štatistiky úloh</a></li>
                    @endif
                </ul>
            </li>



            @if (Auth::user()->mod_8 ==1)
            <li class="{{Request::is('diary') ? 'active' : ''}}"><a href="/diary/"><i class="fa fa-link"></i> <span>Denník</span></a></li>
            @endif
            @if (Auth::user()->mod_6 ==1)
            <li class="{{Request::is('cash') ? 'active' : ''}}"><a href="/cash/"><i class="fa fa-link"></i> <span>Pokladňa</span></a></li>
            @endif
            @if (Auth::user()->mod_7 ==1)
            <li class="treeview {{(Request::is('reminders') || Request::is('reminders/create') || Request::is('reminders/all')) ? 'active' : ''}}">
                <a href="#"><i class="fa fa-link"></i> <span>Pripomienky</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{Request::is('reminders') ? 'active' : ''}}"><a href="/reminders/">Zobraziť aktuáne pripomienky</a></li>
                    <li class="{{Request::is('reminders/create') ? 'active' : ''}}"><a href="/reminders/create/">Pridať pripomienku</a></li>
                    <li class="{{Request::is('reminders/all') ? 'active' : ''}}"><a href="/reminders/all/">Zobraziť všetky pripomienky</a></li>
                </ul>
            </li>
            @endif
            @if (Auth::user()->mod_9 ==1)
            <li class="{{Request::is('messages') ? 'active' : ''}}"><a href="/messages/"><i class="fa fa-link"></i> <span>Správy</span></a></li>
            @endif


            <li class="treeview {{(Request::is('contracts_statuses') || Request::is('employees')  || Request::is('clients') ||  Request::is('contracts_types') || Request::is('invoice_payment') || Request::is('status_invoice') || Request::is('definedtasks') || Request::is('subdefinedtasks')|| Request::is('upload')|| Request::is('users')|| Request::is('client_export')  ) ? 'active' : ''}}" >
                <a href="#"><i class="fa fa-link"></i> <span>Nastavenia</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    @if (Auth::user()->mod_1 ==1)
                    <li class="{{Request::is('clients') ? 'active' : ''}}"><a href="/clients/">Odberatelia</a></li>
                    <li class="{{Request::is('employees') ? 'active' : ''}}"><a href="/employees/">Zamestnanci</a></li>
                    <li class="{{Request::is('definedtasks') ? 'active' : ''}}"><a href="/definedtasks/">Druh práce </a></li>
                    <li class="{{Request::is('subdefinedtasks') ? 'active' : ''}}"><a href="/subdefinedtasks/">Materiál</a></li>
                    <li class="{{Request::is('upload') ? 'active' : ''}}"><a href="/upload/">Výrobky</a></li>
                    <li class="{{Request::is('contracts_statuses') ? 'active' : ''}}"><a href="/contracts_statuses/">Manažér stavu zákazky</a></li>
                    <li class="{{Request::is('contracts_types') ? 'active' : ''}}"><a href="/contracts_types/">Manažér typu zákazky</a></li>
                    @endif
                    @if (Auth::user()->mod_10 ==1)
                    <li class="{{Request::is('invoice_payment') ? 'active' : ''}}"><a href="/invoice_payment/">Manažér metódy platby</a></li>
                    <li class="{{Request::is('status_invoice') ? 'active' : ''}}"><a href="/status_invoice/">Manažér stavu faktúr</a></li>
                    @endif
                    @if (Auth::user()->mod_11 ==1)
                    <li class="{{Request::is('users') ? 'active' : ''}}"><a href="/users/">Uživatelia</a></li>
                    <li class="{{Request::is('client_export') ? 'active' : ''}}"><a href="/client_export/">Export skladu</a></li>
                    @endif

                </ul>
            </li>



        </ul>

    </section>
    <!-- /.sidebar -->
</aside>