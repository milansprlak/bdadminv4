<!-- Main Header -->
<header class="main-header">
    <!-- Nastavenie upozornenia -->
    <script>
        function blinker() {
            $('.blink_me').fadeOut(500);
            $('.blink_me').fadeIn(500);
        }
        setInterval(blinker, 1000); //Runs every second
    </script>

    <!-- Logo -->
    <a href="{{ asset("/") }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>BdA</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>BdAdmin</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">

    @if (Auth::check())

        <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
    @endif
    <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- Message menu -->
                    <li class="dropdown messages-menu">
                        @if ($messages_pocetnost > 0)
                        <a href="#" class="dropdown-toggle blink_me" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">{{$messages_pocetnost}}</span>
                        </a>
                        @else
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope-o"></i>
                                <span class="label label-success">{{$messages_pocetnost}}</span>
                            </a>
                        @endif
                        <ul class="dropdown-menu">
                            <li class="header">Máte {{$messages_pocetnost}} aktívnych správ</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    @if (!empty($pubmessages ))
                                    @foreach($pubmessages as $message )
                                    <li><!-- start message -->
                                        <a href="/messages/">
                                            <div class="pull-left">
                                                <img src="/foto/avatar.png" class="img-circle" alt="User Image">
                                            </div>
                                            <h4>
                                               Správa:
                                                <small><i class="fa fa-clock-o"></i>{{$message->updated_at->format('d.m.Y H:i')}}</small>
                                            </h4>
                                            <p>{{$message->sprava}}</p>
                                        </a>
                                    </li>
                                    <!-- end message -->
                                    @endforeach
                                    @endif

                                </ul>
                            </li>
                            <li class="footer"><a href="/messages/create/">Napísať novú správu</a></li>
                        </ul>
                    </li>

                    <!-- End message menu -->
                @if ((Auth::check())&&((Auth::user()->mod_7)==1))
                    <!-- Notifications Menu -->

                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        @if ($pocet > 0)
                        <a href="#" class="dropdown-toggle blink_me" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">{{$pocet}}</span>
                        </a>
                        @else
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning">{{$pocet}}</span>
                            </a>
                        @endif
                        <ul class="dropdown-menu">
                            <li class="header">Máte {{$pocet}} pripomienky</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    @if (!empty($remindersh ))
                                        @foreach ($remindersh as $reminder1)
                                            <li><!-- start notification -->
                                                <a href="/reminders">
                                                    <i class="fa fa-tasks text-aqua"></i>
                                                {{$reminder1['pripomienka']}}
                                                {{($reminder1['datum'] ? date('d.m.Y', strtotime($reminder1['datum'])): '')}}

                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                        @if (!empty($reminders_reph ))
                                            @foreach ($reminders_reph as $reminder22)
                                                <li><!-- start notification -->
                                                    <a href="/reminders">
                                                        <i class="fa fa-tasks text-aqua"></i>
                                                        {{$reminder22['pripomienka']}}
                                                        {{($reminder22['datumnew'] ? date('d.m.Y', strtotime($reminder22['datumnew'])): '')}}

                                                    </a>
                                                </li>
                                            @endforeach
                                        @endif
                                </ul>
                            </li>
                            <li class="footer"><a href="/reminders/all">Zobraziť všetky</a></li>
                        </ul>
                    </li>
            @endif

            <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{ asset("bower_components/admin-lte/dist/img/avatar.png") }}" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        @if (Auth::check())
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        @else
                            <span class="hidden-xs">Prihlásiť sa</span>
                        @endif
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->


                        @if (Auth::check())
                            <li class="user-header">
                                <img src="{{ asset("bower_components/admin-lte/dist/img/avatar.png") }}" class="img-circle" alt="User Image">

                                <p>
                                    {{ Auth::user()->name }}
                                    <small>BEDRICH spol. s r.o.</small>
                                </p>
                            </li>


                            <!-- Menu Footer-->
                            <li class="user-footer">

                                     <a class="btn btn-outline-primary btn-lg" href="{{ route('changePassword') }}"
                                      >
                                     Zmeniť heslo
                                     </a>
                                    <a class="btn btn-outline-primary btn-lg" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Odhlásiť sa') }}
                                    </a>


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                            </li>

                        @else
                            <li class="user-header">
                                <img src="{{ asset("bower_components/admin-lte/dist/img/avatar.png") }}" class="img-circle" alt="User Image">
                                <p>
                                    Neprihlásený
                                    <small>BEDRICH spol. s r.o.</small>
                                </p>
                            </li>

                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-sm-12 col-form-label text-md-right">E-mailová adresa</label>

                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-12 col-form-label text-md-right">Heslo</label>

                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif


                                        <div class="form-group row">
                                            <div class="col-md-6 offset-md-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Zapamätať si ma') }}
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <button type="submit" class="btn btn-default btn-flat">
                                                    {{ __('Prihlásiť sa') }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
