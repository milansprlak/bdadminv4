@extends('admin_template')
<head>
    <link href="/css/select2.min.css" rel="stylesheet" />
    <script src="/js/jquery.js"></script>
</head>

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Upraviť produkt</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('stock.update',[$product->id])}}" role="form">
                          {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Názov produktu</label>
                                    <input name="nazov_produktu" value="{{$product->nazov_produktu}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte názov produktu">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Počet ks na sklade</label>
                                    <input name="pocet_ks" value="{{$product->pocet_ks}}" type="number" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte počet ks na sklade">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Minimálne množstvo na sklade</label>
                                    <input name="min_mnozstvo"  type="number" min="0" value="{{$product->min_mnozstvo}}" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte počet ks na sklade">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Výška DPH</label>
                                    <input name="dph" value="{{$product->dph}}" type="number" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte výšku DPH">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Cena bez DPH</label>
                                    <input name="cena__bez_dph" value="{{$product->cena__bez_dph}}" type="number" step="0.01" min="0" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte cenu bez DPH">
                                </div>
                                <div class="form-group">
                                    <label>Vyberte klienta:</label>
                                    <select name="client_id" id="client_id" required class="form-control">
                                        @foreach ($clients as $client)
                                            @if($client->id == $product->client_id)
                                                <option value="{{$client->id}}" selected>{{$client->nazov_firmy}}</option>
                                            @else
                                                <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
            <script src="/js/select2.min.js"></script>
            <script type="text/javascript">

                $("#client_id").select2({
                    placeholder: "Vyberte klienta",
                    allowClear: true
                });
            </script>

@endsection
