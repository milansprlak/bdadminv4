@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <a href="/stock/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať produkt do skladu</a>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Sklad produktov</h3>
                    <h3 style="color: red" class="box-title pull-right">Celková hodnota skladu: {{number_format($hodnota_skladu,2,',',' ')}} €   </h3>

                </div>

                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('stock.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class="box-body">

                                    <div class="form-group col-md-3">
                                        <label>Klient:</label>
                                        <select id="klient" name="klient"  class="form-control">
                                            <option value="0">Všetky</option>
                                            @foreach($clients as $client)

                                                @if($client->id == $value_klient)
                                                    <option value="{{$client->id}}" selected>{{$client->nazov_firmy}}</option>
                                                @else
                                                    <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                                @endif
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Zobraziť len produkty s malým množstvom:</label>
                                        <select id="malemnozstvo" name="malemnozstvo"  class="form-control">
                                            @if($value_malemnozstvo == 1)
                                            <option value="0">Nie</option>
                                            <option value="1" selected>Áno</option>
                                            @else
                                                <option value="0" selected>Nie</option>
                                                <option value="1">Áno</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <br>
                                        <button type="submit" class="btn btn-primary col-md-3">Zobraziť</button>
                                    </div>
                                </div>

                                <!-- /.box-body -->
                            </form>

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo</th>
                                    <th>Názov produktu</th>
                                    <th>Počet ks na sklade</th>
                                    <th>Min. množstvo</th>
                                    <th>DPH</th>
                                    <th>Cena bez DPH</th>
                                    <th>Klient</th>
                                    <th>Editácia</th>



                                </tr>
                                <div style="display: none;">
                                    {{ $i=1 }}
                                </div>

                                @foreach ($products as $product)
                                    @if ($product->min_mnozstvo < $product->pocet_ks )
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $product->nazov_produktu}}</td>
                                        <td>{{ $product->pocet_ks}}</td>
                                        <td>{{ $product->min_mnozstvo}}</td>
                                        <td>{{ $product->dph}}</td>
                                        <td>{{ $product->cena__bez_dph}}</td>
                                        <td>{{ $product->klient->nazov_firmy}}</td>
                                        <td><a href="/stock/{{ $product->id }}/edit">Editovať</a></td>
                                    </tr>
                                    @else
                                        <tr>
                                            <td><span style="color: red">{{ $i++ }}</span></td>
                                            <td><span style="color: red">{{ $product->nazov_produktu}}</span></td>
                                            <td><span style="color: red">{{ $product->pocet_ks}}</span></td>
                                            <td><span style="color: red">{{ $product->min_mnozstvo}}</span></td>
                                            <td><span style="color: red">{{ $product->dph}}</span></td>
                                            <td><span style="color: red">{{ $product->cena__bez_dph}}</span></td>
                                            <td><span style="color: red">{{ $product->klient->nazov_firmy}}</span></td>
                                            <td><a href="/stock/{{ $product->id }}/edit">Editovať</a></td>
                                        </tr>
                                    @endif
                                @endforeach

                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection