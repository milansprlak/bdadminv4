<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        div.a {
            width: auto;
            border: 1px solid black;
            padding: 5px;
        }

        body { font-family: DejaVu Sans, sans-serif; }
        #page_break{
            page-break-before:always;
        }
    </style>

</head>
    <div class='row' style="width: auto; border: 1px solid black; padding: 5px;">
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">

                <div class="box-body">

                    <div class="box">
                        <div style="display: none;">{{$stavzakazky=1}}</div>


                        <h2>    {{ $contract->popis }}</h2>

                        <table id="infoozakazke" class="table table-bordered table-striped">
                            <tr>
                                <th>Typ zákazky</th>
                                <th>Zodpovedný vedúci</th>
                                <th>Stav zákazky</th>
                                <th>Klient</th>
                                <th>Poradové číslo</th>
                                <th>Číslo objednávky</th>
                                <th>Termín odovzdania</th>
                                <th>Číslo faktúry</th>
                                <th>Počet ks</th>
                                <th>Odhadovaný čas</th>

                            </tr>

                            <tr>
                                <td>{{$contract->typzakazky->nazov_typu}}</td>
                                <td>{{$contract->zodpovednaosoba->meno}}</td>
                                <td>{{$contract->stavzakazky->nazov_stavu}}</td>
                                <td>{{$contract->klientzakazky->nazov_firmy}}</td>
                                <td>{{ $contract->poradove_cislo}}</td>
                                <td>{{ $contract->cislo_objednavky}}</td>
                                <td>{{($contract->datumodovzdania ? date('d.m.Y', strtotime($contract->datumodovzdania)): '')}}</td>
                                <td>{{ $contract->cislo_faktury}}</td>
                                <td>{{ $contract->pocet_ks }}</td>
                                <td>{{ $contract->odhadovany_cas }}</td>

                            </tr>
                        </table>
                        @if (($stavzakazky==1)||(Auth::check()))
                            <h3>Zoznam činnosti</h3>
                            <div style="display: none;">{{$suma_pocet_ks=0, $suma_pocet_ks_material=0,$hodnotarezijna=0, $suma_hod=0, $suma_min=0, $celkovacenacinnosti=0, $celkovacenameterialu=0,$celkovacena=0 }}</div>
                            @if ($contract->contracts_type_id == 1)

                                <table id="example1" class="table table-bordered table-striped">

                                    <tr>
                                        <th style="width: auto;">Dátum</th>
                                        <th style="width: auto;">Zamestnanec</th>
                                        <th>Názov</th>
                                        <th>Poznámka</th>
                                        <th>Počet ks</th>
                                        <th>Hmotnosť materiálu (kg)</th>
                                        <th>Trvanie hod</th>
                                        <th>Trvanie min</th>
                                        @if (Auth::check())
                                            <th>Celková cena za činnosť</th>
                                            <th>Celková cena za materiál</th>
                                            <th>Celková cena</th>

                                        @endif


                                    </tr>


                                @foreach ($contract->tasks as $task) <!--// nacita tabulku Task priradenu k contract_id -->

                                    <tr>
                                        <td>{{$task->created_at->format('d.m.Y')}}</td>
                                        <td>{{$task->zamestnanec->meno}}</td>
                                        <td>{{$task->definovanytask->nazov}}</td>
                                        <td>{{$task->poznamka}}</td>
                                        <td>{{$task->pocet_ks, $suma_pocet_ks=$suma_pocet_ks+$task->pocet_ks}}</td>
                                        <td>{{$task->pocet_ks_material, $suma_pocet_ks_material=$suma_pocet_ks_material+$task->pocet_ks_material}}</td>
                                        <td>{{$task->trvanie_hod, $suma_hod=$suma_hod+$task->trvanie_hod}}</td>
                                        <td>{{$task->trvanie_min, $suma_min=$suma_min+$task->trvanie_min}}</td>
                                        @if (Auth::check())
                                            <td>{{$task->celkova_cena_cinnost, $celkovacenacinnosti=$celkovacenacinnosti+$task->celkova_cena_cinnost}} &#8364;</td>
                                            <td>{{$task->celkova_cena_material, $celkovacenameterialu=$celkovacenameterialu+$task->celkova_cena_material}} &#8364;</td>
                                            <td>{{$task->celkova_cena,$celkovacena=$celkovacena+$task->celkova_cena}} &#8364;</td>

                                        @endif

                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="bg-primary"><strong>Spolu:</strong></td>
                                        <td class="bg-primary"></td>
                                        <td class="bg-primary"></td>
                                        <td class="bg-primary"></td>
                                        <td class="bg-primary"><strong>{{number_format($suma_pocet_ks)}}</strong></td>
                                        <td class="bg-primary"><strong>{{number_format($suma_pocet_ks_material,2,',',' ')}}</strong></td>
                                        <div style="display: none;">
                                            @if ($suma_min>59)
                                                {{
                                                $pom_hod =floor($suma_min/60),
                                                $suma_hod = $suma_hod+$pom_hod,
                                                $pom_min = $pom_hod*60,
                                                $suma_min=$suma_min-$pom_min

                                                }}
                                            @endif
                                        </div>
                                        <td class="bg-primary"><strong>{{number_format($suma_hod)}}</strong></td>
                                        <td class="bg-primary"><strong>{{number_format($suma_min)}}</strong></td>
                                        @if (Auth::check())

                                            <td class="bg-primary"><strong>{{number_format($celkovacenacinnosti,2,',',' ')}} &#8364;</strong></td>
                                            <td class="bg-primary"><strong>{{number_format($celkovacenameterialu,2,',',' ')}} &#8364;</strong></td>
                                            <td class="bg-primary"><strong>{{number_format($celkovacena,2,',',' ')}} &#8364;</strong></td>

                                        @endif



                                    </tr>

                                </table>
                            @else
                                <table id="example3" class="table table-bordered table-striped">

                                    <tr>
                                        <th>Dátum</th>
                                        <th>Zamestnanec</th>
                                        <th>Názov</th>
                                        <th>Poznámka</th>

                                        <th>Trvanie hod</th>
                                        <th>Trvanie min</th>
                                        @if (Auth::check())
                                            <th>Režijna cena hodina</th>
                                            <th>Celkova cena:</th>
                                            <th>Hodnota tasku</th>
                                        @endif
                                        <th class="hidden-print">Editácia</th>

                                    </tr>


                                @foreach ($contract->tasks as $task) <!--// nacita tabulku Task priradenu k contract_id -->

                                    <tr>
                                        <td>{{$task->created_at->format('d.m.Y')}}</td>
                                        <td>{{$task->zamestnanec->meno}}</td>
                                        <td>{{$task->definovanytask->nazov}}</td>
                                        <td>{{$task->poznamka}}</td>
                                        <td>{{$task->trvanie_hod, $suma_hod=$suma_hod+$task->trvanie_hod}}</td>
                                        <td>{{$task->trvanie_min, $suma_min=$suma_min+$task->trvanie_min}}</td>
                                        @if (Auth::check())
                                            <td>{{$task->cena_cinnost_hod}} &#8364;</td>
                                            <td>{{$task->celkova_cena,$celkovacena=$celkovacena+$task->celkova_cena}} </td>
                                            <td>{{$task->hodnota_tasku, $hodnotarezijna=$hodnotarezijna+$task->hodnota_tasku}} &#8364;</td>
                                        @endif

                                        @if ((($task->created_at->isToday())&&($stavzakazky==1))||(Auth::check()))
                                            <td class="hidden-print"><a href="/tasks/{{ $task->id }}/edit">Editovať</a></td>
                                        @else
                                            <td></td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="bg-primary"><strong>Spolu:</strong></td>
                                        <td class="bg-primary"></td>
                                        <td class="bg-primary"></td>
                                        <td class="bg-primary"></td>

                                        <div style="display: none;">
                                            @if ($suma_min>59)
                                                {{
                                                $pom_hod =floor($suma_min/60),
                                                $suma_hod = $suma_hod+$pom_hod,
                                                $pom_min = $pom_hod*60,
                                                $suma_min=$suma_min-$pom_min

                                                }}
                                            @endif
                                        </div>
                                        <td class="bg-primary"><strong>{{number_format($suma_hod)}}</strong></td>
                                        <td class="bg-primary"><strong>{{number_format($suma_min)}}</strong></td>
                                        <td class="bg-primary"></td>
                                        @if (Auth::check())
                                            <td class="bg-primary"><strong>{{number_format($celkovacena,2,',',' ')}} &#8364;</strong></td>
                                            <td class="bg-primary"><strong>{{number_format($hodnotarezijna,2,',',' ')}} &#8364;</strong></td>
                                            <td class="bg-primary"></td>
                                        @endif

                                    </tr>



                                </table>

                            @endif

                        @endif

                    </div>
                    <!-- /.box -->
                    @if (Auth::check()&&($contract->stavzakazky->id == 1))
                        <h3>Výrobna Hodnota zákazky (za 1ks): {{number_format($celkovacena/$contract->pocet_ks ,2,',',' ')}} &#8364;</h3>
                        <h3>Hodnota zákazky za ks (predajná cena): {{$contract->hodnota_zakazky_za_ks}} &#8364;</h3>
                    @endif

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->
        @if (Auth::check()&&($contract->stavzakazky->id != 1))
            <div id="druhybox" class='col-md-6'>
                <!-- Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Štatistika zákazky</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <div class="box-body">
                        @foreach($contractstatistics as $contractstatistic)
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Hodnota:</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Celková predajná cena za 1ks:</th>
                                    <td>{{number_format($contractstatistic->celkova_predajna_cena_ks,2,',',' ')}} &#8364;</td>

                                </tr>
                                <tr>
                                    <th scope="row">Počet ks:</th>
                                    <td>{{$contractstatistic->pocet_ks}} ks</td>

                                </tr>
                                <tr>
                                    <th scope="row">Celková predajná cena:</th>
                                    <td>{{number_format($contractstatistic->celkova_predajna_cena,2,',',' ')}} &#8364;</td>

                                </tr>
                                <tr>
                                    <th scope="row">Celková výrobná cena:</th>
                                    <td>{{number_format($contractstatistic->celkova_vyrobna_cena,2,',',' ')}} &#8364;</td>

                                </tr>
                                <tr>
                                    <th scope="row">Celková výrobná cena za ks:</th>
                                    <td>{{number_format($contractstatistic->hodnota_zakazky_ks,2,',',' ')}} &#8364;</td>
                                </tr>
                                <tr>
                                    <th scope="row">Percentuálnu podiel pre zodpovednú osobu:</th>
                                    <td>{{$contractstatistic->podiel_zodp_osoba_perc}} %</td>
                                </tr>
                                <tr>
                                    <th scope="row">Podiel pre zodpovednú osobu:</th>
                                    <td>{{number_format($contractstatistic->podiel_zodp_osoba_hodnota,2,',',' ')}} &#8364;</td>
                                </tr>

                                <tr>
                                    <th scope="row">Celkový zisk (100%):</th>
                                    <td>{{number_format($contractstatistic->zisk,2,',',' ')}} &#8364;</td>

                                </tr>
                                <tr>
                                    <th scope="row">Zisk pre firmu (67%):</th>
                                    <td>{{number_format($contractstatistic->zisk_firmu,2,',',' ')}} &#8364;</td>

                                </tr>
                                <tr>
                                    <th scope="row">Zisk pre zamestnancov (33%):</th>
                                    <td>{{number_format($contractstatistic->zisk_zamestnanci,2,',',' ')}} &#8364;</td>

                                </tr>
                                <tr>
                                    <th scope="row">Podiel zo zisku pre zamestnanca za 1 min.:</th>
                                    <td>{{number_format($contractstatistic->podiel_zamestnanci,2,',',' ')}} &#8364;</td>

                                </tr>
                                <tr>
                                    <th scope="row">Celková cena za činnosť:</th>
                                    <td>{{number_format($contractstatistic->celkova_cena_cinnost,2,',',' ')}} &#8364;</td>

                                </tr>
                                <tr>
                                    <th scope="row">Celková cena za materiál:</th>
                                    <td>{{number_format($contractstatistic->celkova_cena_material,2,',',' ')}} &#8364;</td>

                                </tr>

                                </tbody>
                            </table>
                        @endforeach
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        @endif


        <div id="tretibox" class='col-md-6'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Súbory v zákazke</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>

                    </div>
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">

                        <tr>
                            <th>Poradie</th>
                            <th>Názov súboru</th>

                        </tr>
                        <div style="display: none;">
                            {{ $i=1 }}
                        </div>

                        @foreach($download_files as $download_files2)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td><a href="{{ $download_files2->url }}">  {{ $download_files2->nazov }}</a></td>
                                @if(Auth::user())
                                    <td><a href="#"  onclick="
                                                var result = confirm('Skutočne vymazať súbor?');
                                                if( result ){
                                                event.preventDefault();

                                                document.getElementById('delete-form-{{$download_files2->id}}').submit();
                                                }
                                                ">Odstrániť</a>
                                        <form id="delete-form-{{$download_files2->id}}" action="{{ route('upload.destroy',[$download_files2->id])}}"
                                              method="POST" style="display: none;">
                                            <input type="hidden" name="_method" value="delete">
                                            {{ csrf_field() }}
                                        </form>

                                    </td>
                                @endif


                            </tr>
                        @endforeach




                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
    <style type="text/css" media="print">
        @page { size: landscape; }
    </style>
