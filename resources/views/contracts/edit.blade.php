@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="/css/select2.min.css" rel="stylesheet" />
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>
</head>

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Editovať zákazku</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('contracts.update',[$contract->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <input type="hidden" name="contract_id" value="{{$contract->id}}">
                            <input name="contractstype" type="hidden" value="{{ $contract->contracts_type_id }}" >
                            <div class="box-body">
                                @if (($contract->contracts_type_id!=4)&&($contract->contracts_type_id!=3))
                                @if($overenie == 1)
                                    <div class="form-group">
                                        <label>Vyberte typ zákazky:</label>
                                        <select name="contractstype" class="form-control">
                                            @foreach ($contractstypes as $contractstype)
                                                @if($contractstype->id == $contract->contracts_type_id)
                                                    <option value="{{$contractstype->id}}" selected>{{$contractstype->nazov_typu}}</option>
                                                @else
                                                    <option value="{{$contractstype->id}}">{{$contractstype->nazov_typu}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <p><input name="contractstype" type="hidden" value="{{ $contract->contracts_type_id }}" ></p>
                                @endif
                                @endif
                                    @if (($contract->contracts_type_id==4)||($contract->contracts_type_id==2))
                                        <div class="form-group" style="display: none">
                                            <input name="product_id" id="product_id" style="display: none" value="1" type="text" class="form-control" id="recipient-name">
                                            <input name="contractstype" id="contractstype" style="display: none" value="{{$contract->contracts_type_id}}" type="text" class="form-control" id="recipient-name">
                                            <input name="client_id" id="client_id" style="display: none" value="{{$contract->client_id}}" type="text" class="form-control" id="recipient-name">
                                            <input name="nadradena_id" id="nadradena_id" style="display: none" value="{{$contract->id}}" type="text" class="form-control" id="recipient-name">


                                        </div>
                                    @endif
                                    @if ($contract->contracts_type_id ==1 or $contract->contracts_type_id ==3)
                                        @if ($overenie_skladu == true)
                                    <div class="form-group">
                                        <label>Priradiť produkt k zákazke:</label>
                                        <select name="product_id" id="product_id" onchange="defHodnota()"   class="form-control">
                                              @foreach ($products as $product)
                                                    @if($product->id == $contract->product_id)
                                                        <option value="{{$product->id}}" selected>{{$product->nazov_produktu}}</option>
                                                    @else
                                                        <option value="{{$product->id}}">{{$product->nazov_produktu}}</option>
                                                    @endif
                                              @endforeach
                                        </select>
                                    </div>
                                        @else
                                            <div class="form-group">
                                                <label>Priradiť produkt k zákazke:</label>
                                                <select name="product_id" id="product_id"  readonly="readonly"  class="form-control">
                                                    @foreach ($products as $product)
                                                        @if($product->id == $contract->product_id)
                                                            <option value="{{$product->id}}" selected>{{$product->nazov_produktu}}</option>
                                                        @else
                                                            <option value="{{$product->id}}">{{$product->nazov_produktu}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <p>Produkt bol odoslaný na sklad, preto už nie je možné zmeniť produkt!</p>
                                            </div>
                                        @endif
                                    @endif
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Popis zákazky</label>
                                    <input name="popis" value="{{$contract->popis}}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte popis zákazky">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poradové číslo zákazky</label>
                                    <input name="poradove_cislo" value="{{$contract->poradove_cislo}}" required type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte poradové číslo zákazky">
                                </div>
                                    @if ($contract->contracts_type_id ==1 or $contract->contracts_type_id ==3)
                                <div class="form-group">
                                    <label>Vyberte klienta:</label>
                                    <select name="client_id" id="clientid" readonly="readonly" class="form-control">
                                        @foreach ($client as $client2)
                                            @if($client2->id == $contract->client_id)
                                                <option value="{{$client2->id}}" selected>{{$client2->nazov_firmy}}</option>
                                            @else
                                                <option value="{{$client2->id}}" >{{$client2->nazov_firmy}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                    @endif
                                    @if ($overenie_skladu == true)
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Počet ks:</label>
                                    <input name="pocet_ks" value="{{$contract->pocet_ks}}" type="number" min="1" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte počet ks">
                                </div>
                                    @else
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Počet ks:</label>
                                            <input name="pocet_ks" value="{{$contract->pocet_ks}}" type="number" min="1" readonly required class="form-control" id="exampleInputEmail1" placeholder="Zadajte počet ks">
                                        <p>Produkt bol odoslaný na sklad, preto už nie je možné zmeniť množstvo!</p>
                                        </div>
                                    @endif
                                    @if ($contract->contracts_type_id ==1 or $contract->contracts_type_id ==3)
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Číslo objednávky:</label>
                                    <input name="cislo_objednavky" value="{{$contract->cislo_objednavky}}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte číslo obj">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Číslo faktúry:</label>
                                    <input name="cislo_faktury" value="{{$contract->cislo_faktury}}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte číslo faktúry">
                                </div>
                                    @endif
                                <div class="form-group">
                                    <label>Vyberte zodpovednú osobu:</label>
                                    <select name="zodpovedna_osoba_id" class="form-control">
                                        @foreach($employy as $employy2)
                                            @if($employy2->id == $contract->zodpovedna_osoba_id)
                                                <option value="{{$employy2->id}}" selected>{{$employy2->meno}}</option>
                                            @else
                                                <option value="{{$employy2->id}}">{{$employy2->meno}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Podiel v pecentách pre zodpovednú osobu:</label>
                                    <input name="podiel_zodp_osoba_perc" value="{{$contract->podiel_zodp_osoba_perc}}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte podiel v pecentách pre zodpovednú osobu">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Odhadovaný čas:</label>
                                    <input name="odhadovany_cas" value="{{$contract->odhadovany_cas}}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte odhadovaný čas">
                                </div>

                                <div class="form-group">
                                    <label for="hodnotazakazky">Hodnota za ks:</label>
                                    <input name="hodnota_zakazky_za_ks" value="{{$contract->hodnota_zakazky_za_ks}}"  type="number" min="0" step="0.01" class="form-control" id="hodnota_zakazky_za_ks" placeholder="Zadajte hodnotu za ks">
                                </div>

                                <div class="form-group">
                                    <label>Termín odovzdania zákazky:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="datumodovzdania" value="{{$contract->datumodovzdania}}" type='text'  class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>

                                </div>
                                    @if (($overenie == 0)&&($overenie_ukoncenia==true))
                                    <div class="form-group">
                                    <label>Stav zákazky:</label>
                                    <select id="stavzakazky" name="stav_zakazky" onchange="overenie()" class="form-control">
                                        @foreach($contractstatus as $contractstatus2)
                                            @if($contractstatus2->id == $contract->stav_zakazky)
                                                <option value="{{$contractstatus2->id}}" selected>{{$contractstatus2->nazov_stavu}}</option>
                                            @else
                                                <option value="{{$contractstatus2->id}}">{{$contractstatus2->nazov_stavu}}</option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                                        <p><font color="red">Ak idete zmeniť stav zákazky na Neaktívna, najskôr môžete poslať produkt na sklad a potom uložiť !</font></p>
                                    @else
                                        <input name="stav_zakazky" id="stavzakazky" style="display: none" value="1" type="number" class="form-control" id="recipient-name">
                                    @endif
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                                @if ((($contract->contracts_type_id==1)||($contract->contracts_type_id==3)) && ($overenie == 0)&&($overenie_skladu == true))
                                <button name="_method" value="post" type="submit" formaction="{{route('sendstock.store')}}" class="btn btn-success">Poslať na sklad</button>
                                @elseif ($overenie_skladu == false)
                                    <a class="btn btn-warning">Produkt už bol odoslaný na sklad!</a>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

            <script>
                window.onload = overenie();
                function overenie() {
                    var hodnota = document.getElementById("stavzakazky").value;
                    if (hodnota !=1)
                    {
                        document.getElementById("hodnota_zakazky_za_ks").required = true;
                        var x = document.getElementById("hodnota_zakazky_za_ks").required;
                    }
                    else {
                        document.getElementById("hodnota_zakazky_za_ks").required = false;
                    }

                }

            </script>
            @if ($contract->contracts_type_id!=4)
            <script type="text/javascript">
                $('.date').datepicker({
                    //format: 'dd-mm-yyyy'
                    format: 'yyyy-mm-dd'
                });

            </script>
            <script>
                window.onload = defHodnota();
                function defHodnota() {
                    var hodnota = document.getElementById("product_id").value;
                    var sampleTags = [];
                    @foreach ($products as $product)
                    sampleTags.push('{{ $product->id}}');
                    sampleTags.push('{{ $product->client_id}}');


                    if (hodnota == sampleTags[0] ) {
                        sampleTags.push('{{ $product->cena__bez_dph}}');
                        document.getElementById('hodnota_zakazky_za_ks').readOnly = false;
                        document.getElementById('clientid').readOnly = false;
                        $("select[readonly]").find("option:not(:selected)").hide().attr("disabled",false);
                        document.getElementById("hodnota_zakazky_za_ks").value=sampleTags[2];
                        document.getElementById("clientid").value=sampleTags[1];
                        document.getElementById('hodnota_zakazky_za_ks').readOnly = true;
                        document.getElementById('clientid').readOnly = true;
                        $("select[readonly]").find("option:not(:selected)").hide().attr("disabled",true);
                    }
                    sampleTags = [];


                    @endforeach

                }

            </script>
            <script src="/js/select2.min.js"></script>
            <script type="text/javascript">

                $("#product_id").select2({
                    placeholder: "Vyberte nazov zakazky",
                    allowClear: true
                });
            </script>
            @endif


@endsection
