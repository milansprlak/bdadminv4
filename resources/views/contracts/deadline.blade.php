@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam zákaziek, ktorých sa blíži dátum odovzdania </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo</th>
                                    <th>Názov</th>
                                    <th>Popis</th>
                                    <th>Typ zákazky</th>
                                    <th>Zodpovedný vedúci</th>
                                    <th>Stav zákazky</th>
                                    <th>Klient</th>
                                    <th>Číslo objednávky</th>
                                    <th>Počet ks</th>
                                    <th>Termín odovzdania</th>
                                    <th>Číslo faktúry</th>
                                    <th>Dátum vytvorenia</th>


                                </tr>

                                @foreach ($contracts as $contract)
                                       <tr>
                                            <td><a href="/contracts/{{ $contract->id }}">  {{ $contract->poradove_cislo}}</a></td>
                                           <td><a href="/contracts/{{ $contract->id }}">  {{ $contract->produktzakazky->nazov_produktu }}</a></td>
                                            <td><a href="/contracts/{{ $contract->id }}">  {{ $contract->popis }}</a></td>
                                            <td>{{$contract->typzakazky->nazov_typu}}</td>
                                            <td>{{$contract->zodpovednaosoba->meno}}</td>
                                            <td>{{$contract->stavzakazky->nazov_stavu}}</td>
                                            <td>{{$contract->klientzakazky->nazov_firmy}}</td>
                                            <td>{{ $contract->cislo_objednavky}}</td>
                                            <td>{{$contract->pocet_ks}}</td>
                                            <td>{{($contract->datumodovzdania ? date('d.m.Y', strtotime($contract->datumodovzdania)): '')}}</td>
                                            <td>{{ $contract->cislo_faktury}}</td>
                                            <td>{{$contract->created_at->format('d.m.Y')}}</td>
                                        </tr>


                                @endforeach

                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection