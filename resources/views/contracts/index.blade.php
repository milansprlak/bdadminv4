@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam zákaziek</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('contracts.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class="box-body">
                                    <div class='form-group col-md-2'>
                                    <label>Časové obdobie od:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="fromdatum" type='text' value="{{$fromdatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    </div>
                                        <div class='form-group col-md-2'>
                                            <label>Časové obdobie do:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="todatum" type='text' value="{{$todatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Stav zákazky:</label>
                                        <select id="stavzakazky" name="stav_zakazky"  class="form-control">
                                            @foreach($contractstatus as $contractstatus2)
                                                @if($contractstatus2->id == $value_stav_zakazky)
                                                    <option value="{{$contractstatus2->id}}" selected>{{$contractstatus2->nazov_stavu}}</option>
                                                @else
                                                    <option value="{{$contractstatus2->id}}">{{$contractstatus2->nazov_stavu}}</option>
                                                @endif
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Klient:</label>
                                        <select id="klient" name="klient"  class="form-control">
                                            <option value="0">Všetky</option>
                                            @foreach($clients as $client)

                                                @if($client->id == $value_klient)
                                                    <option value="{{$client->id}}" selected>{{$client->nazov_firmy}}</option>
                                                @else
                                                    <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                                @endif
                                            @endforeach

                                        </select>

                                    </div>
                                    <div class="form-group col-md-4">
                                        <br>
                                    <button type="submit" class="btn btn-primary col-md-3">Zobraziť</button>
                                    </div>
                                </div>




                                <!-- /.box-body -->
                            </form>

                            <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo</th>
                                    <th>Názov</th>
                                    <th>Popis</th>
                                    <th>Typ zákazky</th>
                                    <th>Zodpovedný vedúci</th>
                                    <th>Stav zákazky</th>
                                    <th>Klient</th>
                                    <th>Číslo objednávky</th>
                                    <th>Počet ks</th>
                                    <th>Termín odovzdania</th>
                                    <th>Číslo faktúry</th>
                                    <th>Dátum vytvorenia</th>


                                </tr>

                                @foreach ($contracts as $contract)
                                    @if ($contract->stavzakazky->id==1)
                                        <tr>
                                            <td><a href="/contracts/{{ $contract->id }}">  {{ $contract->poradove_cislo}}</a></td>
                                            <td><a href="/contracts/{{ $contract->id }}">  {{ $contract->produktzakazky->nazov_produktu }}</a></td>
                                            <td><a href="/contracts/{{ $contract->id }}">  {{ $contract->popis }}</a></td>
                                            <td>{{$contract->typzakazky->nazov_typu}}</td>
                                            <td>{{$contract->zodpovednaosoba->meno}}</td>
                                            <td>{{$contract->stavzakazky->nazov_stavu}}</td>
                                            <td>{{$contract->klientzakazky->nazov_firmy}}</td>
                                            <td>{{ $contract->cislo_objednavky}}</td>
                                            <td>{{$contract->pocet_ks}}</td>
                                            <td>{{($contract->datumodovzdania ? date('d.m.Y', strtotime($contract->datumodovzdania)): '')}}</td>
                                            <td>{{ $contract->cislo_faktury}}</td>
                                            <td>{{$contract->created_at->format('d.m.Y')}}</td>
                                        </tr>
                                    @else

                                        <tr>
                                            <td><a style="color: red" href="/contracts/{{ $contract->id }}">  {{ $contract->poradove_cislo}}</a></td>
                                            <td><a style="color: red" href="/contracts/{{ $contract->id }}">  {{ $contract->produktzakazky->nazov_produktu }}</a></td>
                                            <td><a style="color: red" href="/contracts/{{ $contract->id }}">  {{ $contract->popis }}</a></td>
                                            <td><span style="color: red">{{$contract->typzakazky->nazov_typu}}</span></td>
                                            <td><span style="color: red">{{$contract->zodpovednaosoba->meno}}</span></td>
                                            <td><span style="color: red">{{$contract->stavzakazky->nazov_stavu}}</span></td>
                                            <td><span style="color: red">{{$contract->klientzakazky->nazov_firmy}}</span></td>
                                            <td><span style="color: red">{{$contract->cislo_objednavky}}</span></td>
                                            <td><span style="color: red">{{$contract->pocet_ks}}</span></td>
                                            <td><span style="color: red">{{($contract->datumodovzdania ? date('d.m.Y', strtotime($contract->datumodovzdania)): '')}}</span></td>
                                            <td><span style="color: red">{{$contract->cislo_faktury}}</span></td>
                                            <td><span style="color: red">{{$contract->created_at->format('d.m.Y')}}</span></td>
                                        </tr>

                                    @endif
                                @endforeach

                            </table>
                            {{ $contracts->appends(['fromdatum' => $fromdatum,'todatum'=> $todatum,'stav_zakazky' => $value_stav_zakazky,'klient' => $value_klient])->links() }}
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->

        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Odpracované hodiny za dnes </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <tr>
                            <th>Poradie</th>
                            <th>Meno zamestnanca</th>
                            <th>Odpracovaný čas za dnes</th>

                            <!--  <th></th>-->

                        </tr>
                        <div style="display: none;">
                            {{ $i=1 }}
                        </div>

                        @foreach($zamestnanci as $zamestnanec)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $zamestnanec['meno'] }}</td>
                                <td>{{ number_format($zamestnanec['hod'])}} hod {{ number_format($zamestnanec['min'])}} min</td>
                            </tr>
                        @endforeach
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>
@endsection