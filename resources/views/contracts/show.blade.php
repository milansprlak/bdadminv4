@extends('admin_template')
<link href="/css/image-picker.css" rel="stylesheet">
<script src="/js/jquery.js"></script>
<script src="/js/image-picker.js"></script>

@section('content')
    <style>
        /* Style the form */
        #regForm {
            background-color: #ffffff;
            margin: 100px auto;
            padding: 40px;
            width: 70%;
            min-width: 300px;
        }

        /* Style the input fields */
        input {
            padding: 10px;
            width: 100%;
            font-size: 17px;
            font-family: Raleway;
            border: 1px solid #aaaaaa;
        }

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
            background-color: #ffdddd;
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }

        /* Mark the active step: */
        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #4CAF50;
        }

        select{
            height: 46px;
            line-height: 46px;
            border-radius: 0;
            box-shadow: none;
            border-color: #d2d6de;
            padding: 10px 16px;
            font-size: 18px;
            display: block;
            width: 100%;
        }


    </style>
    <div class='row'>



        <div class='col-md-12'>
            <!-- Box -->
            @if ($contract->contracts_type_id != 4)
                <a href="/contracts/" class="btn btn-info btn-lg">
                    <span class="glyphicon glyphicon-arrow-left"></span> Späť
                </a>
            @else
                <a href="/contracts/{{$contract->nadradena_id}}" class="btn btn-info btn-lg">
                    <span class="glyphicon glyphicon-arrow-left"></span> Späť
                </a>
            @endif
            <div class="box box-primary">
                <div class="box-header with-border">

                    <h3 class="box-title">Zákazka</h3>

                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
@if ($contract->contracts_type_id != 3)
                    <div class="box">
                        <div id="buttony">
                        @foreach($contractstatus as $contractstatus2)
                            @if($contractstatus2->id==$contract->stav_zakazky)
                                @if (($contractstatus2->id==1)||((Auth::check())&&((Auth::user()->mod_1)==1)))
                                    <div style="display: none;">{{$stavzakazky=1}}</div>

                                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#exampleModal">
                                        Pridať činnosť
                                    </button>
                                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#nahratsubor">
                                        Nahrať súbor
                                    </button>

                                        @else
                                        <div style="display: none;">{{$stavzakazky=0}}</div>

                                @endif
                            @endif
                        @endforeach

                            @if((Auth::check())&&((Auth::user()->mod_1)==1))
                                <a class="btn btn-primary btn-lg" href="/contracts/{{$contract->id}}/edit/" role="button">Editovať zákazku</a>
                                <button onclick="vytlacit_stranku()" class="btn btn-primary btn-lg">
                                   Vytlačiť stránku
                                </button>
                            <!--   <a class="btn btn-primary btn-lg" href="/contracts/getPDF/{{$contract->id}}" role="button">Exportovať do pdf</a> -->
                            @endif
                        </div>
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <form id="regForm" method="post" action="{{ route('tasks.store') }}" >
                            {{ csrf_field() }}

                            <h1>Pridať činnosť:</h1>

                            <!-- One "tab" for each step in the form: -->
                            <div class="tab"><h2>Vyberte Vaše meno:</h2>
                                <select id="employy_id" name="employy_id" class="image-picker show-labels show-html">
                                    @foreach($employy as $employy2)
                                        <option data-img-src="{{$employy2->url_fotka}}" value="{{$employy2->id}}">{{$employy2->meno}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="tab"><h2>Vyberte činnosť:</h2>
                                <p><select id="taskselect"  onchange="defHodnota()" name="defined_task_id"  oninput="this.className = ''">
                                        @foreach ($defined_task as $defined_task2)
                                        <option value="{{$defined_task2->id}}">{{$defined_task2->nazov}}</option>
                                        @endforeach

                                    </select></p> <br>
                                <div id="subtasks" ></div><br>

                                <p><input id="poznamka" style="display: none" name="poznamka" type="text" placeholder="Zadajte vlastný názov kategórie" oninput="this.className = ''"></p>
                                <p><input id="ostatne_cena_za_ks" style="display: none" name="ostatne_cena_za_ks" type="text" placeholder="Cena za 1 ks" oninput="this.className = ''"></p>

                                <p><input id="pocet_ks_material" style="display: none" name="pocet_ks_material" type="number" placeholder="Zadajte hmotnosť materiálu v kg za 1ks" oninput="this.className = ''"></p>

                                <p id="demo"></p>

                            </div>



                               <div class="tab"><h2>Počet ks:</h2>
                                <p><input name="pocet_ks" id="pocet_ks" type="number" min="1" placeholder="Počet ks"  class="required" oninput="this.className = ''" ></p>

                            </div>

                            <div class="tab"><h2>Zadajte čas:</h2>
                                @if((Auth::check())&&((Auth::user()->mod_1)==1))<p><input name="datum" type="date" placeholder="Dátum..." value="{{$now}}" class="required" oninput="this.className = ''"></p> @endif
                                <p><input name="trvanie_hod" type="number" min="0" placeholder="Hodiny..." class="required" oninput="this.className = ''"></p>
                                <p><input name="trvanie_min" type="number" min="0" max="59" placeholder="Minúty..." class="required"  oninput="this.className = ''"></p>
                                <p><input name="contract_id" type="hidden" value="{{ $contract->id }}" oninput="this.className = ''"></p>
                            </div>

                            <div style="overflow:auto;">
                                <div style="float:right;">
                                    <button type="button" class="btn btn-secondary btn-lg" id="prevBtn" onclick="nextPrev(-1)">Späť</button>
                                    <button type="button" class="btn btn-primary btn-lg" id="nextBtn" onclick="nextPrev(1)">Ďalej</button>
                                </div>
                            </div>

                            <!-- Circles which indicates the steps of the form: -->
                            <div style="text-align:center;margin-top:40px;">
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                            </div>

                        </form>
                        </div>

                        <div class="modal fade" id="nahratsubor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2>Pridať súbor</h2>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ route('upload.store') }}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group">
                                                <select name="zakazka_id" class="form-control" style="display:none;">
                                                    <option value="{{$contract->id}}"></option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Vyberte zamestnanca:</label>
                                                <select name="zamestnanec_id" class="form-control">
                                                    @foreach($employy as $employy2)
                                                        <option value="{{$employy2->id}}">{{$employy2->meno}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="file" class="form-control-file" name="fileToUpload" id="exampleInputFile" aria-describedby="fileHelp">
                                                <small id="fileHelp" class="form-text text-muted">Odovzdajte platný súbor. Veľkosť by nemala byť väčšia ako 90 MB.</small>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvoriť</button>
                                            <button type="submit" class="btn btn-primary">Nahrať súbor</button>
                                            </div>
                                        </form>

                                    </div>


                                </div>
                            </div>
                        </div>
                                @if ($contract->contracts_type_id ==1 || $contract->contracts_type_id ==3 )
                                 <h2>{{ $contract->produktzakazky->nazov_produktu }}</h2>
                                @else
                                <h2>{{ $contract->popis }}</h2>
                                @endif


                        <table id="infoozakazke" class="table table-bordered table-striped">
                            <tr>
                                <th>Popis</th>
                                <th>Typ zákazky</th>
                                <th>Zodpovedný vedúci</th>
                                <th>Stav zákazky</th>
                                <th>Klient</th>
                                <th>Poradové číslo</th>
                                <th>Číslo objednávky</th>
                                <th>Termín odovzdania</th>
                                <th>Číslo faktúry</th>
                                <th>Počet ks</th>
                                <th>Odhadovaný čas</th>

                            </tr>

                            <tr>
                                <td>{{ $contract->popis }}</td>
                                <td>{{$contract->typzakazky->nazov_typu}}</td>
                                <td>{{$contract->zodpovednaosoba->meno}}</td>
                                <td>{{$contract->stavzakazky->nazov_stavu}}</td>
                                <td>{{$contract->klientzakazky->nazov_firmy}}</td>
                                <td>{{ $contract->poradove_cislo}}</td>
                                <td>{{ $contract->cislo_objednavky}}</td>
                                <td>{{($contract->datumodovzdania ? date('d.m.Y', strtotime($contract->datumodovzdania)): '')}}</td>
                                <td>{{ $contract->cislo_faktury}}</td>
                                <td>{{ $contract->pocet_ks }}</td>
                                <td>{{ $contract->odhadovany_cas }}</td>

                            </tr>
                        </table>
                        @if (($stavzakazky==1)||((Auth::check())&&((Auth::user()->mod_1)==1)))
                        <h3>Zoznam činnosti</h3>
                            <div style="display: none;">{{$suma_pocet_ks=0, $suma_pocet_ks_material=0,$hodnotarezijna=0, $suma_hod=0, $suma_min=0, $celkovacenacinnosti=0, $celkovacenameterialu=0,$celkovacena=0 }}</div>
    @if (($contract->contracts_type_id == 1)||($contract->contracts_type_id == 4))

    <table id="example1" class="table table-bordered table-striped">

        <tr>
            <th>Dátum</th>
            <th>Zamestnanec</th>
            <th>Názov</th>
            <th>Poznámka</th>
            <th>Počet ks</th>
            <th>Hmotnosť materiálu (kg)</th>
            <th>Trvanie hod</th>
            <th>Trvanie min</th>
            @if ((Auth::check())&&((Auth::user()->mod_1)==1))
            <th>Cena za činnosť (1 hod)</th>
            <th>Cena za materiál (1 kg)</th>
            <th>Celková cena za činnosť</th>
            <th>Celková cena za materiál</th>
            <th>Celková cena</th>
                <th>Podiel pre zamestnanca zo zisku</th>
            @endif
            <th class="hidden-print">Editácia</th>

        </tr>


        @foreach ($contract->tasks as $task) <!--// nacita tabulku Task priradenu k contract_id -->

            <tr>
                        <td>{{ Carbon\Carbon::parse($task->datum)->format('d.m.Y') }}</td>
                        <td>{{$task->zamestnanec->meno}}</td>
                        <td>{{$task->definovanytask->nazov}}</td>
                        <td>{{$task->poznamka}}</td>
                        <td>{{$task->pocet_ks, $suma_pocet_ks=$suma_pocet_ks+$task->pocet_ks}}</td>
                        <td>{{$task->pocet_ks_material, $suma_pocet_ks_material=$suma_pocet_ks_material+$task->pocet_ks_material}}</td>
                        <td>{{$task->trvanie_hod, $suma_hod=$suma_hod+$task->trvanie_hod}}</td>
                        <td>{{$task->trvanie_min, $suma_min=$suma_min+$task->trvanie_min}}</td>
                        @if ((Auth::check())&&((Auth::user()->mod_1)==1))
                        <td>{{$task->cena_cinnost_hod}} €</td>
                        <td>{{$task->cena_material_kg}} €</td>
                        <td>{{$task->celkova_cena_cinnost, $celkovacenacinnosti=$celkovacenacinnosti+$task->celkova_cena_cinnost}} €</td>
                        <td>{{$task->celkova_cena_material, $celkovacenameterialu=$celkovacenameterialu+$task->celkova_cena_material}} €</td>
                        <td>{{$task->celkova_cena,$celkovacena=$celkovacena+$task->celkova_cena}} €</td>
                        <td>{{$task->hodnota_tasku}} €</td>
                        @endif

                        @if ((($task->created_at->isToday())&&($stavzakazky==1))||((Auth::check())&&((Auth::user()->mod_1)==1)))
                            <td class="hidden-print"><a href="/tasks/{{ $task->id }}/edit">Editovať</a></td>
                            @else
                            <td></td>
                        @endif
            </tr>
        @endforeach
        <tr>
            <td class="bg-primary"><strong>Spolu:</strong></td>
            <td class="bg-primary"></td>
            <td class="bg-primary"></td>
            <td class="bg-primary"></td>
            <td class="bg-primary"><strong>{{number_format($suma_pocet_ks)}}</strong></td>
            <td class="bg-primary"><strong>{{number_format($suma_pocet_ks_material,2,',',' ')}}</strong></td>
            <div style="display: none;">
            @if ($suma_min>59)
                {{
                $pom_hod =floor($suma_min/60),
                $suma_hod = $suma_hod+$pom_hod,
                $pom_min = $pom_hod*60,
                $suma_min=$suma_min-$pom_min

                }}
            @endif
            </div>
            <td class="bg-primary"><strong>{{number_format($suma_hod)}}</strong></td>
            <td class="bg-primary"><strong>{{number_format($suma_min)}}</strong></td>
            @if ((Auth::check())&&((Auth::user()->mod_1)==1))
            <td class="bg-primary"></td>
            <td class="bg-primary"></td>
            <td class="bg-primary"><strong>{{number_format($celkovacenacinnosti,2,',',' ')}} €</strong></td>
            <td class="bg-primary"><strong>{{number_format($celkovacenameterialu,2,',',' ')}} €</strong></td>
            <td class="bg-primary"><strong>{{number_format($celkovacena,2,',',' ')}} €</strong></td>
                <td class="hidden-print bg-primary"></td>
            @endif


            <td class="hidden-print bg-primary"></td>
        </tr>



    </table>
                            @else
                                <table id="example3" class="table table-bordered table-striped">

                                    <tr>
                                        <th>Dátum</th>
                                        <th>Zamestnanec</th>
                                        <th>Názov</th>
                                        <th>Poznámka</th>

                                        <th>Trvanie hod</th>
                                        <th>Trvanie min</th>
                                        @if ((Auth::check())&&((Auth::user()->mod_1)==1))
                                            <th>Režijna cena hodina</th>
                                            <th>Celková cena za materiál</th>
                                            <th>Celková cena za činnosť</th>
                                            <th>Celkova cena:</th>
                                            <th>Hodnota tasku</th>
                                        @endif
                                        <th class="hidden-print">Editácia</th>

                                    </tr>


                                @foreach ($contract->tasks as $task) <!--// nacita tabulku Task priradenu k contract_id -->

                                    <tr>
                                        <td>{{ Carbon\Carbon::parse($task->datum)->format('d.m.Y') }}</td>
                                        <td>{{$task->zamestnanec->meno}}</td>
                                        <td>{{$task->definovanytask->nazov}}</td>
                                        <td>{{$task->poznamka}}</td>
                                        <td>{{$task->trvanie_hod, $suma_hod=$suma_hod+$task->trvanie_hod}}</td>
                                        <td>{{$task->trvanie_min, $suma_min=$suma_min+$task->trvanie_min}}</td>
                                        @if ((Auth::check())&&((Auth::user()->mod_1)==1))
                                            <td>{{$task->cena_cinnost_hod}} €</td>
                                            <td>{{$task->celkova_cena_material, $celkovacenameterialu=$celkovacenameterialu+$task->celkova_cena_material}} €</td>
                                            <td>{{$task->celkova_cena_cinnost, $celkovacenacinnosti=$celkovacenacinnosti+$task->celkova_cena_cinnost}} €</td>
                                            <td>{{$task->celkova_cena,$celkovacena=$celkovacena+$task->celkova_cena}} </td>
                                            <td>{{$task->hodnota_tasku, $hodnotarezijna=$hodnotarezijna+$task->hodnota_tasku}} €</td>
                                        @endif

                                        @if ((($task->created_at->isToday())&&($stavzakazky==1))||((Auth::check())&&((Auth::user()->mod_1)==1)))
                                            <td class="hidden-print"><a href="/tasks/{{ $task->id }}/edit">Editovať</a></td>
                                        @else
                                            <td></td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="bg-primary"><strong>Spolu:</strong></td>
                                        <td class="bg-primary"></td>
                                        <td class="bg-primary"></td>
                                        <td class="bg-primary"></td>

                                        <div style="display: none;">
                                            @if ($suma_min>59)
                                                {{
                                                $pom_hod =floor($suma_min/60),
                                                $suma_hod = $suma_hod+$pom_hod,
                                                $pom_min = $pom_hod*60,
                                                $suma_min=$suma_min-$pom_min

                                                }}
                                            @endif
                                        </div>
                                        <td class="bg-primary"><strong>{{number_format($suma_hod)}}</strong></td>
                                        <td class="bg-primary"><strong>{{number_format($suma_min)}}</strong></td>
                                        <td class="bg-primary"></td>
                                        @if ((Auth::check())&&((Auth::user()->mod_1)==1))
                                            <td class="bg-primary"><strong>{{number_format($celkovacenameterialu,2,',',' ')}} €</strong></td>
                                            <td class="bg-primary"><strong>{{number_format($celkovacenacinnosti,2,',',' ')}} €</strong></td>
                                        <td class="bg-primary"><strong>{{number_format($celkovacena,2,',',' ')}} €</strong></td>
                                        <td class="bg-primary"><strong>{{number_format($hodnotarezijna,2,',',' ')}} €</strong></td>
                                        <td class="bg-primary"></td>
                                        @endif

                                    </tr>



                                </table>

     @endif

    @endif

</div>
<!-- /.box -->
@if (((Auth::check())&&((Auth::user()->mod_1)==1))&&($contract->stavzakazky->id == 1))
<h3>Výrobna Hodnota zákazky (za 1ks): {{number_format($celkovacena/$contract->pocet_ks ,2,',',' ')}} €</h3>
    <h3>Hodnota zákazky za ks (predajná cena): {{$contract->hodnota_zakazky_za_ks}} €</h3>
@endif

</div><!-- /.box-body -->

</div><!-- /.box -->
</div><!-- /.col -->
@if (((Auth::check())&&((Auth::user()->mod_1)==1))&&($contract->stavzakazky->id != 1))
<div id="druhybox" class='col-md-6'>
<!-- Box -->
<div class="box box-primary">
<div class="box-header with-border">
<h3 class="box-title">Štatistika zákazky</h3>
<div class="box-tools pull-right">
    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>

</div>
</div>
<div class="box-body">
@foreach($contractstatistics as $contractstatistic)
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Hodnota:</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">Celková predajná cena za 1ks:</th>
            <td>{{number_format($contractstatistic->celkova_predajna_cena_ks,2,',',' ')}} €</td>

        </tr>
        <tr>
            <th scope="row">Počet ks:</th>
            <td>{{$contractstatistic->pocet_ks}} ks</td>

        </tr>
        <tr>
            <th scope="row">Celková predajná cena:</th>
            <td>{{number_format($contractstatistic->celkova_predajna_cena,2,',',' ')}} €</td>

        </tr>
        <tr>
            <th scope="row">Celková výrobná cena:</th>
            <td>{{number_format($contractstatistic->celkova_vyrobna_cena,2,',',' ')}} €</td>

        </tr>
        <tr>
            <th scope="row">Celková výrobná cena za ks:</th>
            <td>{{number_format($contractstatistic->hodnota_zakazky_ks,2,',',' ')}} €</td>
        </tr>
        <tr>
            <th scope="row">Percentuálnu podiel pre zodpovednú osobu:</th>
            <td>{{$contractstatistic->podiel_zodp_osoba_perc}} %</td>
        </tr>
        <tr>
            <th scope="row">Podiel pre zodpovednú osobu:</th>
            <td>{{number_format($contractstatistic->podiel_zodp_osoba_hodnota,2,',',' ')}} €</td>
        </tr>

        <tr>
            <th scope="row">Celkový zisk (100%):</th>
            <td>{{number_format($contractstatistic->zisk,2,',',' ')}} €</td>

        </tr>
        <tr>
            <th scope="row">Zisk pre firmu (67%):</th>
            <td>{{number_format($contractstatistic->zisk_firmu,2,',',' ')}} €</td>

        </tr>
        <tr>
            <th scope="row">Zisk pre zamestnancov (33%):</th>
            <td>{{number_format($contractstatistic->zisk_zamestnanci,2,',',' ')}} €</td>

        </tr>
        <tr>
            <th scope="row">Podiel zo zisku pre zamestnanca za 1 min.:</th>
            <td>{{number_format($contractstatistic->podiel_zamestnanci,2,',',' ')}} €</td>

        </tr>
        <tr>
            <th scope="row">Celková cena za činnosť:</th>
            <td>{{number_format($contractstatistic->celkova_cena_cinnost,2,',',' ')}} €</td>

        </tr>
        <tr>
            <th scope="row">Celková cena za materiál:</th>
            <td>{{number_format($contractstatistic->celkova_cena_material,2,',',' ')}} €</td>

        </tr>

        </tbody>
    </table>
@endforeach
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!-- /.col -->
@endif


<div id="tretibox" class='col-md-6'>
<!-- Box -->
<div class="box box-primary">
<div class="box-header with-border">
<h3 class="box-title">Súbory v zákazke</h3>
<div class="box-tools pull-right">
    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>

</div>
</div>
<div class="box-body">
<table id="example1" class="table table-bordered table-striped">

    <tr>
        <th>Poradie</th>
        <th>Názov súboru</th>

    </tr>
    <div style="display: none;">
    {{ $i=1 }}
    </div>

    @foreach($download_files as $download_files2)
   <tr>
       <td>{{ $i++ }}</td>
       <td><a href="{{ $download_files2->url }}">  {{ $download_files2->nazov }}</a></td>
       @if((Auth::check())&&((Auth::user()->mod_1)==1))
       <td><a href="#"  onclick="
var result = confirm('Skutočne vymazať súbor?');
  if( result ){
          event.preventDefault();

          document.getElementById('delete-form-{{$download_files2->id}}').submit();
  }
      ">Odstrániť</a>
           <form id="delete-form-{{$download_files2->id}}" action="{{ route('upload.destroy',[$download_files2->id])}}"
               method="POST" style="display: none;">
               <input type="hidden" name="_method" value="delete">
               {{ csrf_field() }}
           </form>

       </td>
       @endif


    </tr>
    @endforeach




</table>
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!-- /.col -->
        <!-- Nadradena zakazka -->
@else
        @if (((Auth::check())&&((Auth::user()->mod_1)==1)))
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addproductModal" data-whatever="@mdo">Pridať podradenú zákazku</button>
                <a class="btn btn-primary" href="/contracts/{{$contract->id}}/edit/" role="button">Editovať zákazku</a>
                <div class="modal fade" id="addproductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow:hidden;">
                    <div class="modal-dialog" role="document">

                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLabel">Pridať podradenú zákazku</h3>

                            </div>
                            <div class="modal-body">
                                <form id="addItem" method="post" action="{{ route('contracts.store') }}" >
                                    {{ csrf_field() }}
                                    <div class="form-group" style="display: none">
                                        <input name="product_id" id="product_id" style="display: none" value="1" type="text" class="form-control" id="recipient-name">
                                        <input name="contractstype" id="contractstype" style="display: none" value="4" type="text" class="form-control" id="recipient-name">
                                        <input name="client_id" id="client_id" style="display: none" value="{{$contract->client_id}}" type="text" class="form-control" id="recipient-name">
                                        <input name="nadradena_id" id="nadradena_id" style="display: none" value="{{$contract->id}}" type="text" class="form-control" id="recipient-name">
                                        <input name="stav_zakazky" id="stav_zakazky" style="display: none" value="1" type="text" class="form-control" id="recipient-name">
                                        <input name="zodpovedna_osoba_id" id="zodpovedna_osoba_id" style="display: none" value="{{$contract->zodpovedna_osoba_id}}" type="text" class="form-control" id="recipient-name">
                                        <input name="podiel_zodp_osoba_perc" id="podiel_zodp_osoba_perc" style="display: none" value="{{$contract->podiel_zodp_osoba_perc}}" type="text" class="form-control" id="recipient-name">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" id="nazovpolozkylabel" class="col-form-label">Názov zákazky:</label>
                                        <input name="popis" id="popis"  type="text" class="form-control" id="recipient-name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Poradové číslo:</label>
                                        <input name="poradove_cislo" type="text" value="{{$nextnumber}}" class="form-control"  id="poradove_cislo" placeholder="Zadajte poradové číslo" >
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Počet ks:</label>
                                        <input type="number" name="pocet_ks" class="form-control" value="1" min="1" required id="recipient-name">
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Predajná cena za ks:</label>
                                        <input type="number" name="hodnota_zakazky_za_ks" id="hodnota_zakazky_za_ks" class="form-control"  min="0" step="0.01" required id="recipient-name">
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Dátum odovzdania:</label>
                                        <input name="datumodovzdania" value="{{$now}}" type="date" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte dátum splatnosti">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Odhadovaný čas:</label>
                                        <input name="odhadovany_cas" id="odhadcas" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte odhadovany cas">
                                    </div>



                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavrieť</button>
                                        <button type="submit" class="btn btn-primary">Vložiť položku</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

                <table id="example1" class="table table-bordered table-striped">
                    <tr>
                        <th>Poradové číslo</th>
                        <th>Názov</th>
                        <th>Typ zákazky</th>
                        <th>Zodpovedný vedúci</th>
                        <th>Stav zákazky</th>
                        <th>Klient</th>
                        <th>Číslo objednávky</th>
                        <th>Počet ks</th>
                        <th>Termín odovzdania</th>
                        <th>Číslo faktúry</th>
                        <th>Dátum vytvorenia</th>


                    </tr>

                    @foreach ($podradene_zakazky as $podradena_zakazka)
                        @if ($podradena_zakazka->stav_zakazky == 1)
                        <tr>
                            <td><a href="/contracts/{{ $podradena_zakazka->id }}">  {{ $podradena_zakazka->poradove_cislo}}</a></td>
                            <td><a href="/contracts/{{ $podradena_zakazka->id }}">  {{ $podradena_zakazka->popis }}</a></td>
                            <td>{{$podradena_zakazka->typzakazky->nazov_typu}}</td>
                            <td>{{$podradena_zakazka->zodpovednaosoba->meno}}</td>
                            <td>{{$podradena_zakazka->stavzakazky->nazov_stavu}}</td>
                            <td>{{$podradena_zakazka->klientzakazky->nazov_firmy}}</td>
                            <td>{{ $podradena_zakazka->cislo_objednavky}}</td>
                            <td>{{$podradena_zakazka->pocet_ks}}</td>
                            <td>{{($podradena_zakazka->datumodovzdania ? date('d.m.Y', strtotime($podradena_zakazka->datumodovzdania)): '')}}</td>
                            <td>{{ $podradena_zakazka->cislo_faktury}}</td>
                            <td>{{$podradena_zakazka->created_at->format('d.m.Y')}}</td>
                        </tr>
                        @else
                            <tr>
                                <td><a style="color: red" href="/contracts/{{ $podradena_zakazka->id }}">  {{ $podradena_zakazka->poradove_cislo}}</a></td>
                                <td><a style="color: red" href="/contracts/{{ $podradena_zakazka->id }}">  {{ $podradena_zakazka->popis }}</a></td>
                                <td><span style="color: red">{{$podradena_zakazka->typzakazky->nazov_typu}}</span></td>
                                <td><span style="color: red">{{$podradena_zakazka->zodpovednaosoba->meno}}</span></td>
                                <td><span style="color: red">{{$podradena_zakazka->stavzakazky->nazov_stavu}}</span></td>
                                <td><span style="color: red">{{$podradena_zakazka->klientzakazky->nazov_firmy}}</span></td>
                                <td><span style="color: red">{{ $podradena_zakazka->cislo_objednavky}}</span></td>
                                <td><span style="color: red">{{$podradena_zakazka->pocet_ks}}</span></td>
                                <td><span style="color: red">{{($podradena_zakazka->datumodovzdania ? date('d.m.Y', strtotime($podradena_zakazka->datumodovzdania)): '')}}</span></td>
                                <td><span style="color: red">{{ $podradena_zakazka->cislo_faktury}}</span></td>
                                <td><span style="color: red">{{$podradena_zakazka->created_at->format('d.m.Y')}}</span></td>
                            </tr>
                        @endif


                    @endforeach

                </table>





            @if (((Auth::check())&&((Auth::user()->mod_1)==1)))
<br>
                <div id="rozdiel" class='col-md-12'>
                    <div  class='col-md-6'>
                        @if($rozdiel_pred_cien==0)
                        <h3>Rozdiel medzi pred. cenou produktu a pred. cenou podzákazok:   <p class="btn btn-success btn-lg">{{number_format($rozdiel_pred_cien,2,',',' ')}} €</p></h3>
                        @else
                            <h3>Rozdiel medzi pred. cenou produktu a pred. cenou podzákazok:   <p class="btn btn-danger btn-lg">{{number_format($rozdiel_pred_cien,2,',',' ')}} €</p></h3>
                        @endif
                    </div>
                </div>
    <div id="druhybox" class='col-md-6'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Štatistika</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>

                    </div>
                </div>
                <div class="box-body">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Hodnota:</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Celková predajná cena (vložených podradených zákazok):</th>
                                <td>{{number_format($celkova_predajna_cena_zakazok,2,',',' ')}} €</td>

                            </tr>
                            <tr>
                                <th scope="row">Celková predajná cena (výsledného produktu):</th>
                                <td>{{number_format($celkova_predajna_cena,2,',',' ')}} €</td>

                            </tr>
                            <tr>
                                <th scope="row">Celková predajná cena výsledného produktu za ks:</th>
                                <td><span style="color: red">{{number_format($celkova_predajna_cena_ks,2,',',' ')}} €</span></td>

                            </tr>
                            <tr>
                                <th scope="row">Počet ks:</th>
                                <td>{{$pocet_ks}} ks</td>

                            </tr>

                            <tr>
                                <th scope="row">Celková výrobná cena:</th>
                                <td>{{number_format($celkova_vyrobna_cena,2,',',' ')}} €</td>

                            </tr>
                            <tr>
                                <th scope="row">Celková výrobná cena za ks:</th>
                                <td><span style="color: red">{{number_format($hodnota_zakazky_ks,2,',',' ')}} €</span></td>
                            </tr>
                            <tr>
                                <th scope="row">Percentuálnu podiel pre zodpovednú osobu:</th>
                                <td>{{$podiel_zodp_osoba_perc }} %</td>
                            </tr>
                            <tr>
                                <th scope="row">Podiel pre zodpovednú osobu:</th>
                                <td>{{number_format($podiel_zodp_osoba_hodnota,2,',',' ')}} €</td>
                            </tr>

                            <tr>
                                <th scope="row">Celkový zisk (100%):</th>
                                <td>{{number_format($zisk,2,',',' ')}} €</td>

                            </tr>
                            <tr>
                                <th scope="row">Zisk pre firmu (67%):</th>
                                <td>{{number_format($zisk_firmu ,2,',',' ')}} €</td>

                            </tr>
                            <tr>
                                <th scope="row">Zisk pre zamestnancov (33%):</th>
                                <td>{{number_format($zisk_zamestnanci,2,',',' ')}} €</td>

                            </tr>
                            <tr>
                                <th scope="row">Podiel zo zisku pre zamestnanca za 1 min.:</th>
                                <td>{{number_format($podiel_zamestnanci,2,',',' ')}} €</td>

                            </tr>
                            <tr>
                                <th scope="row">Celková cena za činnosť:</th>
                                <td>{{number_format($celkova_cena_cinnost,2,',',' ')}} €</td>

                            </tr>
                            <tr>
                                <th scope="row">Celková cena za materiál:</th>
                                <td>{{number_format($celkova_cena_material,2,',',' ')}} €</td>

                            </tr>
                            </tbody>
                        </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        @endif
        @endif

    </div><!-- /.row -->

    <style type="text/css" media="print">
    @page { size: landscape; }
    </style>

    @if ($contract->contracts_type_id != 3)
    <script>
    window.onload = defHodnota();

    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab
    function defHodnota() {
    var hodnota = document.getElementById("taskselect").value;

    //19_7_2018

    el = document.getElementById('taskselect')
    selectedText = el.options[el.selectedIndex].text

    if (selectedText == 'Ostatné') {
    document.getElementById('poznamka').style.display = 'block';
    document.getElementById('ostatne_cena_za_ks').style.display = 'block';
    }
    else {
    document.getElementById('poznamka').style.display = 'none';
    document.getElementById('ostatne_cena_za_ks').style.display = 'none';
    }
    //

    // document.getElementById("demo").innerHTML = "You selected: " + hodnota;

    var sampleTags = [];
    var sampleTags2 = [];
    var podkategoria = 0;
    var podkategoria_id = 0;
    @foreach ($subdefined_task as $subdefined_task2)
    sampleTags.push('{{ $subdefined_task2->id}}');
    sampleTags.push('{{ $subdefined_task2->nazov}}');
    sampleTags.push('{{ $subdefined_task2->nadradena_kat_id}}');

    //console.log(sampleTags[2]);
    if (hodnota == sampleTags[2]) {
    podkategoria = 1;
    podkategoria_id = sampleTags[2];
    }
    sampleTags2.push([sampleTags]);
    sampleTags = [];

    @endforeach

    if (podkategoria == 1)   {

    //  document.getElementById("demo").innerHTML = "Kategoria obsahuje podkategoriu. ID hlavnej kategorie je "+podkategoria_id;
    subTasks(hodnota);
    document.getElementById('pocet_ks_material').style.display = 'block';
    }

    else {

    // document.getElementById("demo").innerHTML = "Kategoria neobsahuje podkategoriu";
    document.getElementById("subtasks").innerHTML = '';
    document.getElementById('pocet_ks_material').style.display = 'none';

    }




    }

    function subTasks(idkat) {
    document.getElementById("subtasks").innerHTML = '';
    var myDiv = document.getElementById("subtasks");
    var subtasks = [];
    var subtasks2 = [];


    var selectList = document.createElement("select");
    selectList.setAttribute("id", "mySelect");
    selectList.setAttribute("text", "Vyberte material");
    selectList.setAttribute("name", "subdefined_task_id");
    myDiv.appendChild(selectList);


    @foreach ($subdefined_task as $subdefined_task2)

    subtasks.push('{{ $subdefined_task2->id}}');
    subtasks.push('{{ $subdefined_task2->nazov}}');
    subtasks.push('{{ $subdefined_task2->nadradena_kat_id}}');
    if (idkat == subtasks[2])
    {

    var option = document.createElement("option");
    option.setAttribute("value", subtasks[0]);
    option.text = subtasks[1];
    selectList.appendChild(option);
    }
    subtasks2.push([subtasks]);
    subtasks = [];

    @endforeach
    //    console.log(subtasks2);


    }


    function showTab(n) {

    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");

    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
    } else {
    document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Uložiť";
    } else {
    document.getElementById("nextBtn").innerHTML = "Ďalej";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
    }

    function nextPrev(n) {


    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
    }




    function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i,valid = true;
    x = document.getElementsByClassName("tab");
    // y = x[currentTab].getElementsByTagName("input");
    y = x[currentTab].getElementsByClassName("required");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {

    // If a field is empty...
    if (y[i].value == "") {

    // add an "invalid" class to the field:
    y[i].className += " invalid";
    // and set the current valid status to false:
    valid = false;

    }


    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {


    document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
    }

    function fixStepIndicator(n) {

    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");

    for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
    }

    function vytlacit_stranku() {

    document.getElementById('tretibox').style.display = 'none';
    document.getElementById('buttony').style.display = 'none';
    window.print();

    document.getElementById('tretibox').style.display = 'block';
    document.getElementById('buttony').style.display = 'block';
    }

    </script>
    <script>
    $("#employy_id").imagepicker(
    {
    show_label: true,
    }
    );
    // Retrieve the picker
    $("#employy_id").data('picker');
    </script>
    @else
        <script type="text/javascript">
            $('#addItem').on('submit', function(e) {
                var firstNamea = $('#popis');

                // Check if there is an entered value
                if(!firstNamea.val()) {
                    // Add errors highlight
                    firstNamea.closest('.form-group').removeClass('has-success').addClass('has-error');

                    // Stop submission of the form
                    e.preventDefault();
                } else {
                    // Remove the errors highlight
                    firstNamea.closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            });
        </script>
        <script type="text/javascript">
            $('#addItem').on('submit', function(e) {
                var firstNameb = $('#poradove_cislo');

                // Check if there is an entered value
                if(!firstNameb.val()) {
                    // Add errors highlight
                    firstNameb.closest('.form-group').removeClass('has-success').addClass('has-error');

                    // Stop submission of the form
                    e.preventDefault();
                } else {
                    // Remove the errors highlight
                    firstNameb.closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            });
        </script>
        <script type="text/javascript">
            $('#addItem').on('submit', function(e) {
                var firstNamec = $('#podiel_zodp_osoba_perc');

                // Check if there is an entered value
                if(!firstNamec.val()) {
                    // Add errors highlight
                    firstNamec.closest('.form-group').removeClass('has-success').addClass('has-error');

                    // Stop submission of the form
                    e.preventDefault();
                } else {
                    // Remove the errors highlight
                    firstNamec.closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            });
        </script>
    @endif

    @endsection