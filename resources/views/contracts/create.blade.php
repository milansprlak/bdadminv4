@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="/css/select2.min.css" rel="stylesheet" />
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pridať zákazku</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{ route('contracts.store') }}" role="form">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">

                                    <label>Vyberte typ zákazky: <i class="fa fa-question-circle" data-toggle="tooltip" title="Štandardná – ide o štandardnú zákazku. Režijná – ide o režijnu zákazku. Nadradená – ide o zákazku ktorá bude nadradená podradenej zákazke.
                                    Podradená – ide o zákazku, ktorá je podradená nadradenej zákazke." id="blah"></i></label>
                                    <select name="contractstype" id="contractstype_id" onchange="nastavenie()" class="form-control">
                                        @foreach ($contractstypes as $contractstype)
                                            <option value="{{$contractstype->id}}">{{$contractstype->nazov_typu}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" name="product_id" id="hiddenelement" />
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poradové číslo zákazky: <i class="fa fa-question-circle" data-toggle="tooltip" title="Toto pole obsahuje poradové číslo zväčšené o 1 od poslednej zákazky. Ak je nový rok tak sa automatický nastaví na 1/yy (napr. 1/19)." id="blah"></i></label>

                                    <input name="poradove_cislo" value="{{$nextContractNumber}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte poradové číslo zákazky">
                                </div>
                                <div id="produkt"  class="form-group">
                                    <label>Priradiť produkt k zákazke: <i class="fa fa-question-circle" data-toggle="tooltip" title="Tu si vyberte produkt, ktorý idete vyrábať. " id="blah"></i></label>
                                    <select name="product_id" id="product_id" onchange="defHodnota()" class="form-control">
                                        @foreach($products as $product)
                                            <option value="{{$product->id}}">{{$product->nazov_produktu}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Zadajte popis zákazky: <i class="fa fa-question-circle" data-toggle="tooltip" title="Ak chcete môžete zadať popis zákazky." id="blah"></i></label>
                                    <input name="popis" type="text"  class="form-control" id="popis" placeholder="Zadajte popis zákazky">
                                </div>
                                <div class="form-group">
                                    <label>Vyberte klienta: <i class="fa fa-question-circle" data-toggle="tooltip" title="V tejto možností sa Vám automatický nastaví klient podľa vybratého produktu (Priradiť produkt k zákazke). " id="blah"></i></label>
                                    <select name="client_id" id="clientid" class="form-control">
                                        @foreach ($client as $client2)
                                        <option value="{{$client2->id}}">{{$client2->nazov_firmy}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Počet ks: <i class="fa fa-question-circle" data-toggle="tooltip" title="Zadajte počet vyrábaných kusov produktu v tejto zákazke.  " id="blah"></i></label>
                                    <input name="pocet_ks" id="pocet_ks" type="number" min="1" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte počet ks">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Číslo objednávky:</label>
                                    <input name="cislo_objednavky" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte číslo objednávky">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Číslo faktúry:</label>
                                    <input name="cislo_faktury" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte číslo faktúry">
                                </div>
                                <div class="form-group">
                                    <label>Vyberte zodpovednú osobu:</label>
                                    <select name="zodpovedna_osoba_id" id="zodposovaid" class="form-control">
                                        @foreach($employy as $employy2)
                                            <option value="{{$employy2->id}}">{{$employy2->meno}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Podiel v pecentách pre zodpovednú osobu:</label>
                                    <input name="podiel_zodp_osoba_perc" value="5" type="number" min="0" max="100" class="form-control" id="exampleInputEmail1" placeholder="Zadajte podiel v pecentách pre zodpovednú osobu">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Odhadovaný čas:</label>
                                    <input name="odhadovany_cas" id="odhadcas" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte odhadovany cas">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Hodnota zákazky za ks (predajná cena):</label>
                                    <input name="hodnota_zakazky_za_ks" type="number" min="0"  step="0.01" class="form-control "  id="hodnota_zakazky_za_ks" placeholder="Zadajte hodnotu za ks" >
                                </div>
                                <div class="form-group">
                                    <label>Termín odovzdania zákazky:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="datumodovzdania" type='text'  class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>

                                </div>
                                <input type="hidden" id="stav_zakazky" name="stav_zakazky" value="1">



                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
            <script src="/js/select2.min.js"></script>

            <script type="text/javascript">

                $("#zodposovaid").select2({
                    placeholder: "Zodpovednu osobu",
                    allowClear: true
                });
            </script>
            <script type="text/javascript">

                $("#product_id").select2({
                    placeholder: "Vyberte nazov zakazky",
                    allowClear: true
                });
            </script>




            <script type="text/javascript">
                $('.date').datepicker({
                    //format: 'dd-mm-yyyy'
                    format: 'yyyy-mm-dd'
                });

            </script>

            <script>
                window.onload = nastavenie();
                function nastavenie() {
                    var udaj = document.getElementById("contractstype_id").value;


                    if (udaj == 2)
                    {
                        document.getElementById('hodnota_zakazky_za_ks').readOnly = false;
                        document.getElementById('clientid').readOnly = false;
                        $('#clientid').css('pointer-events','auto');
                        document.getElementById("product_id").disabled = true;
                        document.getElementById("produkt").style.display = "none";
                        document.getElementById("hodnota_zakazky_za_ks").value = null;
                        document.getElementById("hiddenelement").disabled  = false;
                        document.getElementById("hiddenelement").value = 1;
                        document.getElementById("popis").required =true;


                    }
                    else
                    {
                        document.getElementById('hodnota_zakazky_za_ks').readOnly = true;
                        document.getElementById('clientid').readOnly = true;
                        $('#clientid').css('pointer-events','none');
                        document.getElementById("product_id").disabled = false;
                        document.getElementById("produkt").style.display = "block";
                        document.getElementById("hiddenelement").disabled  = true;
                        document.getElementById("popis").required = false;
                    }
                }

            </script>

    <script>
        window.onload = defHodnota();

        function defHodnota() {
            var hodnota = document.getElementById("product_id").value;

            var sampleTags = [];
            @foreach ($products as $product)
            sampleTags.push('{{ $product->id}}');
            sampleTags.push('{{ $product->client_id}}');


            if (hodnota == sampleTags[0] ) {
                sampleTags.push('{{ $product->cena__bez_dph}}');
                document.getElementById('hodnota_zakazky_za_ks').readOnly = false;
                document.getElementById('clientid').readOnly = false;
                document.getElementById("hodnota_zakazky_za_ks").value=sampleTags[2];
                document.getElementById("clientid").value=sampleTags[1];
                document.getElementById('hodnota_zakazky_za_ks').readOnly = true;
                document.getElementById('clientid').readOnly = true;
                $('#clientid').css('pointer-events','none');


            }
            sampleTags = [];

            @endforeach
        }
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

    </script>



@endsection
