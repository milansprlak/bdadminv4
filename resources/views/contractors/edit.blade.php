@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Upraviť dodávateľa</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('contractors.update',[$contractor->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Obchodné meno:</label>
                                    <input name="nazov_firmy" value="{{$contractor->nazov_firmy}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte obchodné meno">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kontaktná osoba:</label>
                                    <input name="kontaktna_osoba" value="{{$contractor->kontaktna_osoba}}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte kontaktnú osobu">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">IČO:</label>
                                    <input name="ico" type="text" value="{{$contractor->ico}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte ičo">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">DIČ:</label>
                                    <input name="dic" type="text" value="{{$contractor->dic}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte dič">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">IČ DPH:</label>
                                    <input name="icdph" type="text" value="{{$contractor->icdph}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte IČ DPH">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Adresa:</label>
                                    <input name="fa_adresa" type="text" value="{{$contractor->fa_adresa}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte adresu">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mesto:</label>
                                    <input name="fa_mesto" type="text" value="{{$contractor->fa_mesto}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte mesto">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">PSČ:</label>
                                    <input name="fa_psc" type="text" value="{{$contractor->fa_psc}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte psč">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Krajina</label>
                                    <input name="fa_krajina" type="text" value="{{$contractor->fa_krajina}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte krajinu">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Telefón:</label>
                                    <input name="telefon" type="tel" value="{{$contractor->telefon}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte telefón">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mobil:</label>
                                    <input name="mobil" type="tel" value="{{$contractor->mobil}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte mobil">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-MAIL:</label>
                                    <input name="email" type="email" value="{{$contractor->email}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">WWW stránka:</label>
                                    <input name="www_site" type="url" value="{{$contractor->www_site}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte www stránku">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Názov banky:</label>
                                    <input name="nazov_banky" type="text" value="{{$contractor->nazov_banky}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte názov banky">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">IBAN:</label>
                                    <input name="iban" type="text" value="{{$contractor->iban}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte IBAN">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">SWIFT:</label>
                                    <input name="swift" type="text" value="{{$contractor->swift}}" class="form-control" id="exampleInputEmail1" placeholder="Zadajte SWIFT">
                                </div>
                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>



@endsection
