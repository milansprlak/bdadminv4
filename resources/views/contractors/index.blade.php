@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <a href="/contractors/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať nového dodávateľa</a>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam dodávateľov</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo:</th>
                                    <th>Obchodné meno:</th>
                                    <th>Kontaktná osoba:</th>
                                    <th>IČO:</th>
                                    <th>DIČ:</th>
                                    <th>IČDPH:</th>
                                    <th>Adresa:</th>
                                    <th>Mesto:</th>
                                    <th>PSČ:</th>
                                    <th>Krajina</th>
                                    <th>Telefón:</th>
                                    <th>Mobil:</th>
                                    <th>E-MAIL:</th>
                                    <th>WWW:</th>
                                    <th>Názov banky:</th>
                                    <th>IBAN:</th>
                                    <th>SWIFT:</th>
                                    <th>Editácia</th>



                                </tr>
                                <div style="display: none;">
                                    {{ $i=1 }}
                                </div>

                                @foreach ($contractors as  $contractor)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $contractor->nazov_firmy}}</td>
                                        <td>{{ $contractor->kontaktna_osoba}}</td>
                                        <td>{{ $contractor->ico}}</td>
                                        <td>{{ $contractor->dic}}</td>
                                        <td>{{ $contractor->icdph}}</td>
                                        <td>{{ $contractor->fa_adresa}}</td>
                                        <td>{{ $contractor->fa_mesto}}</td>
                                        <td>{{ $contractor->fa_psc}}</td>
                                        <td>{{ $contractor->fa_krajina}}</td>
                                        <td>{{ $contractor->telefon}}</td>
                                        <td>{{ $contractor->mobil}}</td>
                                        <td>{{ $contractor->email}}</td>
                                        <td>{{ $contractor->www_site}}</td>
                                        <td>{{ $contractor->nazov_banky}}</td>
                                        <td>{{ $contractor->iban}}</td>
                                        <td>{{ $contractor->swift}}</td>
                                        <td><a href="/contractors/{{ $contractor->id }}/edit">Editovať</a></td>
                                    </tr>


                                @endforeach

                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection