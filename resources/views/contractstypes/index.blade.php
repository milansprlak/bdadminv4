@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <a href="/contracts_types/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať typ zákazky</a>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Typy zákazky</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo</th>
                                    <th>Názov typu zákazky</th>
                                    <th>Editácia</th>



                                </tr>
                                <div style="display: none;">
                                    {{ $i=1 }}
                                </div>

                                @foreach ($contracts_types as $contracts_type)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $contracts_type->nazov_typu }}</td>
                                        <td><a href="/contracts_types/{{ $contracts_type->id }}/edit">Editovať</a></td>

                                    </tr>
                                @endforeach

                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection