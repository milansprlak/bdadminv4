@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam aktuálnych pripomienok</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Pripomienka</th>
                                    <th>Dátum udalosti</th>
                                    <th>Upozornenie od</th>
                                    <th>Opakovanie za</th>
                                    <th>Upozornenie pred</th>
                                    <th>Editácia</th>


                                </tr>
                                @if (!empty($reminders ))
                                @foreach ($reminders as $reminder)
                                        <tr>
                                            <td>{{$reminder['pripomienka']}}</td>
                                            <td>{{($reminder['datum'] ? date('d.m.Y', strtotime($reminder['datum'])): '')}}</td>
                                            <td>{{($reminder['datumod'] ? date('d.m.Y', strtotime($reminder['datumod'])): '')}}</td>
                                            <td>
                                                {{$reminder['opakovanie_num']}}
                                                @switch($reminder['opakovanie_val'])
                                                    @case('0')
                                                    Neopakuje sa
                                                    @break
                                                    @case(1)
                                                    dní
                                                    @break
                                                    @case(2)
                                                    týždne
                                                    @break
                                                    @case(3)
                                                    mesiace
                                                    @break
                                                    @case(4)
                                                    roky
                                                    @break
                                                    @default
                                                @endswitch
                                            </td>
                                            <td>
                                                {{$reminder['pripomenutie_num']}}
                                                @switch($reminder['pripomenutie_val'])
                                                    @case(1)
                                                    dní
                                                    @break
                                                    @case(2)
                                                    týždne
                                                    @break
                                                    @case(3)
                                                    mesiace
                                                    @break
                                                    @default
                                                @endswitch
                                            </td>
                                            <td><a href="/reminders/{{ $reminder['id'] }}/edit">Editovať</a></td>
                                        </tr>
                                    @endforeach
                                    @endif


                                @if (!empty($reminders_rep))
                                    @foreach ($reminders_rep as $reminder2)
                                        <tr>
                                            <td>{{$reminder2['pripomienka']}}</td>
                                            <td>{{($reminder2['datumnew'] ? date('d.m.Y', strtotime($reminder2['datumnew'])): '')}}</td>
                                            <td>{{($reminder2['datumod'] ? date('d.m.Y', strtotime($reminder2['datumod'])): '') }}</td>
                                            <td>
                                                {{$reminder2['opakovanie_num']}}
                                                @switch($reminder2['opakovanie_val'])
                                                    @case('0')
                                                    Neopakuje sa
                                                    @break
                                                    @case(1)
                                                    dní
                                                    @break
                                                    @case(2)
                                                    týždne
                                                    @break
                                                    @case(3)
                                                    mesiace
                                                    @break
                                                    @case(4)
                                                    roky
                                                    @break

                                                @endswitch
                                            </td>
                                            <td>
                                                {{$reminder2['pripomenutie_num']}}
                                                @switch($reminder2['pripomenutie_val'])
                                                    @case(1)
                                                    dní
                                                    @break
                                                    @case(2)
                                                    týždne
                                                    @break
                                                    @case(3)
                                                    mesiace
                                                    @break
                                                @endswitch

                                            </td>
                                            <td><a href="/reminders/{{ $reminder2['id']}}/edit">Editovať</a></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </table>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div><!-- /.col -->



        </div><!-- /.row -->

    @endsection