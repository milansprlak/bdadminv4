@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Vytvoriť pripomienku</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{ route('reminders.store') }}" role="form">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Zadajte pripomienku:</label>
                                    <input name="pripomienka" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte pripomienku">
                                </div>
                                <div class="form-group">
                                    <label>Stav:</label>
                                    <select name="stavpripomienky" class="form-control">
                                        <option value="1">Aktívna</option>
                                        <option value="0">Neaktívna</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Dátum:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="terminpripomienky" type='text'  class="form-control" />
                                        <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Interval opakovania:</label>
                                    <input type="number" name="opakovanie_num" min="0" max="100" step="1" value="1" class="form-control">
                                    <select name="opakovanie_val" class="form-control">
                                        <option value="0">Neopakuje sa</option>
                                        <option value="1">Dní</option>
                                        <option value="2">Týždne</option>
                                        <option value="3">Mesiace</option>
                                        <option value="4">Roky</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Upozornenie pred udalosťou:</label>
                                    <input type="number" name="upozornenie_num" min="0" max="100" step="1" value="1" class="form-control">
                                    <select name="upozornenie_val" class="form-control">
                                        <option value="1">Dni</option>
                                        <option value="2">Týždne</option>
                                        <option value="3">Mesiace</option>
                                    </select>
                                </div>


                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
            <script type="text/javascript">
                $('.date').datepicker({
                    //format: 'dd-mm-yyyy'
                    format: 'yyyy-mm-dd'
                });

            </script>


@endsection
