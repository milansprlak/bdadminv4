@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <a href="/status_invoice/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať stav faktúry</a>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Stavy faktúr</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo</th>
                                    <th>Názov stavu faktúry</th>
                                    <th>Editácia</th>



                                </tr>
                                <div style="display: none;">
                                    {{ $i=1 }}
                                </div>

                                @foreach ($invoice_statuses as $invoice_status)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $invoice_status->nazov_stavu }}</td>
                                        <td><a href="/status_invoice/{{ $invoice_status->id }}/edit">Editovať</a></td>

                                    </tr>
                                @endforeach

                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection