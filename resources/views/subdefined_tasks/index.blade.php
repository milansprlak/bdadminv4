@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <a href="/subdefinedtasks/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať podkategóriu</a>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam podkategórii</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo</th>
                                    <th>Názov podkategórie</th>
                                    <th>Názov nadradenej kategórie</th>
                                    <th>Cena materiálu za 1 kg</th>
                                    <th>Editácia</th>



                                </tr>
                                <div style="display: none;">
                                    {{ $i=1 }}
                                </div>

                                @foreach ($subdefinedTasks as $subdefinedTask)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $subdefinedTask->nazov }}</td>
                                        <td>
                                            @foreach($definedTasks as $definedTask)
                                                @if($subdefinedTask->nadradena_kat_id==$definedTask->id)
                                                    {{$definedTask->nazov}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{ $subdefinedTask->cena_material_kg }} €</td>
                                        <td><a href="/subdefinedtasks/{{ $subdefinedTask->id }}/edit">Editovať</a></td>

                                    </tr>
                                @endforeach

                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection