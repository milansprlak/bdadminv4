@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Upraviť podkategóriu</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('subdefinedtasks.update',[$subdefinedTask->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Názov podkategórie</label>
                                    <input name="nazov" value="{{$subdefinedTask->nazov}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte názov podkategórie">
                                </div>
                                <div class="form-group">
                                    <label>Nadradená kategória:</label>
                                    <select name="nadradena_kat_id" class="form-control">
                                        @foreach ($definedTask as $definedTask2)
                                            @if($definedTask2->id == $subdefinedTask->nadradena_kat_id)
                                                <option value="{{$definedTask2->id}}" selected>{{$definedTask2->nazov}}</option>
                                            @else
                                                <option value="{{$definedTask2->id}}" >{{$definedTask2->nazov}}</option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputCenaMaterialu">Cena materiálu za 1 kg</label>
                                    <input name="cena_material_kg" value="{{$subdefinedTask->cena_material_kg}}" type="number" step="0.01" min="0" required class="form-control" id="exampleInputCenaMaterialu" placeholder="Zadajte cenu materiálu za 1kg">
                                </div>

                             </div>

                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>


@endsection
