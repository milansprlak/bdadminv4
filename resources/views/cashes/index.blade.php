@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <a href="/cash/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať nový záznam</a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Pokladňa</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('cash.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class='form-group col-md-3'>
                                    <label>Časové obdobie od:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="fromdatum" type='text' value="{{$fromdatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                                <div class='form-group col-md-3'>
                                    <label>Časové obdobie do:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="todatum" type='text' value="{{$todatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                                <div id="buttony" class="col-md-3">
                                    <br>
                                    <button type="submit" class="btn btn-primary">Zobraziť</button>
                                </div>

                                <!-- /.box-body -->
                            </form>
                            <div class='col-md-12'>
                            <h3 class="box-title">Štatistiky</h3>

                            <table id="example2" class="table table-bordered table-striped">
                                <tr>
                                    <th>Účet:</th>
                                    <th>Stav účtu:</th>
                                </tr>
                                <tr>
                                    <td>Účet 1:</td>
                                    <td>{{number_format($sum_1ucet,2,',',' ')}} €</td>

                                </tr>
                                <tr>
                                    <td>Účet 2:</td>
                                    <td>{{number_format($sum_2ucet,2,',',' ')}} €</td>
                                </tr>
                                <tr>
                                    <td>Spolu:</td>
                                    <td>{{number_format($celkova_suma,2,',',' ')}} €</td>
                                </tr>

                            </table>

                            <h3 class="box-title">Finančné operácie</h3>

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Dátum</th>
                                    <th>Typ</th>
                                    <th>Poznámka</th>
                                    <th>Suma</th>
                                    <th>Editácia</th>
                                </tr>
                                @foreach ($cashes as $cashes2)
                                    @if (($cashes2->typ == 1)&&($cashes2->suma < 0))
                                        <tr>
                                            <td>{{ $cashes2->created_at->format('d.m.Y H:i')}}</td>

                                            @if($cashes2->typ == 1)
                                                <td>Účet 1</td>
                                            @else
                                                <td>Účet 2</td>
                                            @endif
                                            <td>{{ $cashes2->poznamka}}</td>

                                            <td>{{ $cashes2->suma}} €</td>
                                            @if ($cashes2->created_at->isToday())
                                                <td><a href="/cash/{{ $cashes2->id }}/edit">Editovať</a></td>
                                            @endif

                                        </tr>
                                    @elseif (($cashes2->typ != 1)&&($cashes2->suma < 0))
                                        <tr>
                                            <td><span style="color: red">{{ $cashes2->created_at->format('d.m.Y H:i')}}</span></td>

                                            @if($cashes2->typ == 1)
                                                <td><span style="color: red">Účet 1</span></td>
                                            @else
                                                <td><span style="color: red">Účet 2</span></td>
                                            @endif
                                            <td><span style="color: red">{{ $cashes2->poznamka}}</span></td>

                                            <td><span style="color: red">{{ $cashes2->suma}} €</span></td>
                                            @if ($cashes2->created_at->isToday())
                                                <td><a href="/cash/{{ $cashes2->id }}/edit">Editovať</a></td>
                                            @endif

                                        </tr>
                                    @elseif (($cashes2->typ == 1)&&($cashes2->suma > 0))
                                        <tr>
                                            <td><strong>{{ $cashes2->created_at->format('d.m.Y H:i')}}</strong></td>

                                            @if($cashes2->typ == 1)
                                                <td><strong>Účet 1</strong></td>
                                            @else
                                                <td><strong>Účet 2</strong></td>
                                            @endif
                                            <td><strong>{{ $cashes2->poznamka}}</strong></td>

                                            <td><strong>{{ $cashes2->suma}} €</strong></td>
                                            @if ($cashes2->created_at->isToday())
                                                <td><a href="/cash/{{ $cashes2->id }}/edit">Editovať</a></td>
                                            @endif

                                        </tr>
                                    @else
                                        <tr>
                                            <td><span style="color: red"><strong>{{ $cashes2->created_at->format('d.m.Y H:i')}}</strong></span></td>

                                            @if($cashes2->typ == 1)
                                                <td><span style="color: red"><strong>Účet 1</strong></span></td>
                                            @else
                                                <td><span style="color: red"><strong>Účet 2</strong></span></td>
                                            @endif
                                            <td><span style="color: red"><strong>{{ $cashes2->poznamka}}</strong></span></td>

                                            <td><span style="color: red"><strong>{{ $cashes2->suma}} €</strong></span></td>
                                            @if ($cashes2->created_at->isToday())
                                                <td><a href="/cash/{{ $cashes2->id }}/edit">Editovať</a></td>
                                            @endif

                                        </tr>
                                    @endif


                                @endforeach

                            </table>
                            {{ $cashes->appends(['fromdatum' => $fromdatum,'todatum'=> $todatum])->links() }}

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>
@endsection