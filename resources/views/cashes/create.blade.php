@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pridať záznam</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{ route('cash.store') }}" role="form">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Vyberte číslo účtu:</label>
                                    <select name="typuctu" class="form-control">
                                            <option value="1">Účet 1</option>
                                            <option value="2">Účet 2</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Vyberte operáciu:</label>
                                    <select name="operacia" class="form-control">
                                        <option value="2">Výdaj</option>
                                        <option value="1">Vklad</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poznámka</label>
                                    <input name="poznamka" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte poznamku">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputSuma">Suma</label>
                                    <input name="suma" type="text" required class="form-control" id="exampleInputSuma" placeholder="Zadajte sumu.">
                                </div>

                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>


@endsection
