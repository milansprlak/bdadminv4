@extends('admin_template')

@section('content')
    <style>
        select{
            height: 46px;
            line-height: 46px;
            border-radius: 0;
            box-shadow: none;
            border-color: #d2d6de;
            padding: 10px 16px;
            font-size: 18px;
            display: block;
            width: 100%;
        }
        input{
            height: 46px;
            line-height: 46px;
            border-radius: 0;
            box-shadow: none;
            border-color: #d2d6de;
            padding: 10px 16px;
            font-size: 18px;
            display: block;
            width: 100%;
        }
    </style>
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Upraviť činnosti</h3>
                        </div>
                        <!-- /.box-header -->

                        <!-- form start -->
                        <form  method="post" action="{{route('tasks.update',[$task->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="tab"><h2>Vyberte Vaše meno:</h2>
                                    <p> <select multiple class="form-control input-lg" name="employy_id" oninput="this.className = ''">
                                            @foreach($employy as $employy2)
                                                @if($employy2->id == $task->employy_id)
                                                    <option value="{{$employy2->id}}" selected>{{$employy2->meno}}</option>
                                                @else
                                                    <option value="{{$employy2->id}}">{{$employy2->meno}}</option>
                                                @endif
                                            @endforeach
                                        </select></p>

                                </div>
                                <div class="form-group">
                                    <label>Nadradená kategória:</label>
                                    <select name="defined_task_id" id="taskselect"  onchange="defHodnota()" class="form-control">
                                        @foreach ($defined_task as $definedTask2)
                                            @if($definedTask2->id == $task->defined_task_id)
                                                <option value="{{$definedTask2->id}}" selected>{{$definedTask2->nazov}}</option>
                                            @else
                                                <option value="{{$definedTask2->id}}" >{{$definedTask2->nazov}}</option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                                <div id="subtasks" ></div><br>
                                <p><input id="poznamka" value="{{$task->poznamka}}" style="display: none" name="poznamka" type="text" placeholder="Zadajte vlastný názov kategórie" oninput="this.className = ''"></p>
                                <p><input id="ostatne_cena_za_ks" value="{{$task->cena_material_kg}}" style="display: none" name="ostatne_cena_za_ks" type="text" placeholder="Cena za 1 ks" oninput="this.className = ''"></p>
                                <p><input id="pocet_ks_material" value="{{$task->pocet_ks_material}}" style="display: none" name="pocet_ks_material" type="number" placeholder="Zadajte hmotnosť materiálu v kg" oninput="this.className = ''"></p>

                                <div class="tab"><h2>Počet ks:</h2>
                                    <p><input name="pocet_ks" value="{{$task->pocet_ks}}" id="pocet_ks"  type="number" min="1" placeholder="Počet ks"  class="required" oninput="this.className = ''" ></p>

                                </div>

                                <div class="tab"><h2>Zadajte čas:</h2>
                                    @if(Auth::check()) <p><input name="datum" value="{{$task->datum}}" type="date" placeholder="Dátum..." class="required" oninput="this.className = ''"></p>@endif
                                    <p><input name="trvanie_hod" value="{{$task->trvanie_hod}}" type="number" placeholder="Hodiny..." class="required" oninput="this.className = ''"></p>
                                    <p><input name="trvanie_min" value="{{$task->trvanie_min}}" type="number" placeholder="Minúty..." class="required" oninput="this.className = ''"></p>

                                    @foreach ($contract as $contract2)
                                        @if($contract2->id == $task->contract_id)
                                            <p><input name="contract_id" type="hidden" value="{{ $contract2->id }}" oninput="this.className = ''"></p>
                                        @endif
                                    @endforeach
                                </div>

                             </div>

                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

    <script>
        window.onload = defHodnota();
        function defHodnota() {
            var hodnota = document.getElementById("taskselect").value;
            // document.getElementById("demo").innerHTML = "You selected: " + hodnota;
            //19_7_2018

            el = document.getElementById('taskselect')
            selectedText = el.options[el.selectedIndex].text

            if (selectedText == 'Ostatné') {
                document.getElementById('poznamka').style.display = 'block';
                document.getElementById('ostatne_cena_za_ks').style.display = 'block';
            }
            else {
                document.getElementById('poznamka').style.display = 'none';
                document.getElementById('ostatne_cena_za_ks').style.display = 'none';
            }
            //

            var sampleTags = [];
            var sampleTags2 = [];
            var podkategoria = 0;
            var podkategoria_id = 0;
            @foreach ($subdefined_task as $subdefined_task2)
            sampleTags.push('{{ $subdefined_task2->id}}');
            sampleTags.push('{{ $subdefined_task2->nazov}}');
            sampleTags.push('{{ $subdefined_task2->nadradena_kat_id}}');
            //    console.log(sampleTags[2]);
            if (hodnota == sampleTags[2]) {
                podkategoria = 1;
                podkategoria_id =sampleTags[2];
            }
            sampleTags2.push([sampleTags]);
            sampleTags = [];

            @endforeach

            if (podkategoria == 1)
            {

                //  document.getElementById("demo").innerHTML = "Kategoria obsahuje podkategoriu. ID hlavnej kategorie je "+podkategoria_id;
                subTasks(hodnota);
                document.getElementById('pocet_ks_material').style.display = 'block';
            }
            else {

                // document.getElementById("demo").innerHTML = "Kategoria neobsahuje podkategoriu";
                document.getElementById("subtasks").innerHTML = '';
                document.getElementById('pocet_ks_material').style.display = 'none';
                document.getElementById('pocet_ks_material').value = "";
            }


        }

        function subTasks(idkat) {
            document.getElementById("subtasks").innerHTML = '';
            var myDiv = document.getElementById("subtasks");
            var subtasks = [];
            var subtasks2 = [];


            var selectList = document.createElement("select");
            selectList.setAttribute("id", "mySelect");
            selectList.setAttribute("text", "Vyberte material");
            selectList.setAttribute("name", "subdefined_task_id");
            myDiv.appendChild(selectList);


            @foreach ($subdefined_task as $subdefined_task2)


            subtasks.push('{{ $subdefined_task2->id}}');
            subtasks.push('{{ $subdefined_task2->nazov}}');
            subtasks.push('{{ $subdefined_task2->nadradena_kat_id}}');


            if (idkat == subtasks[2])
            {

                var option = document.createElement("option");
                option.setAttribute("value", subtasks[0]);
                option.text = subtasks[1];
                //
                 if (subtasks[0]=='{{$task->subdefined_task_id}}')
                 {
                     option.selected="0";
                 }
                selectList.appendChild(option);
            }
            subtasks2.push([subtasks]);
            subtasks = [];

            @endforeach
            //    console.log(subtasks2);


        }
    </script>


@endsection
