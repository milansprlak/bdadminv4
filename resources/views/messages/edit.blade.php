@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Upraviť poznámku</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('messages.update',[$message->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="form-group">

                                    <label for="exampleInputEmail1">Meno</label>
                                    <input name="sprava" value="{{$message->sprava}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte poznamku">
                                </div>
                                <div class="form-group">
                                    <label>Stav správy:</label>
                                    <select id="stav" name="stav"  class="form-control">
                                        @if($message->stav == 1)
                                            <option value="1" selected>Aktívne</option>
                                            <option value="2">Neaktívne</option>
                                        @else
                                            <option value="1">Aktívne</option>
                                            <option value="2" selected>Neaktívne</option>
                                        @endif

                                    </select>
                                </div>

                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>



@endsection
