@extends('admin_template')


@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <a href="/messages/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať novú správu</a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Správy</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('messages.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class="form-group">
                                    <label>Stav správy:</label>
                                    <select id="stav" name="stav"  class="form-control">
                                            @if($stav == 1)
                                                <option value="1" selected>Aktívne</option>
                                                <option value="2">Neaktívne</option>
                                            @else
                                            <option value="1">Aktívne</option>
                                                <option value="2" selected>Neaktívne</option>
                                            @endif


                                    </select>

                                    <button type="submit" class="btn btn-primary">Zobraziť</button>
                                </div>
                                <!-- /.box-body -->
                            </form>


                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Dátum</th>
                                    <th>Uživateľ</th>
                                    <th>Správa</th>
                                    <th>Stav</th>
                                    <th>Akcia</th>
                                </tr>
                                @foreach ($messages as $message)
                                        <tr>
                                            <td>{{ $message->created_at->format('d.m.Y H:i')}}</td>
                                            <td>{{ $message->uzivatel->name}}</td>
                                            <td>{{ $message->sprava}}</td>
                                            <td>@if($message->stav == 1) Aktívna @else Neaktívna @endif</td>
                                            <td>@if($message->stav == 1)
                                                <button href="#"  class="btn btn-success" onclick="document.getElementById('vyriesene-form-{{$message->id}}').submit();">Vyriešiť</button>/
                                                @endif
                                                 <button href="#"  class="btn btn-primary" onclick="window.location.href='/messages/{{ $message->id }}/edit'">Editovať</button>

                                                <form id="vyriesene-form-{{$message->id}}" action="{{ route('messages.update',[$message->id])}}"
                                                      method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="put">
                                                    <input type="hidden" name="changestatus" value="1">
                                                    <input type="hidden" name="message_id" value="{{$message->id}}">
                                                    <input type="hidden" name="status" value="2">

                                                </form>

                                            </td>


                                        </tr>
                                @endforeach

                            </table>
                            {{ $messages->appends(['stav' => $stav])->links() }}


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection