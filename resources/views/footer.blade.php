<div class="modal fade" id="pridatzakazkuModal"
     tabindex="-1" role="dialog"
     aria-labelledby="favoritesModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Pridať zákazku</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Zatvoriť</button>
                <span class="pull-right">
          <button type="button" class="btn btn-primary">
           Pridať
          </button>
        </span>
            </div>
        </div>
    </div>
</div>

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Vytvorené pre spoločnosť BEDRICH spol. s r.o.
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="https://aurelo.sk">Aurelo.sk</a>.</strong> All rights reserved.
</footer>
