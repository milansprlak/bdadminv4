@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Štatistiky</h3>
                <!--    <a href="{{ url()->previous() }}" class="btn btn-default">Späť</a> -->
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('statistics.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class="box-body">
                                    <div class='form-group col-md-2'>
                                    <label>Časové obdobie od:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="fromdatum" type='text' value="{{$fromdatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    </div>
                                    <div class='form-group col-md-2'>
                                        <label>Časové obdobie do:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="todatum" type='text' value="{{$todatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="exampleInputEmail1">Podiel na auto/frézu v percentách:</label>
                                        <input name="auto_freza" value="{{$auto_freza_perc}}" id="auto_freza" type="number" min="0" max="100" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte počet ks">
                                    </div>
                                    <div id="buttony" class="col-md-3">
                                        <br>
                                        <button type="submit" class="btn btn-primary">Zobraziť</button>
                                        <button onclick="vytlacit_stranku()" class="btn btn-primary">
                                            Vytlačiť stránku
                                        </button>

                                    </div>
                                </div>

                                <!-- /.box-body -->
                            </form>
                            <div>
                                <h2>Štatistika firmy</h2>
                                <p><small>Pozn.: Do štatistík vstupujú iba uzavreté zákazky</small></p>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Hodnota:</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Počet ukončených zákaziek:</th>
                                        <td>{{number_format($statistiky_pocet)}}</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">Celkový zisk (100%):</th>
                                        <td>{{number_format($statistiky_zisk,2,',',' ')}} €</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">Zisk pre firmu (67%) pred odpočítaním na auto/frézu:</th>
                                        <td>{{number_format($statistiky_zisk_firma_pred,2,',',' ')}} €</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">Zisk pre firmu po odpočítaní na auto/frézu:</th>
                                        <td>{{number_format($statistiky_zisk_firma,2,',',' ')}} €</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">Ušetrene peniaze na auto/frézu:</th>
                                        <td>{{number_format($auto_freza_value,2,',',' ')}} €</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">Zisk pre zamestnancov (33%):</th>
                                        <td>{{number_format($statistiky_zisk_zamestnanci,2,',',' ')}} €</td>

                                    </tr>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col"></th>
                                    </tr>
                                    @foreach($rezijne_statistiky as $rezijne_statistiky2)
                                        <tr>
                                            <th scope="row">Počet ukončených režijných zákaziek:</th>
                                            <td>{{$rezijne_statistiky2['pocet']}}</td>

                                        </tr>
                                        <tr>
                                            <th scope="row">Celkový čas režijných zákaziek:</th>
                                            <td>{{number_format($rezijne_statistiky2['hod'])}} hod {{number_format($rezijne_statistiky2['min'])}} min</td>

                                        </tr>
                                        <tr>
                                            <th scope="row">Celková hodnota režijných zákaziek:</th>
                                            <td>{{number_format($rezijne_statistiky2['hodnota'],2,',',' ')}} €</td>

                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>

                            </div>

                            <h2>Štatistika zamestnancov</h2>
                            <table id="example1" class="table table-hover">
                                <tr>
                                    <th>Meno:</th>
                                    <th>Odpracovaný čas:</th>
                                    <th>Hodnota:</th>
                                    <th>Odmena pre zodpovednú osobu:</th>
                                    <th>Odpracovaný čas na režijných nákladov:</th>
                                    <th>Hodnota režijných nákladov:</th>
                                    <th>Celkový odpracovaný čas:</th>
                                    <th>Celková hodnota:</th>
                                </tr>

                                @foreach ($zamestnanec as $zamestnanec2)
                                    <tr>
                                        <td><a href="/employees/{{ $zamestnanec2['idmeno']}}">  {{ $zamestnanec2['meno']}}</a></td>
                                        <td>{{number_format($zamestnanec2['hod'])}} hod {{number_format($zamestnanec2['min'])}} min</td>
                                        <td>{{number_format($zamestnanec2['hodnota'],2,',',' ')}} €</td>
                                        <td>{{number_format($zamestnanec2['bonus'],2,',',' ')}} €</td>

                                        <td>{{number_format($zamestnanec2['rezhod'])}} hod {{number_format($zamestnanec2['rezmin'])}} min</td>
                                        <td>{{number_format($zamestnanec2['rezhodnota'],2,',',' ')}} €</td>

                                        @if(($zamestnanec2['min']+$zamestnanec2['rezmin'])<60)
                                            <td>{{number_format($zamestnanec2['hod']+$zamestnanec2['rezhod'])}} hod {{number_format($zamestnanec2['min']+$zamestnanec2['rezmin'])}} min</td>

                                        @else
                                            <div style="display: none;">
                                                {{
                                                $celkove_hodiny = $zamestnanec2['hod']+$zamestnanec2['rezhod'],
                                                $celkove_min = $zamestnanec2['min']+$zamestnanec2['rezmin'],
                                                $pom_hod = floor($celkove_min / 60),
                                                $celkove_hodiny= $celkove_hodiny + $pom_hod,
                                                $pom_min = $pom_hod * 60,
                                                $celkove_min = $celkove_min - $pom_min
                                                }}
                                            </div>
                                            <td>{{number_format($celkove_hodiny)}} hod {{number_format($celkove_min) }} min</td>

                                        @endif
                                        <td>{{number_format($zamestnanec2['hodnota']+$zamestnanec2['bonus'],2,',',' ')}} €</td>





                                    </tr>
                                @endforeach

                            </table>



                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>
    <script>
        function vytlacit_stranku() {

            document.getElementById('buttony').style.display = 'none';
            window.print();
            document.getElementById('buttony').style.display = 'block';
        }
    </script>
@endsection