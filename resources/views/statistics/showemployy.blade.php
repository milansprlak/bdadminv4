@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Štatistika zamestnanca</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('employees.show',$employy->id) }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class='form-group col-md-3'>
                                    <label>Časové obdobie od:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="fromdatum" type='text' value="{{$fromdatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                                <div class='form-group col-md-3'>
                                    <label>Časové obdobie do:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="todatum" type='text' value="{{$todatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>

                                    <div id="buttony col-md-6">
                                        <br>

                                        <button type="submit" class="btn btn-primary">Zobraziť</button>
                                        <button onclick="vytlacit_stranku()" class="btn btn-primary">
                                            Vytlačiť stránku
                                        </button>

                                    </div>


                                <!-- /.box-body -->
                            </form>
                            <div class='col-md-12'>
                                <h2>{{$employy->meno}}</h2>

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Hodnota:</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Počet zákaziek:</th>
                                        <td>{{$pocet_zakazok}}</td>

                                    </tr>




                                    </tbody>
                                </table>

                            </div>

                            <h2>Zoznam zákaziek</h2>
                            <table id="example1" class="table table-hover">
                                <tr>
                                    <th>Poradové číslo:</th>
                                    <th>Názov:</th>

                                </tr>
                                @foreach ($zakazky as $zakazka)
                                    <tr>
                                        <td><a href="/contracts/{{ $zakazka['id'] }}">  {{ $zakazka['poradove_cislo']}}</a></td>
                                        <td><a href="/contracts/{{ $zakazka['id'] }}">  {{ $zakazka['meno'] }}</a></td>
                                    </tr>
                                @endforeach

                            </table>



                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>
    <script>
        function vytlacit_stranku() {

            document.getElementById('buttony').style.display = 'none';
            window.print();
            document.getElementById('buttony').style.display = 'block';
        }
    </script>
@endsection