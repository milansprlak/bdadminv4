@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>
    <script src="/js/jquery.flot.js"></script>
    <script src="/js/jquery.flot.resize.js"></script>
    <script src="/js/jquery.flot.categories.js"></script>
    <script src="/js/jquery.flot.barlabels.js"></script>

</head>

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Štatistiky</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('statisticsinvoice.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class="box-body">
                                    <div class="form-group col-md-2">
                                        <label for="exampleInputEmail1">Zadajte rok:</label>
                                        <input name="rok" type="number" min="2018" value="{{$rok}}" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte obchodné meno">
                                    </div>


                                    <div id="buttony" class="col-md-3">
                                        <br>
                                        <button type="submit" class="btn btn-primary">Zobraziť</button>
                                        <button onclick="vytlacit_stranku()" class="btn btn-primary">
                                            Vytlačiť stránku
                                        </button>

                                    </div>
                                </div>

                                <!-- /.box-body -->
                            </form>
                            <div>
                                <h2>Štatistika faktúr</h2>

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Hodnota:</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <th scope="row">Vyfakturované: </th>
                                        <td>{{number_format($vyfakturovane,2,',',' ')}} €</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">Zaplatené:</th>
                                        <td>{{number_format($uhhradene,2,',',' ')}} €</td>

                                    </tr>
                                    <tr>
                                        <th scope="row">Nezaplatené:</th>
                                        <td>{{number_format($neuhradene,2,',',' ')}} €</td>

                                    </tr>


                                    </tbody>
                                </table>

                            </div>
                            <!-- Graf -->
                            <div class="col-sm-8">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <i class="fa fa-bar-chart-o"></i>

                                        <h3 class="box-title">Mesačná štatistika</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div id="bar-chart" style="height: 300px;"></div>
                                    </div>
                                    <!-- /.box-body-->
                                </div>
                            </div>
                            <!-- Koniec Grafu-->



                         </div>

                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                </div><!-- /.box-body -->


            </div><!-- /.box -->

        </div><!-- /.col -->


    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>
    <script>
        function vytlacit_stranku() {

            document.getElementById('buttony').style.display = 'none';
            window.print();
            document.getElementById('buttony').style.display = 'block';
        }
    </script>
    <script>
        /*
 * BAR CHART
 * ---------
 */

        var bar_data = {
            data : [['Jan', {{($statistika->where('mesiac', '1')->first())==null ? 0: $statistika->where('mesiac', '1')->first()->total_cena}}],
                ['Feb', {{($statistika->where('mesiac', '2')->first())==null ? 0: $statistika->where('mesiac', '2')->first()->total_cena}}],
                ['Mar', {{($statistika->where('mesiac', '3')->first())==null ? 0: $statistika->where('mesiac', '3')->first()->total_cena}}],
                ['Apr', {{($statistika->where('mesiac', '4')->first())==null ? 0: $statistika->where('mesiac', '4')->first()->total_cena}}],
                ['Máj', {{($statistika->where('mesiac', '5')->first())==null ? 0: $statistika->where('mesiac', '5')->first()->total_cena}}],
                ['Jún', {{($statistika->where('mesiac', '6')->first())==null ? 0: $statistika->where('mesiac', '6')->first()->total_cena}}],
                ['Júl', {{($statistika->where('mesiac', '7')->first())==null ? 0: $statistika->where('mesiac', '7')->first()->total_cena}}],
                ['Aug', {{($statistika->where('mesiac', '8')->first())==null ? 0: $statistika->where('mesiac', '8')->first()->total_cena}}],
                ['Sept', {{($statistika->where('mesiac', '9')->first())==null ? 0: $statistika->where('mesiac', '9')->first()->total_cena}}],
                ['Okt', {{($statistika->where('mesiac', '10')->first())==null ? 0: $statistika->where('mesiac', '10')->first()->total_cena}}],
                ['Nov', {{($statistika->where('mesiac', '11')->first())==null ? 0: $statistika->where('mesiac', '11')->first()->total_cena}}],
                ['Dec', {{($statistika->where('mesiac', '12')->first())==null ? 0: $statistika->where('mesiac', '12')->first()->total_cena}}]],
            color: '#3c8dbc'
        }

        $.plot('#bar-chart', [bar_data], {
            grid  : {
                borderWidth: 1,
                borderColor: '#f3f3f3',
                tickColor  : '#f3f3f3'
            },
            series: {

                bars: {
                    show    : true,
                    barWidth: 0.5,
                    align   : 'center',

                }
            },
            xaxis : {
                mode      : 'categories',
                tickLength: 0
            }


        })


        /* END BAR CHART */



    </script>



@endsection