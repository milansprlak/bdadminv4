@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Štatistiky úloh</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('taskstatistics.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">

                                <div class="form-group col-md-3">
                                    <label>Prehľaď za:</label>
                                    <select id="obdobie_value" name="obdobie_value"  class="form-control">
                                        @if($definovane_obdobie == 0)
                                            <option value="0" selected>Posledný rok / Definované obdobie</option>
                                            <option value="1">Celkové obdobie</option>
                                        @else
                                            <option value="0">Posledný rok / Definované obdobie</option>
                                            <option value="1" selected>Celkové obdobie</option>
                                        @endif
                                    </select>
                                </div>


                                    <div class='form-group col-md-3'>
                                    <label>Časové obdobie od:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="fromdatum" type='text' value="{{$fromdatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    </div>
                                    <div class='form-group col-md-3'>
                                        <label>Časové obdobie do:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="todatum" type='text' value="{{$todatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    </div>

                                    <div id="buttony" class="col-md-3">
                                        <br>
                                        <button type="submit" class="btn btn-primary">Zobraziť</button>
                                        <button onclick="vytlacit_stranku()" class="btn btn-primary">
                                            Vytlačiť stránku
                                        </button>

                                    </div>


                                <!-- /.box-body -->
                            </form>

                            <div class='col-md-12'>
                            <h2>Štatistika úloh</h2>
                            <table id="example1" class="table table-hover">
                                <tr>
                                    <th>Názov úlohy:</th>
                                    <th>Celkový čas:</th>
                                    <th>Cena za činnosť:</th>

                                </tr>

                                @foreach ($task as $task2)
                                    <tr>
                                        <td>{{$task2['nazov_ulohy']}}</td>
                                        <td>{{number_format($task2['sum_celkovy_cas_hod'],0,',',' ')}} hod {{number_format($task2['sum_celkovy_cas_min'],0,',',' ')}} min</td>
                                        <td>{{number_format($task2['cena'],2,',',' ')}} €</td>
                                    </tr>
                                @endforeach

                            </table>


                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>
    <script>
        function vytlacit_stranku() {

            document.getElementById('buttony').style.display = 'none';
            window.print();
            document.getElementById('buttony').style.display = 'block';
        }
    </script>
@endsection