<!doctype html>
<html lang="sk">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>{{$priceoffer->cislo}}</title>

  <style type="text/css">
    * {
      font-family: DejaVu Sans;  !important;
    }
    table{
      font-size: x-small;
      page-break-inside:auto;
    }

    .hlavicka{
      font-size: 14px;

    }



    .box{
      background-color: #f5f5f5;
      border: 1px solid #e31e24;


      border-radius: 5px;
    }

    .boxpoznamka{
      background-color: #f5f5f5;
      border: 1px solid #b3b3b3;
    }

    .platobne{
      background-color: #f5f5f5;
      border: 1px solid #e3e3e3;


      border-radius: 3px;
    }
    hr{
      color: #f5f5f5;
      margin-top: -50px;
    }
    tfoot tr td{
      font-weight: bold;
      font-size: x-small;
    }
    td {
      vertical-align: top;
      page-break-inside:avoid;
      page-break-after:auto;
    }

    tr{
      page-break-inside:avoid;
      page-break-after:auto;

    }
    .zvyraznenie{
      background-color: #f5f5f5;
      font-size:9px;
      line-height: 14px;
      font-weight: bold;
    }

    .popis{
      line-height: 14px;
    }

    footer {
      position: fixed;
      bottom: -50px;
      left: 0px;
      right: 0px;
      height: 50px;
      font-size: x-small;
      text-align: center;
      line-height: 35px;
    }


  </style>

</head>
<body>


<table width="100%" >
  <tr>
    <td vertical-align="center"><img src="/img/logo_bedrich.png" alt="{{$priceoffer->contractor->nazov_firmy}}" height="40" width="100"></td>
    <td align="right" vertical-align="center" class="hlavicka">
      CENOVÁ PONUKA č. {{$priceoffer->cislo}}
    </td>
  </tr>
</table>



  <br>
  <table width="100%">
    <tr>
      <td>
        Dodávateľ: <br>
      <address>
        <strong>{{$priceoffer->contractor->nazov_firmy}}<br>
        {{$priceoffer->contractor->fa_adresa}}<br>
        {{$priceoffer->contractor->fa_psc}} {{$priceoffer->contractor->fa_mesto}}</strong>
        <span style="font-size: 7px;"><br><br>
          IČO: {{$priceoffer->contractor->ico}}<br>
          DIČ: {{$priceoffer->contractor->dic}}<br>
          IČ DPH: {{$priceoffer->contractor->icdph}}<br>
          @if($priceoffer->contractor->telefon != null)
            Telefón: {{$priceoffer->contractor->telefon}}<br>
          @endif
          Mobil: {{$priceoffer->contractor->mobil}}<br>
          E-mail: {{$priceoffer->contractor->email}}<br>
          WEB: {{$priceoffer->contractor->www_site}}
        </span>
      </address>
    </td>


    <td>
      Dátum vytvorenia: {{($priceoffer->datum_vystavenia ? date('d.m.Y', strtotime($priceoffer->datum_vystavenia)): '')}} <br>
      Dátum splatnosti: {{($priceoffer->datum_splatnosti ? date('d.m.Y', strtotime($priceoffer->datum_splatnosti)): '')}} <br>

<div class="box">
      Odberateľ: <br>
      <address>
        <strong>{{$priceoffer->klient->nazov_firmy}}<br>
          {{$priceoffer->klient->fa_adresa}}<br>
          {{$priceoffer->klient->fa_psc}} {{$priceoffer->klient->fa_mesto}}</strong>

        <span style="font-size: 7px;"><br><br>
        @if($priceoffer->klient->ico != null)
            IČO: {{$priceoffer->klient->ico}}<br>
          @endif
          @if($priceoffer->klient->dic != null)
            DIČ: {{$priceoffer->klient->dic}}<br>
          @endif
          @if($priceoffer->klient->icdph != null)
            IČ DPH: {{$priceoffer->klient->icdph}}<br>
          @endif
          @if($priceoffer->email != null)
            Email: {{$priceoffer->email}}
          @endif



         </span>
      </address>
  <br>
</div>

    </td>


    </tr>

  </table>



<br/>

<table width="100%">
  <thead class="zvyraznenie">
  <tr class="zvyraznenie">
    <th width="45%">Názov položky</th>
    <th width="12%" align="right">Počet</th>
    <th width="18%" align="right">J. cena bez DPH</th>
    <th width="10%" align="right">DPH %</th>
    <th width="15%" align="right">Celkom s DPH</th>
  </tr>
  </thead>
  <tbody>
  @foreach ($priceoffer->polozky as $item) <!--// nacita tabulku Task priradenu k contract_id -->
  <tr>
    <th width="45%" scope="row">{{$item->nazov_polozky}}</th>
    <td width="12%" align="right">{{$item->pocet_ks}} {{$item->jednotka}}</td>
    <td width="18%" align="right">{{number_format($item->cena_bez_dph_ks,2,',',' ')}} &euro;</td>
    <td width="10%" align="right">{{$item->vyska_dph}} %</td>
    <td width="15%" align="right">{{number_format($item->celkova_cena,2,',',' ')}} &euro;</td>
  </tr>
  @endforeach


  </tbody>
  <br><br>
  <tfoot>
  <tr>
    <td colspan="3"></td>
    <td align="right">
      Celkom:</td>
    <td align="right">{{number_format($sum_celkova_bez_dph,2,',',' ')}} &euro;</td>
  </tr>
  <tr>
    <td colspan="3"></td>
    <td align="right">DPH:</td>
    <td align="right">{{number_format($sum_ciastka_dph,2,',',' ')}} &euro;</td>
  </tr>
  <tr>
    <td colspan="3"></td>
    <td align="right">Celková suma:</td>
    <td align="right" class="zvyraznenie">{{number_format($sum_celkova,2,',',' ')}} &euro;</td>
  </tr>
  </tfoot>
</table>
<br/><br/>
@if($priceoffer->poznamka != null)
<table width="100%" class="boxpoznamka">

  <tr>
    <th>Poznámka:</th>

  </tr>


  <tr>
    <td>
      {{$priceoffer->poznamka}}
    </td>


  </tr>

</table>
@endif
</body>
</html>