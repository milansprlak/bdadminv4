@extends('admin_template')


@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <h1>
               Cenová ponuka: {{$priceOffer->cislo}}
            </h1>

        </section>


        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-10">
                    <a href="/price_offers/{{$priceOffer->id}}/edit"  class="btn btn-primary">Editovať cenovú ponuku</a>
                </div>

                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> {{$priceOffer->contractor->nazov_firmy}}
                        <i class="pull-right">Cenová ponuka č.: {{$priceOffer->cislo}}</i>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    Dodávateľ
                    <address>
                        <strong>{{$priceOffer->contractor->nazov_firmy}}</strong><br>
                        {{$priceOffer->contractor->fa_adresa}}<br>
                        {{$priceOffer->contractor->fa_psc}} {{$priceOffer->contractor->fa_mesto}}<br><br>
                        IČO: {{$priceOffer->contractor->ico}}<br>
                        DIČ: {{$priceOffer->contractor->dic}}<br>
                        IČ DPH: {{$priceOffer->contractor->icdph}}<br>
                        @if($priceOffer->contractor->telefon != null)
                            Telefón: {{$priceOffer->contractor->telefon}}<br>
                        @endif
                        Mobil: {{$priceOffer->contractor->mobil}}<br>
                        E-mail: {{$priceOffer->contractor->email}}<br>
                        WEB: {{$priceOffer->contractor->www_site}}
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    Odberateľ:
                    <address>
                        <strong>{{$priceOffer->klient->nazov_firmy}}</strong><br>
                        {{$priceOffer->klient->fa_adresa}}<br>
                        {{$priceOffer->klient->fa_psc}} {{$priceOffer->klient->fa_mesto}}<br><br>
                        @if($priceOffer->klient->ico != null)
                            IČO: {{$priceOffer->klient->ico}}<br>
                        @endif
                        @if($priceOffer->klient->dic != null)
                            DIČ: {{$priceOffer->klient->dic}}<br>
                        @endif
                        @if($priceOffer->klient->icdph != null)
                            IČ DPH: {{$priceOffer->klient->icdph}}<br>
                        @endif
                        @if($priceOffer->email != null)
                            Email: {{$priceOffer->email}}
                        @endif
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    Dátum vytvorenia: {{($priceOffer->datum_vystavenia ? date('d.m.Y', strtotime($priceOffer->datum_vystavenia)): '')}} <br>
                    Dátum splatnosti: {{($priceOffer->datum_splatnosti ? date('d.m.Y', strtotime($priceOffer->datum_splatnosti)): '')}} <br>


                </div>
                <!-- /.col -->




            </div>


            <!-- add products /.row -->
            @if ($priceOffer->odoslane!=1)
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addproductModal" data-whatever="@mdo">Pridať položku</button>
            @endif
                <div class="modal fade" id="addproductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow:hidden;">
                <div class="modal-dialog" role="document">

                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="exampleModalLabel">Pridať položku do faktúry</h3>

                        </div>
                        <div class="modal-body">
                            <form id="addItem" method="post" action="{{ route('priceoffer_detail.store') }}" >
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Názov položky:</label>
                                    <input name="nazovpolozky" id="nazovpolozky"  type="text" class="form-control" id="recipient-name">
                                    <input name="priceoffer_id" id="priceoffer_id" style="display: none" value="{{$priceOffer->id}}" type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Počet ks:</label>
                                    <input type="number" step="0.01" min="0" value="1" name="pocet_ks" class="form-control" value="1" min="1" required id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Jednotka:</label>
                                    <input type="text" name="jednotka" id="jednotka" class="form-control" value="ks" required id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Cena bez DPH za ks:</label>
                                    <input type="number" step="0.01" name="cenabezdph" id="cenabezdph" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Výška DPH</label>
                                    <input type="number" name="vyskadph" id="vyskadph" value="20" min="0" max="100" class="form-control" id="recipient-name">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavrieť</button>
                                    <button type="submit" class="btn btn-primary">Vložiť položku</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal EDIT-->
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Editovanie položky</h4>
                        </div>
                        <form action="{{route('priceoffer_detail.update',"item_id")}}" method="post">
                            {{method_field('patch')}}
                            {{csrf_field()}}
                            <div class="modal-body">
                                <input type="hidden" name="item_id" id="item_id" value="">


                                <div class="form-group">
                                    <label for="recipient-name" id="editnazovpolozkylabel" class="col-form-label">Názov položky:</label>
                                    <input name="editnazovpolozky" id="editnazovpolozky"  type="text" class="form-control" id="recipient-name">
                                    <input name="editpriceoffer_id" id="editpriceoffer_id" style="display: none" value="{{$priceOffer->id}}" type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Počet ks:</label>
                                    <input type="number" step="0.01" name="editpocet_ks" id="editpocet_ks" class="form-control" value="1" min="1" required id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Jednotka:</label>
                                    <input type="text" name="editjednotka" id="editjednotka" class="form-control" value="ks" required id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Cena bez DPH za ks:</label>
                                    <input type="number" step="0.01" name="editcenabezdph" id="editcenabezdph" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Výška DPH</label>
                                    <input type="number" name="editvyskadph" id="editvyskadph" value="20" min="0" max="100" class="form-control" id="recipient-name">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Zavrieť</button>
                                <button type="submit" class="btn btn-primary">Uložiť zmeny</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Názov položky</th>
                            <th>Počet</th>
                            <th>J. cena bez DPH</th>
                            <th>DPH %</th>
                            <th>Celkom s DPH</th>
                            @if ($priceOffer->odoslane!=1)
                            <th>Akcia</th>
                            @endif

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        @foreach ($priceOffer->polozky as $item)

                            <tr>

                                <td>{{$item->nazov_polozky}}</td>
                                <td>{{$item->pocet_ks}} {{$item->jednotka}}</td>
                                <td>{{number_format($item->cena_bez_dph_ks,2,',',' ')}} €</td>
                                <td>{{$item->vyska_dph}} %</td>
                                <td>{{number_format($item->celkova_cena,2,',',' ')}} €</td>

                                @if ($priceOffer->odoslane!=1)
                                <td><button class="btn btn-info" data-nazov="{{$item->nazov_polozky}}" data-nazov_id="{{$item->stock_id}}" data-pocet_ks="{{$item->pocet_ks}}" data-jednotka="{{$item->jednotka}}" data-cena="{{$item->cena_bez_dph_ks}}" data-vyskadph="{{$item->vyska_dph}}" data-item_id={{$item->id}} data-toggle="modal" data-target="#edit">Editovať</button>
                                    /
                                    <button href="#"  class="btn btn-danger" onclick="
                                            var result = confirm('Skutočne odstrániť polžku z cenovej ponuky?');
                                            if( result ){
                                            event.preventDefault();

                                            document.getElementById('delete-form-{{$item->id}}').submit();
                                            }
                                            ">Odstrániť</button>
                                    <form id="delete-form-{{$item->id}}" action="{{ route('priceoffer_detail.destroy',[$item->id])}}"
                                          method="POST" style="display: none;">
                                        <input type="hidden" name="_method" value="delete">
                                        <input type="hidden" name="priceoffer_id" value="{{$priceOffer->id}}">
                                        {{ csrf_field() }}
                                    </form>

                                </td>
                                @endif


                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->

            <div class="col-xs-6 text-muted well well-sm no-shadow">
                <p class="lead">Poznámka:</p>
                <div class="col-xs-6">
                    {{$priceOffer->poznamka}}
                </div>


            </div>




            <div class="row">
                <!-- accepted payments column -->

                <!-- /.col -->
                <div  class="col-xs-6 pull-right">


                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">Základ DPH: </th>
                                <td>{{number_format($sum_celkova_bez_dph,2,',',' ')}} €</td>
                            </tr>
                            <tr>
                                <th>Výška DPH: </th>
                                <td>{{number_format($sum_ciastka_dph,2,',',' ')}} €</td>
                            </tr>
                            <tr>
                                <th>Celková suma:</th>
                                <td>{{number_format($sum_celkova,2,',',' ')}} €</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">

                    @if ($priceOffer->odoslane==0)
                    <button type="button" class="btn btn-success pull-right" onclick="document.getElementById('odoslana-form-{{$priceOffer->id}}').submit();"><i class="fa fa-envelope "></i> Odoslať</button>
                        <form id="odoslana-form-{{$priceOffer->id}}" action="{{ route('price_offers.update',[$priceOffer->id])}}"
                              method="POST" style="display: none;">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <input type="hidden" name="odoslat" value="1">
                        </form>
                    @else
                    @endif
                    <a href="/price_offers/getPDF/{{$priceOffer->id}}" class=" btn btn-primary pull-right" role="button" style="margin-right: 5px;"><i class="fa fa-download"></i> Generuj PDF</a>

                </div>
            </div>
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>



<!-- ./wrapper -->


@endsection