@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Editovať cenovú ponuku</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('price_offers.update',[$priceoffer->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Vyberte klienta:</label>
                                    <select name="client_id" id="client_id"  required class="form-control">
                                        @foreach ($clients as $client)
                                            @if($client->id == $priceoffer->client_id)
                                                <option value="{{$client->id}}" selected>{{$client->nazov_firmy}}</option>
                                            @else
                                                <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Vyberte dodávateľa:</label>
                                    <select name="contractor_id" id="contractor_id" required class="form-control">
                                        @foreach ($invoice_contractors as $invoice_contractor)
                                            <option value="{{$invoice_contractor->id}}">{{$invoice_contractor->nazov_firmy}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poradové číslo faktúry</label>
                                    <input name="poradove_cislo" value="{{$priceoffer->cislo}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte poradové číslo faktúry">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Dátum vytvorenia:</label>
                                    <input name="datum_vystavenia" value="{{$priceoffer->datum_vystavenia}}" type="date" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte dátum vytvorenia">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Dátum splatnosti:</label>
                                    <input name="datum_splatnosti" value="{{$priceoffer->datum_splatnosti}}" type="date" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte dátum splatnosti">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mailová adresa:</label>
                                    <input name="email" type="email" value="{{$priceoffer->email}}" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte správu">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poznámka</label>
                                    <input name="poznamka" value="{{$priceoffer->poznamka}}" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte správu">
                                </div>
                                <div class="form-group">
                                    <label>Stav:</label>
                                    <select name="odoslane" id="odoslane" required class="form-control">
                                        @if ($priceoffer->odoslane == 0)
                                             <option value="0" selected>Nová</option>
                                             <option value="1">Odoslaná</option>
                                        @else
                                            <option value="0">Nová</option>
                                            <option value="1" selected>Odoslaná</option>
                                        @endif

                                    </select>
                                </div>

                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>




@endsection
