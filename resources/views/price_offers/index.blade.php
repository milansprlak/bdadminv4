@extends('admin_template')


@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <a href="/price_offers/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Vytvoriť cenovú ponuku</a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cenové ponuky</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                           <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Dátum</th>
                                    <th>Por. číslo</th>
                                    <th>Klient</th>
                                    <th>Súbor</th>
                                    <th>Stav</th>

                                </tr>
                                @foreach ($price_offers as $price_offer)
                                        <tr>
                                            <td><a href="/price_offers/{{ $price_offer->id }}">{{ $price_offer->created_at->format('d.m.Y H:i')}}</a></td>
                                            <td><a href="/price_offers/{{ $price_offer->id }}">{{ $price_offer->cislo}}</a></td>
                                            <td><a href="/price_offers/{{ $price_offer->id }}">  {{ $price_offer->klient->nazov_firmy}}</a></td>
                                            <td><a href="price_offers/getPDF/{{$price_offer->id}}">Stiahnúť</a></td>
                                            @if ($price_offer->odoslane == 1)
                                                <td>Odoslaná</td>
                                            @else
                                                <td>Čaká na odoslanie</td>
                                            @endif

                                        </tr>
                                @endforeach

                            </table>
                            {{ $price_offers->links() }}


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection