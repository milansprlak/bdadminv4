@extends('admin_template')
<head>
    <script src="/js/jquery.js"></script>
    <link href="/css/select2.min.css" rel="stylesheet" />
</head>

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Vytvoriť cenovú ponuku</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{ route('price_offers.store') }}" enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Vyberte klienta:</label>
                                    <select name="client_id" id="client_id" required class="form-control">
                                        @foreach ($clients as $client)
                                            <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Vyberte dodávateľa:</label>
                                    <select name="contractor_id" id="contractor_id" required class="form-control">
                                        @foreach ($invoice_contractors as $invoice_contractor)
                                            <option value="{{$invoice_contractor->id}}">{{$invoice_contractor->nazov_firmy}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poradové číslo cenovej ponuky</label>
                                    <input name="poradove_cislo" value="{{$nextPriceOfferNumber}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte poradové číslo faktúry">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Dátum vytvorenia:</label>
                                    <input name="datum_vystavenia" value="{{$today}}" type="date" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte dátum vytvorenia">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Dátum splatnosti:</label>
                                    <input name="datum_splatnosti" value="{{$datumsplatnosti}}" type="date" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte dátum splatnosti">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mailová adresa:</label>
                                    <input name="email" type="email" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte správu">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poznámka</label>
                                    <input name="poznamka" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte správu">
                                </div>

                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

            <script src="/js/select2.min.js"></script>
            <script type="text/javascript">

                $("#client_id").select2({
                    placeholder: "Vyberte klienta",
                    allowClear: true
                });
            </script>


@endsection
