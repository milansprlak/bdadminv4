@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>
@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <a href="/employees/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať zamestnanca</a>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam zamestnancov</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('employees.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class="box-body">
                                    <h3>Dátum:</h3>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="datum" type='text' value="{{$datum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Zobraziť</button>
                                </div>

                                <!-- /.box-body -->
                            </form>

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo</th>
                                    <th>Meno zamestnanca</th>
                                    <th>Režijná cena práce</th>
                                    <th>URL Fotka</th>
                                    <th>Odpracovaný čas</th>
                                    <th>Editácia</th>



                                </tr>


                                @foreach ($zamestnanci  as $zamestnanec)
                                    <tr>
                                        <td>{{ $zamestnanec['id'] }}</td>
                                        <td>{{ $zamestnanec['meno'] }}</td>
                                        <td>{{number_format( $zamestnanec['cena_rezia'],2) }}</td>
                                        <td>{{ $zamestnanec['fotka'] }}</td>
                                        <td>{{ number_format($zamestnanec['hod'])}} hod {{ number_format($zamestnanec['min'])}} min</td>
                                        <td><a href="/employees/{{ $zamestnanec['id'] }}/edit">Editovať</a></td>

                                    </tr>
                                @endforeach

                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>

@endsection