@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pridať zamestnanca</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{ route('employees.store') }}" role="form">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Meno</label>
                                    <input name="meno" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte meno zamestnanca">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Režijná cena práce</label>
                                    <input name="cena_prace_rezia" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte režijnú cenu práce za 1 hod">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">URL fotka</label>
                                    <input name="url_fotka" type="text" class="form-control" id="exampleInputEmail1" placeholder="Zadajte URL adresu fotky">
                                </div>


                             </div>

                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>



@endsection
