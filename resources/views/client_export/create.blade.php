@extends('admin_template')
<link href="/css/select2.min.css" rel="stylesheet" />

@section('content')


            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pridať uživateľa</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{ route('client_export.store') }}" role="form">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Uživateľské meno:</label>
                                    <input name="username" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte obchodné meno">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-MAIL:</label>
                                    <input name="email" type="email" required  class="form-control" id="exampleInputEmail1" placeholder="Zadajte email">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Heslo:</label>
                                    <input name="password" type="password" required  class="form-control" id="exampleInputEmail1" placeholder="Zadajte heslo">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kontrola hesla:</label>
                                    <input name="password_confirmation" type="password" required class="form-control" id="password-confirm" placeholder="Zadajte znovu">
                                </div>
                                <div class="form-group">
                                    <label>Vyberte klienta:</label>
                                    <select name="client_id" id="client_id" required class="form-control">
                                        @foreach ($clients as $client)
                                            <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                        @endforeach
                                    </select>
                                </div>


                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
            <script src="/js/jquery.js"></script>
            <script src="/js/select2.min.js"></script>
            <script type="text/javascript">

                $("#client_id").select2({
                    placeholder: "Vyberte klienta",
                    allowClear: true
                });
            </script>


@endsection
