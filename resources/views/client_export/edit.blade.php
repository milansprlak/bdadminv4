@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Upraviť uživateľa:</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('client_export.update',[$user->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Uživateľské meno:</label>
                                    <input name="username" value="{{$user->username}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte obchodné meno">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-MAIL:</label>
                                    <input name="email" value="{{$user->email}}" type="email" required  class="form-control" id="exampleInputEmail1" placeholder="Zadajte email">
                                </div>
                                <div class="form-group">
                                    <label>Vyberte klienta:</label>
                                    <select name="client_id" id="client_id" required class="form-control">
                                        @foreach ($clients as $client)
                                            @if($client->id == $user->client_id)
                                                <option value="{{$client->id}}" selected>{{$client->nazov_firmy}}</option>
                                            @else
                                                <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nové heslo:</label>
                                    <input name="password" type="password" class="form-control" id="exampleInputEmail1" placeholder="Zadajte heslo">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kontrola hesla:</label>
                                    <input name="password_confirmation" type="password" class="form-control" id="password-confirm" placeholder="Zadajte znovu">
                                </div>

                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>



@endsection
