@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Upraviť kategóriu</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('definedtasks.update',[$definedtask->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Názov kategórie</label>
                                    <input name="nazov" value="{{$definedtask->nazov}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte názov kategórie">
                                </div>
                                <div class="form-group">
                                    <label for="example_cena_cinnost_hod">Cena za činnosť (1 hod)</label>
                                    <input name="cena_cinnost_hod" value="{{$definedtask->cena_cinnost_hod}}" type="number" step="0.01" min="0" required class="form-control" id="example_cena_cinnost_hod" placeholder="Zadajte cenu za činnosť (1 hod)">
                                </div>
                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>



@endsection
