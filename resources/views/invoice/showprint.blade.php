@extends('admin_template')
@section('content')
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> {{$invoice->contractor->nazov_firmy}}
          <i class="pull-right">Faktúra č.: {{$invoice->poradove_cislo}}</i>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Dodávateľ
        <address>
          <strong>{{$invoice->contractor->nazov_firmy}}</strong><br>
          {{$invoice->contractor->fa_adresa}}<br>
          {{$invoice->contractor->fa_psc}} {{$invoice->contractor->fa_mesto}}<br><br>
          IČO: {{$invoice->contractor->ico}}<br>
          DIČ: {{$invoice->contractor->dic}}<br>
          IČ DPH: {{$invoice->contractor->icdph}}<br>
          Telefón: {{$invoice->contractor->telefon}}<br>
          Mobil: {{$invoice->contractor->mobil}}<br>
          E-mail: {{$invoice->contractor->email}}<br>
          WEB: {{$invoice->contractor->www_site}}
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Odberateľ:
        <address>
          <strong>{{$invoice->klientfaktury->nazov_firmy}}</strong><br>
          {{$invoice->klientfaktury->fa_adresa}}<br>
          {{$invoice->klientfaktury->fa_psc}} {{$invoice->klientfaktury->fa_mesto}}<br><br>
          @if($invoice->klientfaktury->ico != null)
            IČO: {{$invoice->klientfaktury->ico}}<br>
          @endif
          @if($invoice->klientfaktury->dic != null)
            DIČ: {{$invoice->klientfaktury->dic}}<br>
          @endif
          @if($invoice->klientfaktury->icdph != null)
            IČ DPH: {{$invoice->klientfaktury->icdph}}<br>
          @endif
          @if($invoice->klientfaktury->email != null)
            Email: {{$invoice->klientfaktury->email}}
          @endif
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Dátum vytvorenia: {{($invoice->datum_vystavenia ? date('d.m.Y', strtotime($invoice->datum_vystavenia)): '')}} <br>
        Dátum splatnosti: {{($invoice->datum_splatnosti ? date('d.m.Y', strtotime($invoice->datum_splatnosti)): '')}} <br>
        Dátum dodania: {{($invoice->datum_dodania ? date('d.m.Y', strtotime($invoice->datum_dodania)): '')}} <br>
        Forma úhrady: {{$invoice->forma_uhrady->forma_uhrady }}<br>
      </div>
      <div class="col-xs-12 text-muted well well-sm no-shadow">
        <div class="col-xs-12 text-muted lead">
          Údaje pre platbu:</div>
        <div class="col-xs-8">
          Názov banky: <strong>{{$invoice->contractor->nazov_banky}}</strong> <br>
          Číslo účtu (IBAN): <strong>{{$invoice->contractor->iban}}</strong><br>
          SWIFT: <strong>{{$invoice->contractor->swift}}</strong>
        </div>
        <div class="col-xs-4">
          Variabilný symbol: <strong>{{$invoice->variabilny_symbol}}</strong><br>
          @if($invoice->specificky_symbol != null)
            Špecifický symbol: <strong>{{$invoice->specificky_symbol}}</strong><br>
          @endif
          @if($invoice->konstantny_symbol != null)
            Konštantný symbol: <strong>{{$invoice->konstantny_symbol}}</strong> <br>
          @endif
          Cena na úhradu: <strong>{{number_format($sum_celkova,2,',',' ')}} €</strong>
        </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Názov položky</th>
            <th>Počet</th>
            <th>J. cena bez DPH</th>
            <th>DPH %</th>
            <th>Celkom s DPH</th>
          </tr>
          </thead>
          <tbody>
          @foreach ($invoice->items as $item) <!--// nacita tabulku Task priradenu k contract_id -->

          <tr>
            <td>{{$item->nazov_polozky}}</td>
            <td>{{$item->pocet_ks}}</td>
            <td>{{number_format($item->cena_bez_dph_ks,2,',',' ')}} €</td>
            <td>{{$item->vyska_dph}} %</td>
            <td>{{number_format($item->celkova_cena,2,',',' ')}} €</td>
          </tr>

          @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->

      <!-- /.col -->
      <div class="col-xs-5 pull-right">
       <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Základ DPH: </th>
              <td>{{number_format($sum_celkova_bez_dph,2,',',' ')}} €</td>
            </tr>
            <tr>
              <th>Výška DPH: </th>
              <td>{{number_format($sum_ciastka_dph,2,',',' ')}} €</td>
            </tr>
            <tr>
              <th>Celková suma:</th>
              <td>{{number_format($sum_celkova,2,',',' ')}} €</td>
            </tr>
          </table>
        </div>
      </div>

      <div class="col-xs-12 ">
<br><br><br><br><br>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>


@endsection