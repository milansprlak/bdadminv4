<!doctype html>
<html lang="sk">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>FA_{{$invoice->poradove_cislo}}</title>

  <style type="text/css">
    * {
      font-family: DejaVu Sans;  !important;
    }
    table{
      font-size: x-small;
      page-break-inside:auto;
    }

    .hlavicka{
      font-size: 14px;

    }



    .box{
      background-color: #f5f5f5;
      border: 1px solid #e31e24;

    }

    .boxpoznamka{
      background-color: #f5f5f5;
      border: 1px solid #b3b3b3;
    }

    .platobne{
      background-color: #f5f5f5;
      border: 1px solid #393185;


      border-radius: 3px;
    }
    hr{
      color: #f5f5f5;
      margin-top: -50px;
    }
    tfoot tr td{
      font-weight: bold;
      font-size: x-small;
    }
    td {
      vertical-align: top;
      page-break-inside:avoid;
      page-break-after:auto;
    }

    tr{
      page-break-inside:avoid;
      page-break-after:auto;

    }
    .zvyraznenie{
      background-color: #f5f5f5;
font-size:9px;
      line-height: 14px;
      font-weight: bold;
    }

    .popis{
      line-height: 14px;
    }

    footer {
      position: fixed;
      bottom: -50px;
      left: 0px;
      right: 0px;
      height: 50px;
      font-size: x-small;
      text-align: center;
      line-height: 35px;
    }





  </style>

</head>
<body>


<table width="100%" >
  <tr>
    <td vertical-align="center"><img src="/img/logo_bedrich.png" alt="{{$invoice->contractor->nazov_firmy}}" height="40" width="100"></td>
    <td align="right" vertical-align="center" class="hlavicka">
      DODACÍ LIST K FAKTÚRE č. {{$invoice->poradove_cislo}}

    </td>
  </tr>

</table>


    <br>
<table width="100%">
  <tr>
    <td>

     Dodávateľ:<br>
      <address>
        <strong>{{$invoice->contractor->nazov_firmy}}<br>
        {{$invoice->contractor->fa_adresa}}<br>
        {{$invoice->contractor->fa_psc}} {{$invoice->contractor->fa_mesto}}</strong>
        <span style="font-size: 7px;"><br><br>
        IČO: {{$invoice->contractor->ico}}<br>
        DIČ: {{$invoice->contractor->dic}}<br>
        IČ DPH: {{$invoice->contractor->icdph}}<br>
        @if($invoice->contractor->telefon != null)
            Telefón: {{$invoice->contractor->telefon}}<br>
          @endif
        Mobil: {{$invoice->contractor->mobil}}<br>
        E-mail: {{$invoice->contractor->email}}<br>
        WEB: {{$invoice->contractor->www_site}}
        </span>
        </address>
    </td>


    <td>
      @if($invoice->cislo_objednavky != null)
        <strong>Číslo objednávky: {{$invoice->cislo_objednavky }}</strong>
      @endif
      <span style="font-size: 7px;"><br><br>
      Dodací list č.:  {{$invoice->poradove_cislo }}<br>
      Dátum vystavenia: {{($invoice->datum_vystavenia ? date('d.m.Y', strtotime($invoice->datum_vystavenia)): '')}} <br>
      </span>

        <div class="box">
        Odberateľ:<br>
        <address>

          <strong>{{$invoice->klientfaktury->nazov_firmy}}<br>
            {{$invoice->klientfaktury->fa_adresa}}<br>
            {{$invoice->klientfaktury->fa_psc}} {{$invoice->klientfaktury->fa_mesto}}</strong>

          <span style="font-size: 7px;"><br><br>
        @if($invoice->klientfaktury->ico != null)
              IČO: {{$invoice->klientfaktury->ico}}<br>
            @endif
            @if($invoice->klientfaktury->dic != null)
              DIČ: {{$invoice->klientfaktury->dic}}<br>
            @endif
            @if($invoice->klientfaktury->icdph != null)
              IČ DPH: {{$invoice->klientfaktury->icdph}}<br>
            @endif
            @if($invoice->klientfaktury->email != null)
              Email: {{$invoice->klientfaktury->email}}
            @endif
          </span>

        </address>
          <br>
        </div>
    </td>


  </tr>

</table>
<br/>
<table width="100%">
  <thead class="zvyraznenie">
  <tr class="zvyraznenie">
    <th>Názov položky</th>
    <th align="right">Počet</th>
  </tr>
  </thead>
  <tbody>
  @foreach ($invoice->items as $item) <!--// nacita tabulku Task priradenu k contract_id -->
  <tr>
    <th scope="row">{{$item->nazov_polozky}}</th>
    <td align="right">{{$item->pocet_ks}} {{$item->jednotka}}</td>
  </tr>
  @endforeach


  </tbody>
  <br><br>

</table>
<br/><br/>

<br/><br/><br/>
<table width="100%" >

  <tr>
    <td align="right" ><img src="/img/razitko_bedrich.png" height="60" ></td>

  </tr>




</table>

</body>
</html>