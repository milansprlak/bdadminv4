@extends('admin_template')
<head>
    <link href="/css/select2.min.css" rel="stylesheet" />
    <script src="/js/jquery.js"></script>



</head>

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <h1>
                Faktúra: {{$invoice->poradove_cislo}} <a href=""  class="btn btn-success">{{$invoice->invoicesstatus->nazov_stavu}}</a>
            </h1>

        </section>


        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-10">
                    <a href="/invoice/{{$invoice->id}}/edit"  class="btn btn-primary">Editovať faktúru</a>
                </div>

                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> {{$invoice->contractor->nazov_firmy}}
                        <i class="pull-right">Faktúra č.: {{$invoice->poradove_cislo}}</i>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    Dodávateľ
                    <address>
                        <strong>{{$invoice->contractor->nazov_firmy}}</strong><br>
                        {{$invoice->contractor->fa_adresa}}<br>
                        {{$invoice->contractor->fa_psc}} {{$invoice->contractor->fa_mesto}}<br><br>
                        IČO: {{$invoice->contractor->ico}}<br>
                        DIČ: {{$invoice->contractor->dic}}<br>
                        IČ DPH: {{$invoice->contractor->icdph}}<br>
                        @if($invoice->contractor->telefon != null)
                            Telefón: {{$invoice->contractor->telefon}}<br>
                        @endif
                        Mobil: {{$invoice->contractor->mobil}}<br>
                        E-mail: {{$invoice->contractor->email}}<br>
                        WEB: {{$invoice->contractor->www_site}}
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    Odberateľ:
                    <address>
                        <strong>{{$invoice->klientfaktury->nazov_firmy}}</strong><br>
                        {{$invoice->klientfaktury->fa_adresa}}<br>
                        {{$invoice->klientfaktury->fa_psc}} {{$invoice->klientfaktury->fa_mesto}}<br><br>
                        @if($invoice->klientfaktury->ico != null)
                        IČO: {{$invoice->klientfaktury->ico}}<br>
                        @endif
                        @if($invoice->klientfaktury->dic != null)
                        DIČ: {{$invoice->klientfaktury->dic}}<br>
                        @endif
                        @if($invoice->klientfaktury->icdph != null)
                        IČ DPH: {{$invoice->klientfaktury->icdph}}<br>
                        @endif
                        @if($invoice->klientfaktury->email != null)
                        Email: {{$invoice->klientfaktury->email}}
                        @endif
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    @if($invoice->cislo_objednavky != null)
                    Číslo objednávky: {{$invoice->cislo_objednavky }}<br>
                    @endif
                    Dátum vytvorenia: {{($invoice->datum_vystavenia ? date('d.m.Y', strtotime($invoice->datum_vystavenia)): '')}} <br>
                    Dátum splatnosti: {{($invoice->datum_splatnosti ? date('d.m.Y', strtotime($invoice->datum_splatnosti)): '')}} <br>
                    Dátum dodania: {{($invoice->datum_dodania ? date('d.m.Y', strtotime($invoice->datum_dodania)): '')}} <br>
                    Forma úhrady: {{$invoice->forma_uhrady->forma_uhrady }}<br>

                </div>
                <!-- /.col -->

                <div class="col-xs-12 text-muted well well-sm no-shadow">
                    <p class="lead">Platobné údaje:</p>
                    <div class="col-xs-8">
                        Názov banky: <strong>{{$invoice->contractor->nazov_banky}}</strong> <br>
                        Číslo účtu (IBAN): <strong>{{$invoice->contractor->iban}}</strong><br>
                        SWIFT: <strong>{{$invoice->contractor->swift}}</strong>
                    </div>
                    <div class="col-xs-4">
                        Variabilný symbol: <strong>{{$invoice->variabilny_symbol}}</strong><br>
                        @if($invoice->specificky_symbol != null)
                            Špecifický symbol: <strong>{{$invoice->specificky_symbol}}</strong><br>
                        @endif
                        @if($invoice->konstantny_symbol != null)
                        Konštantný symbol: <strong>{{$invoice->konstantny_symbol}}</strong><br>
                        @endif
                        Cena na úhradu: <strong>{{number_format($sum_celkova,2,',',' ')}} €</strong>
                    </div>

                </div>



            </div>

        @if($invoice->invoice_status_id == 1)
            <!-- add products /.row -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addproductModal" data-whatever="@mdo">Pridať položku</button>
            <div class="modal fade" id="addproductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="overflow:hidden;">
                <div class="modal-dialog" role="document">

                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="exampleModalLabel">Pridať položku do faktúry</h3>

                        </div>
                        <div class="modal-body">
                            <form id="addItem" method="post" action="{{ route('invoice_detail.store') }}" >
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Názov položky zo skladu:</label>
                                    <select name="product_id" id="product_id" onchange="defHodnota()" class="form-control" style="width: 100%">

                                        @foreach($products as $product)
                                            <option value="{{$product->id}}">{{$product->nazov_produktu}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" id="nazovpolozkylabel" class="col-form-label">Názov položky:</label>
                                    <input name="nazovpolozky" id="nazovpolozky" style="display: none" type="text" class="form-control" id="recipient-name">
                                    <input name="invoice_id" id="invoice_id" style="display: none" value="{{$invoice->id}}" type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Počet ks:</label>
                                    <input type="number" name="pocet_ks" class="form-control" step="0.01" value="1" min="1" required id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Jednotka:</label>
                                    <input type="text" name="jednotka" id="jednotka" class="form-control" value="ks" required id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Cena bez DPH za ks:</label>
                                    <input type="number" step="0.01"  min="0"  name="cenabezdph" id="cenabezdph" class="form-control required" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Výška DPH</label>
                                    <input type="number" name="vyskadph" id="vyskadph" value="20" min="0" max="100" class="form-control" id="recipient-name">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavrieť</button>
                                    <button type="submit" class="btn btn-primary">Vložiť položku</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal EDIT-->
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Editovanie položky</h4>
                        </div>
                        <form id="editItem" action="{{route('invoice_detail.update',"item_id")}}" method="post">
                            {{method_field('patch')}}
                            {{csrf_field()}}
                            <div class="modal-body">
                                <input type="hidden" name="item_id" id="item_id" value="">

                                <div class="form-group">
                                    <label>Názov položky zo skladu:</label>
                                    <select name="editproduct_id" id="editproduct_id" onchange="editdefHodnota()" class="form-control" style="width: 100%">

                                        @foreach($products as $product)
                                            <option value="{{$product->id}}">{{$product->nazov_produktu}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" id="editnazovpolozkylabel" style="display: none" class="col-form-label">Názov položky:</label>
                                    <input name="editnazovpolozky" id="editnazovpolozky" style="display: none" type="text" class="form-control" id="recipient-name">
                                    <input name="editinvoice_id" id="editinvoice_id" style="display: none" value="{{$invoice->id}}" type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Počet ks:</label>
                                    <input type="number" name="editpocet_ks" id="editpocet_ks" step="0.01" class="form-control" value="1" min="1" required id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Jednotka:</label>
                                    <input type="text" name="editjednotka" id="editjednotka" class="form-control" value="ks" required id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Cena bez DPH za ks:</label>
                                    <input type="number" step="0.01" min="0" name="editcenabezdph" id="editcenabezdph" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Výška DPH</label>
                                    <input type="number" name="editvyskadph" id="editvyskadph" value="20" min="0" max="100" class="form-control" id="recipient-name">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Zavrieť</button>
                                <button type="submit" class="btn btn-primary">Uložiť zmeny</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif
            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Názov položky</th>
                            <th>Počet</th>
                            <th>J. cena bez DPH</th>
                            <th>DPH %</th>
                            <th>Celkom s DPH</th>
                            @if($invoice->invoice_status_id == 1)
                            <th>Akcia</th>
                            @endif

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        @foreach ($invoice->items as $item) <!--// nacita tabulku Task priradenu k contract_id -->

                        <tr>

                            <td>{{$item->nazov_polozky}}</td>
                            <td>{{$item->pocet_ks}} {{$item->jednotka}}</td>
                            <td>{{number_format($item->cena_bez_dph_ks,2,',',' ')}} €</td>
                            <td>{{$item->vyska_dph}} %</td>
                            <td>{{number_format($item->celkova_cena,2,',',' ')}} €</td>
                            @if($invoice->invoice_status_id == 1)

                            <td><button class="btn btn-info" data-nazov="{{$item->nazov_polozky}}" data-nazov_id="{{$item->stock_id}}" data-pocet_ks="{{$item->pocet_ks}}" data-jednotka="{{$item->jednotka}}" data-cena="{{$item->cena_bez_dph_ks}}" data-vyskadph="{{$item->vyska_dph}}" data-item_id={{$item->id}} data-toggle="modal" data-target="#edit">Editovať</button>
                            /
                            <button href="#"  class="btn btn-danger" onclick="
                                        var result = confirm('Skutočne odstrániť polžku z faktúry?');
                                        if( result ){
                                        event.preventDefault();

                                        document.getElementById('delete-form-{{$item->id}}').submit();
                                        }
                                        ">Odstrániť</button>
                                <form id="delete-form-{{$item->id}}" action="{{ route('invoice_detail.destroy',[$item->id])}}"
                                      method="POST" style="display: none;">
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                                    {{ csrf_field() }}
                                </form>

                            </td>
                            @endif

                        </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            @if($invoice->poznamka != null)
            <div class="col-xs-6 text-muted well well-sm no-shadow">
                <p class="lead">Poznámka:</p>
                <div class="col-xs-6">
                    {{$invoice->poznamka}}
                </div>


            </div>
            @endif

            <div class="row">
                <!-- accepted payments column -->

                <!-- /.col -->
                <div  class="col-xs-6 pull-right">


                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">Základ DPH: </th>
                                <td>{{number_format($sum_celkova_bez_dph,2,',',' ')}} €</td>
                            </tr>
                            <tr>
                                <th>Výška DPH: </th>
                                <td>{{number_format($sum_ciastka_dph,2,',',' ')}} €</td>
                            </tr>
                            <tr>
                                <th>Celková suma:</th>
                                <td>{{number_format($sum_celkova,2,',',' ')}} €</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                <!--  <a href="/invoice/print/{{$invoice->id}}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
                    @if (($invoice->invoicesstatus->nazov_stavu) == "Odoslaná" )
                        <button type="button"  class="btn btn-success pull-right" onclick="document.getElementById('uhradena-form-{{$invoice->id }}').submit();"><i class="fa fa-credit-card"></i> Pridať úhradu</button>
                    @elseif (($invoice->invoicesstatus->nazov_stavu) == "Uhradená" )
                    @else
                        <button type="button"  class="btn btn-success pull-right" onclick="document.getElementById('uhradena-form-{{$invoice->id }}').submit();"><i class="fa fa-credit-card"></i> Pridať úhradu</button>
                        <button type="button"  class="btn btn-primary pull-right" onclick="document.getElementById('odoslana-form-{{$invoice->id}}').submit();"><i class="fa fa-envelope-o"></i> Odoslať </button>
                    @endif

                    <form id="odoslana-form-{{$invoice->id}}" action="{{ route('invoice.update',[$invoice->id])}}"
                          method="POST" style="display: none;">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="changestatus" value="1">
                        <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                        <input type="hidden" name="status" value="2">

                    </form>
                    <form id="uhradena-form-{{$invoice->id }}" action="{{ route('invoice.update',[$invoice->id])}}"
                          method="POST" style="display: none;">.
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="changestatus" value="1">
                        <input type="hidden" name="invoice_id" value="{{$invoice->id }}">
                        <input type="hidden" name="status" value="3">

                    </form>
                    <a href="/invoice/dodaci-list/{{$invoice->id}}" class=" btn btn-primary pull-right" role="button" style="margin-right: 5px;"><i class="fa fa-download"></i> Dodací list</a>
                    <a href="/invoice/getPDF/{{$invoice->id}}" class=" btn btn-primary pull-right" role="button" style="margin-right: 5px;"><i class="fa fa-file-pdf-o"></i> Generuj PDF</a>
                </div>
            </div>
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>



        <script src="/js/select2.min.js"></script>


        <script type="text/javascript">
            $("#product_id").select2({
                allowClear: true,
                multiple: false,
                dropdownParent: $("#product_id").parent(),
                escapeMarkup: function (m) {
                    return m;
                },
            });
        </script>
        <script>
            window.onload = defHodnota();
            function defHodnota() {
                var hodnota = document.getElementById("product_id").value;


                el = document.getElementById('product_id')
                selectedText = el.options[el.selectedIndex].text

                if (hodnota == 1) {
                    document.getElementById('nazovpolozky').style.display = 'block';
                    document.getElementById('nazovpolozkylabel').style.display = 'block';
                    document.getElementById("nazovpolozky").value = null;
                    document.getElementById("cenabezdph").value = null;
                    document.getElementById("vyskadph").value = 20;
                }
                else {
                    document.getElementById('nazovpolozky').style.display = 'none';
                    document.getElementById('nazovpolozkylabel').style.display = 'none';
                    document.getElementById("nazovpolozky").value = selectedText;



                    @foreach ($products as $product)
                    if (hodnota == "{{$product->id}}" ) {
                        document.getElementById("cenabezdph").value = '{{ $product->cena__bez_dph}}';
                        document.getElementById("vyskadph").value = '{{ $product->dph}}';
                    }
                    @endforeach
                }
            }
        </script>
        <script>
            window.onload = editdefHodnota();
            function editdefHodnota() {
                var hodnota = document.getElementById("editproduct_id").value;


                el = document.getElementById('editproduct_id')
                selectedText = el.options[el.selectedIndex].text

                if (hodnota == 1) {
                    document.getElementById('editnazovpolozky').style.display = 'block';
                    document.getElementById('editnazovpolozkylabel').style.display = 'block';
                    document.getElementById("editnazovpolozky").value = null;
                    document.getElementById("editcenabezdph").value = null;
                    document.getElementById("editvyskadph").value = 20;
                }
                else {
                    document.getElementById('editnazovpolozky').style.display = 'none';
                    document.getElementById('editnazovpolozkylabel').style.display = 'none';
                    document.getElementById("editnazovpolozky").value = selectedText;



                    @foreach ($products as $product)
                    if (hodnota == "{{$product->id}}" ) {
                        document.getElementById("editcenabezdph").value = '{{ $product->cena__bez_dph}}';
                        document.getElementById("editvyskadph").value = '{{ $product->dph}}';
                    }
                    @endforeach
                }
            }
        </script>

        <script type="text/javascript">
            $('#addItem').on('submit', function(e) {
                var firstName = $('#cenabezdph');

                // Check if there is an entered value
                if(!firstName.val()) {
                    // Add errors highlight
                    firstName.closest('.form-group').removeClass('has-success').addClass('has-error');

                    // Stop submission of the form
                    e.preventDefault();
                } else {
                    // Remove the errors highlight
                    firstName.closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            });
        </script>

        <script type="text/javascript">
            $('#editItem').on('submit', function(e) {
                var firstName = $('#editcenabezdph');

                // Check if there is an entered value
                if(!firstName.val()) {
                    // Add errors highlight
                    firstName.closest('.form-group').removeClass('has-success').addClass('has-error');

                    // Stop submission of the form
                    e.preventDefault();
                } else {
                    // Remove the errors highlight
                    firstName.closest('.form-group').removeClass('has-error').addClass('has-success');
                }
            });
        </script>

<!-- ./wrapper -->


@endsection