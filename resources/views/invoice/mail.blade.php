Dobrý deň <strong>{{ $name }}</strong>,

<p>V prílohe Vám posielame faktúru č. {{ $detail }}.</p>

<p>Ďakujeme a prajeme pekný deň.</p>

<br>
    S pozdravom: <br>
    BEDRICH spol. s r.o.<br>
    Štefanov nad Oravou 93<br>
    027 44 Tvrdošín<br>
    tel. 043/5323076<br>
    IČO: 36 406 783<br>
    IČ DPH: SK2021681816<br>
    Email: <a href="mailto:info@bedrich.sk">info@bedrich.sk</a><br>
    WEB: <a href="https://bedrich.sk">https://bedrich.sk</a>
</p>