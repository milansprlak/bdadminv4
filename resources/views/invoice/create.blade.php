@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="/css/select2.min.css" rel="stylesheet" />
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pridať faktúru</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{ route('invoice.store') }}" role="form">
                            {{ csrf_field() }}
                            <div class="box-body">

                                <div class="form-group">
                                    <label>Vyberte klienta:</label>
                                    <select name="client_id" id="client_id" required class="form-control">
                                        @foreach ($clients as $client)
                                            <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Vyberte dodávateľa:</label>
                                    <select name="contractor_id" id="contractor_id" required class="form-control">
                                        @foreach ($invoice_contractors as $invoice_contractor)
                                            <option value="{{$invoice_contractor->id}}">{{$invoice_contractor->nazov_firmy}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poradové číslo faktúry</label>
                                    <input name="poradove_cislo" value="{{$invoice_next_number}}" type="number" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte poradové číslo faktúry">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Číslo objednávky</label>
                                    <input name="cislo_objednavky"  type="text"  class="form-control" id="exampleInputEmail1" placeholder="Zadajte číslo objednávky">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Poznámka</label>
                                    <input name="poznamka"  type="text"  class="form-control" id="exampleInputEmail1" placeholder="Zadajte poznámku.">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Dátum vytvorenia:</label>
                                    <input name="datum_vystavenia" value="{{$today}}" type="date" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte dátum vytvorenia">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Dátum dodania:</label>
                                    <input name="datum_dodania" value="{{$datum_dodania}}" type="date" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte dátum dodania">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Dátum splatnosti:</label>
                                    <input name="datum_splatnosti" value="{{$datumsplatnosti}}" type="date" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte dátum splatnosti">
                                </div>

                                <div class="form-group">
                                    <label>Forma úhrady:</label>
                                    <select name="forma_uhrady" required class="form-control">
                                        @foreach($invoice_payments as $invoice_payment)
                                            <option value="{{$invoice_payment->id}}">{{$invoice_payment->forma_uhrady}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Zadajte variabilný symbol</label>
                                    <input name="variabilny_symbol" value="{{$invoice_next_number}}" type="number" class="form-control" id="exampleInputEmail1" placeholder="Zadajte variabilný symbol">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Zadajte konštantný symbol</label>
                                    <input name="konstantny_symbol" value="0308" type="number" class="form-control" id="exampleInputEmail1" placeholder="Zadajte konštantný symbol" max="9999">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Zadajte špecifický symbol</label>
                                    <input name="specificky_symbol"  type="number"  class="form-control" id="exampleInputEmail1" placeholder="Zadajte špecifický symbol">
                                </div>

                                <div class="form-group">
                                    <label>Stav faktúry:</label>
                                    <select name="stav_faktury" class="form-control">
                                        @foreach($invoice_statuses as $invoice_status)
                                            <option value="{{$invoice_status->id}}">{{$invoice_status->nazov_stavu}}</option>
                                        @endforeach
                                    </select>
                                </div>


                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
            <script src="/js/select2.min.js"></script>
            <script type="text/javascript">

                $("#client_id").select2({
                    placeholder: "Vyberte klienta",
                    allowClear: true
                });
            </script>


@endsection
