@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam faktúr</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('invoice.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class="box-body">
                                    <div class='form-group col-md-2'>
                                    <label>Časové obdobie od:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="fromdatum" type='text' value="{{$fromdatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    </div>
                                    <div class='form-group col-md-2'>
                                        <label>Časové obdobie do:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="todatum" type='text' value="{{$todatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Stav zákazky:</label>
                                        <select id="stavzakazky" name="stav_faktury"  class="form-control">
                                            <option value="0">Všetky</option>
                                            @foreach($invoice_statuses as $invoice_status)
                                                @if($invoice_status->id == $value_stav_faktury)
                                                    <option value="{{$invoice_status->id}}" selected>{{$invoice_status->nazov_stavu}}</option>
                                                @else
                                                    <option value="{{$invoice_status->id}}">{{$invoice_status->nazov_stavu}}</option>
                                                @endif
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Klient:</label>
                                        <select id="klient" name="klient"  class="form-control">
                                            <option value="0">Všetky</option>
                                            @foreach($clients as $client)

                                                @if($client->id == $value_klient)
                                                    <option value="{{$client->id}}" selected>{{$client->nazov_firmy}}</option>
                                                @else
                                                    <option value="{{$client->id}}">{{$client->nazov_firmy}}</option>
                                                @endif
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <br>
                                        <button type="submit" class="btn btn-primary col-md-3">Zobraziť</button>
                                    </div>
                                </div>

                                <!-- /.box-body -->
                            </form>


                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Číslo faktúry</th>
                                    <th>Meno klienta</th>
                                    <th>Číslo objednávky</th>
                                    <th>Dátum vytvorenia</th>
                                    <th>Čiastka</th>
                                    <th>Dátum splatnosti</th>
                                    <th>Stav faktúry</th>
                                    <th>Akcia</th>
                                </tr>

                                @foreach ($invoices as $invoice)
                                    @if ($invoice->invoicesstatus->id != 4)

                                        <tr>
                                            <td><a href="/invoice/{{ $invoice->id }}">  {{ $invoice->poradove_cislo}}</a></td>
                                            <td>{{$invoice->klientfaktury->nazov_firmy}}</td>
                                            <td>{{$invoice->cislo_objednavky}}</td>
                                            <td>{{($invoice->datum_vystavenia ? date('d.m.Y', strtotime($invoice->datum_vystavenia)): '')}}</td>
                                            <td>{{number_format($invoice->cena_s_dph, 2, ',', '')}} €</td>
                                            <td>{{($invoice->datum_splatnosti ? date('d.m.Y', strtotime($invoice->datum_splatnosti)): '')}}</td>
                                            <td>{{$invoice->invoicesstatus->nazov_stavu}}</td>
                                            <td> @if (($invoice->invoicesstatus->nazov_stavu) == "Odoslaná" )
                                                 <button href="#"  class="btn btn-success" onclick="document.getElementById('uhradena-form-{{$invoice->id }}').submit();">Uhradiť</button>
                                                 @elseif (($invoice->invoicesstatus->nazov_stavu) == "Uhradená" )
                                                @else
                                                <button href="#"  class="btn btn-primary" onclick="document.getElementById('odoslana-form-{{$invoice->id}}').submit();">Odoslať</button>
                                                <button href="#"  class="btn btn-success" onclick="document.getElementById('uhradena-form-{{$invoice->id }}').submit();">Uhradiť</button>
                                                @endif
                                                <form id="odoslana-form-{{$invoice->id}}" action="{{ route('invoice.update',[$invoice->id])}}"
                                                      method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="put">
                                                    <input type="hidden" name="changestatus" value="1">
                                                    <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                                                    <input type="hidden" name="status" value="2">

                                                </form>
                                                <form id="uhradena-form-{{$invoice->id }}" action="{{ route('invoice.update',[$invoice->id])}}"
                                                      method="POST" style="display: none;">.
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="put">
                                                    <input type="hidden" name="changestatus" value="1">
                                                    <input type="hidden" name="invoice_id" value="{{$invoice->id }}">
                                                    <input type="hidden" name="status" value="3">

                                                </form>
                                            </td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td><a style="color: red" href="/invoice/{{ $invoice->id }}">  {{ $invoice->poradove_cislo}}</a></td>
                                            <td><span style="color: red">{{$invoice->klientfaktury->nazov_firmy}}</span></td>
                                            <td><span style="color: red">{{$invoice->cislo_objednavky}}</span></td>
                                            <td><span style="color: red">{{($invoice->datum_vystavenia ? date('d.m.Y', strtotime($invoice->datum_vystavenia)): '')}}</span></td>
                                            <td><span style="color: red">{{number_format($invoice->cena_s_dph, 2, ',', '')}} €</span></td>
                                            <td><span style="color: red">{{($invoice->datum_splatnosti ? date('d.m.Y', strtotime($invoice->datum_splatnosti)): '')}}</span></td>
                                            <td><span style="color: red">{{$invoice->invoicesstatus->nazov_stavu}}</span></td>
                                            <td> @if (($invoice->invoicesstatus->nazov_stavu) == "Odoslaná" )
                                                    <button href="#"  class="btn btn-success" onclick="document.getElementById('uhradena-form-{{$invoice->id }}').submit();">Uhradiť</button>
                                                @elseif (($invoice->invoicesstatus->nazov_stavu) == "Uhradená" )
                                                @else
                                                    <button href="#"  class="btn btn-primary" onclick="document.getElementById('odoslana-form-{{$invoice->id}}').submit();">Odoslať</button>
                                                    <button href="#"  class="btn btn-success" onclick="document.getElementById('uhradena-form-{{$invoice->id }}').submit();">Uhradiť</button>
                                                @endif
                                                <form id="odoslana-form-{{$invoice->id}}" action="{{ route('invoice.update',[$invoice->id])}}"
                                                      method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="put">
                                                    <input type="hidden" name="changestatus" value="1">
                                                    <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                                                    <input type="hidden" name="status" value="2">

                                                </form>
                                                <form id="uhradena-form-{{$invoice->id }}" action="{{ route('invoice.update',[$invoice->id])}}"
                                                      method="POST" style="display: none;">.
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="put">
                                                    <input type="hidden" name="changestatus" value="1">
                                                    <input type="hidden" name="invoice_id" value="{{$invoice->id }}">
                                                    <input type="hidden" name="status" value="3">

                                                </form>
                                            </td>
                                        </tr>
                                        @endif

                                @endforeach

                            </table>
                            {{ $invoices->appends(['fromdatum' => $fromdatum,'todatum'=> $todatum,'stav_faktury' => $value_stav_faktury,'klient' => $value_klient])->links() }}


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>
@endsection