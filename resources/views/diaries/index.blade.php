@extends('admin_template')
<head>
    <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

</head>

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <a href="/diary/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať novú poznámku</a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Denník</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="box-body">
                            <form  method="post" action="{{ route('diary.index') }}" role="form">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="get">
                                <div class='form-group col-md-3'>
                                    <label>Časové obdobie od:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="fromdatum" type='text' value="{{$fromdatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                                <div class='form-group col-md-3'>
                                    <label>Časové obdobie do:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="todatum" type='text' value="{{$todatum}}" class="form-control" />
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                                <div class='col-md-3'>
                                    <br>
                                    <button type="submit" class="btn btn-primary">Zobraziť</button>
                                </div>

                                <!-- /.box-body -->
                            </form>


                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Dátum</th>
                                    <th>Poznámka</th>
                                    <th>Editácia</th>
                                </tr>
                                @foreach ($diaries as $diaries2)
                                        <tr>
                                            <td>{{ $diaries2->created_at->format('d.m.Y H:i')}}</td>
                                            <td>{{ $diaries2->poznamka}}</td>
                                            @if ($diaries2->created_at->isToday())
                                                <td><a href="/diary/{{ $diaries2->id }}/edit">Editovať</a></td>
                                            @endif

                                        </tr>
                                @endforeach

                            </table>
                            {{ $diaries->appends(['fromdatum' => $fromdatum,'todatum'=> $todatum])->links() }}


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->
    <script type="text/javascript">
        $('.date').datepicker({
            //format: 'dd-mm-yyyy'
            format: 'yyyy-mm-dd'
        });

    </script>
@endsection