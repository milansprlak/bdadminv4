@extends('admin_template')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <a href="/clients/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Pridať nového klienta</a>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Zoznam klientov</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">

                    <div class="box">

                        <!-- /.box-header -->
                        <div class="table-responsive ">

                            <table id="example1" class="table table-bordered table-striped">
                                <tr>
                                    <th>Poradové číslo:</th>
                                    <th>Obchodné meno:</th>
                                    <th>Kontaktná osoba:</th>
                                    <th>IČO:</th>
                                    <th>DIČ:</th>
                                    <th>IČDPH:</th>
                                    <th>Adresa:</th>
                                    <th>Mesto:</th>
                                    <th>PSČ:</th>
                                    <th>Krajina</th>
                                    <th>Telefón:</th>
                                    <th>Mobil:</th>
                                    <th>E-MAIL:</th>
                                    <th>Editácia</th>



                                </tr>
                                <div style="display: none;">
                                    {{ $i=1 }}
                                </div>

                                @foreach ($clients as  $client)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $client->nazov_firmy}}</td>
                                        <td>{{ $client->kontaktna_osoba}}</td>
                                        <td>{{ $client->ico}}</td>
                                        <td>{{ $client->dic}}</td>
                                        <td>{{ $client->icdph}}</td>
                                        <td>{{ $client->fa_adresa}}</td>
                                        <td>{{ $client->fa_mesto}}</td>
                                        <td>{{ $client->fa_psc}}</td>
                                        <td>{{ $client->fa_krajina}}</td>
                                        <td>{{ $client->telefon}}</td>
                                        <td>{{ $client->mobil}}</td>
                                        <td>{{ $client->email}}</td>
                                        <td><a href="/clients/{{ $client->id }}/edit">Editovať</a></td>
                                    </tr>


                                @endforeach

                            </table>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->

@endsection