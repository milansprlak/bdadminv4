@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pridať uživateľa</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{ route('users.store') }}" role="form">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Uživateľské meno:</label>
                                    <input name="name" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte obchodné meno">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-MAIL:</label>
                                    <input name="email" type="email" required  class="form-control" id="exampleInputEmail1" placeholder="Zadajte email">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Heslo:</label>
                                    <input name="password" type="password" required  class="form-control" id="exampleInputEmail1" placeholder="Zadajte heslo">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kontrola hesla:</label>
                                    <input name="password_confirmation" type="password" required class="form-control" id="password-confirm" placeholder="Zadajte znovu">
                                </div>
                                <h3>Oprávnenia uživateľa:</h3>
                                <div class="form-group">
                                    <label>Administrácia zákaziek:</label>
                                    <select name="mod_1" class="form-control">
                                           <option value="0">Nepovolený prístup</option>
                                           <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zobrazenie zákaziek na odovzdanie:</label>
                                    <select name="mod_2" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zobrazenie štatistiky firmy:</label>
                                    <select name="mod_3" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zobrazenie štatistiky klientov:</label>
                                    <select name="mod_4" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zobrazenie štatistiky úloh:</label>
                                    <select name="mod_5" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Prístup k pokladni:</label>
                                    <select name="mod_6" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Prístup k pripomienkam:</label>
                                    <select name="mod_7" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Prístup k denníku:</label>
                                    <select name="mod_8" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Prístup k správam:</label>
                                    <select name="mod_9" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Administrácia faktúr:</label>
                                    <select name="mod_10" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Administrácia užívateľov:</label>
                                    <select name="mod_11" class="form-control">
                                        <option value="0">Nepovolený prístup</option>
                                        <option value="1">Povolený prístup</option>
                                    </select>
                                </div>


                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>


@endsection
