@extends('admin_template')

@section('content')
            <div class="row">

                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-12">

                    <!-- general form elements disabled -->
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Upraviť uživateľa:</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form  method="post" action="{{route('users.update',[$user->id])}}" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Uživateľské meno:</label>
                                    <input name="name" value="{{$user->name}}" type="text" required class="form-control" id="exampleInputEmail1" placeholder="Zadajte obchodné meno">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-MAIL:</label>
                                    <input name="email" value="{{$user->email}}" type="email" required  class="form-control" id="exampleInputEmail1" placeholder="Zadajte email">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nové heslo:</label>
                                    <input name="password" type="password" class="form-control" id="exampleInputEmail1" placeholder="Zadajte heslo">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kontrola hesla:</label>
                                    <input name="password_confirmation" type="password" class="form-control" id="password-confirm" placeholder="Zadajte znovu">
                                </div>
                                <h3>Oprávnenia uživateľa:</h3>
                                <div class="form-group">
                                    <label>Administrácia zákaziek:</label>
                                    <select name="mod_1" class="form-control">
                                            @if($user->mod_1 == 0)
                                                <option value="0" selected>Nepovolený prístup</option>
                                                <option value="1" >Povolený prístup</option>
                                            @else
                                                <option value="0" >Nepovolený prístup</option>
                                                <option value="1" selected>Povolený prístup</option>
                                             @endif
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label>Zobrazenie zákaziek na odovzdanie:</label>
                                    <select name="mod_2" class="form-control">
                                        @if($user->mod_2 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zobrazenie štatistiky firmy:</label>
                                    <select name="mod_3" class="form-control">
                                        @if($user->mod_3 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zobrazenie štatistiky klientov:</label>
                                    <select name="mod_4" class="form-control">
                                        @if($user->mod_4 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Zobrazenie štatistiky úloh:</label>
                                    <select name="mod_5" class="form-control">
                                        @if($user->mod_5 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Prístup k pokladni:</label>
                                    <select name="mod_6" class="form-control">
                                        @if($user->mod_6 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Prístup k pripomienkam:</label>
                                    <select name="mod_7" class="form-control">
                                        @if($user->mod_7 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Prístup k denníku:</label>
                                    <select name="mod_8" class="form-control">
                                        @if($user->mod_8 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Prístup k správam:</label>
                                    <select name="mod_9" class="form-control">
                                        @if($user->mod_9 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Administrácia faktúr:</label>
                                    <select name="mod_10" class="form-control">
                                        @if($user->mod_10 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Administrácia užívateľov:</label>
                                    <select name="mod_11" class="form-control">
                                        @if($user->mod_11 == 0)
                                            <option value="0" selected>Nepovolený prístup</option>
                                            <option value="1" >Povolený prístup</option>
                                        @else
                                            <option value="0" >Nepovolený prístup</option>
                                            <option value="1" selected>Povolený prístup</option>
                                        @endif
                                    </select>
                                </div>
                             </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Uložiť</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
            </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>



@endsection
