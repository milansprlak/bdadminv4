<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $fillable = [
        'id',
        'meno',
        'url_fotka',
        'cena_prace_rezia',

    ];
    public function contract(){
        return $this->belongsTo('App\Contract');

    }

    public function tasks(){
        return $this->hasMany('App\Task','employy_id');

    }

}
