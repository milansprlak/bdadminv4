<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientExport extends Model
{
    protected $fillable = [
        'id',
        'username',
        'email',
        'password',
        'client_id',

    ];
    public function klient()
    {
        return $this->belongsTo('App\Client','client_id');
    }
}
