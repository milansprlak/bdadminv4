<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class DefinedTask extends Model
{
    protected $fillable = [
        'id',
        'nazov',
        'cena_cinnost_hod',
        ];
    public function user(){
        return $this->belongsToMany('App\User');
    }

    public function contract(){
        return $this->belongsTo('App\Contract');

    }

    public function task()
    {
        return $this->belongsTo('App\Task');
    }
    public function defined_task()
    {
        return $this->belongsTo('App\DefinedTask');
    }
}
