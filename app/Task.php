<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'id',
        'contract_id',
        'contracts_type_id',
        'defined_task_id',
        'subdefined_task_id',
        'employy_id',
        'pocet_ks',
        'trvanie_hod',
        'trvanie_min',
        'pocet_ks_material',
        'cena_cinnost_hod',
        'cena_material_kg',
        'celkova_cena_cinnost',
        'celkova_cena_material',
        'celkova_cena',
        'stav',
        'hodnota_tasku',
        'poznamka',
        'datum',
    ];

    public function user(){
        return $this->belongsToMany('App\User');
    }

    public function contract(){
        return $this->belongsTo('App\Contract');

    }

    public function zakazka(){
        return $this->belongsTo('App\Contract','contract_id');
    }

    public function clients()
    {
        return $this->hasMany('App\Client');
    }
    public function defined_task()
    {
        return $this->hasMany('App\DefinedTask');
    }
    public function subdefined_task()
    {
        return $this->hasMany('App\SubdefinedTask');
    }
    public function employees()
    {
        return $this->hasMany('App\Employees');
    }
    public function zamestnanec()
    {
        return $this->belongsTo('App\Employees','employy_id');
    }
    public function definovanytask()
    {
        return $this->belongsTo('App\DefinedTask','defined_task_id');
    }


}
