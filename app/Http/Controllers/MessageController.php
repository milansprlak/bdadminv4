<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Log;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            if((Auth::check())&&((Auth::user()->mod_9)==1)){
                $stav_spravy =$request->stav;


                if (($request->stav == null)) {
                    $stav_spravy = 1 ;
                }


                $messages = Message::where('stav', '=', $stav_spravy)->orderBy('id', 'DESC')->paginate(100);
                return view('messages.index',['messages'=>$messages, 'stav'=>$stav_spravy]);
            }
            return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view('messages.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()){
            $user_id = Auth::id();
        }
        else
        {
            $user_id=2;
        }
            $message = Message::create([
                'sprava' => $request->input('sprava'),
                'stav' => 1,
                'user_id' => $user_id ,
            ]);


            if($message){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('messages')->info('Použivateľ : '.$message->user_id.' odoslal spravu ID: '.$message->id.' Text spravy: '.$message->sprava.' z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('contracts.index')->with('success_message', 'Správa bola úspešné odoslaná.');
            }


        return back()->withInput()->with('danger_message', 'Nastala chyba.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if((Auth::check())&&((Auth::user()->mod_9)==1)){
            $findmessage = Message::find($id);

            return view('messages.edit',['message'=>$findmessage]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        if((Auth::check())&&((Auth::user()->mod_9)==1)){
            if ($request->input('changestatus') == 1) // Iba meni stav na odoslane a uhradene
            {
                $message = Message::find($message->id);
                $message->stav = $request->input('status');
                $message->save();
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('messages')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' zmenil na vyriešenú správu s ID: '.$message->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('messages.index')->with('success_message', 'Stav správy bol úspešne zmenený.');
            }


            $updated = $message->update([
                'sprava' => $request->input('sprava'),
                'stav' => $request->input('stav'),
                'user_id' => Auth::id()
            ]);
            if (! $updated){
                return back()->withInput();
            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('messages')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' editoval spravu ID: '.$message->id.' Text spravy: '.$message->sprava.' Stav spravy: '.$message->stav.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return redirect()->route('messages.index')
                ->with('success_message','Správa bola úspešne aktualizovaná.');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
