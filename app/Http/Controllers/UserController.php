<?php
// mod_1  = Zákazky: (contracts), (contracts_statuses), (contracts_types), (contracts_names), (tasks), (definedtasks),
//          (subdefinedtasks), (employees), (clients), (upload), (stock), (sendstock)
// mod_2  = Zákazky na odovzdanie (deadline),
// mod_3  = Štatistiky firmy (statistics)
// mod_4  = Štatistiky klientov: (statisticsclient)
// mod_5  = Štatistika úloh (taskstatistics)
// mod_6  = Pokladňa (cash)
// mod_7  = Pripomienky (reminders)
// mod_8  = Denník (diary)
// mod_9  = Správy (messages)         ---- treba urobit
// mod_10 = Faktúry (invoices), (status_invoice), (invoice_payment), (stock), (sendstock), (contractors),
//          (invoice_detail), (price_offers), (clients), (stock), (sendstock), (invoices_dasboard)
// mod_11 = Uživatelia (users), export skladu
// mod_12 =
// mod_13 =
// mod_14 =
// mod_15 =



namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Log;
use Auth;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_11)==1)){
            $users= User::all()->except(1);
            return view('users.index',['users'=>$users]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_11)==1)){
            return view('users.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        if ((Auth::check())&&((Auth::user()->mod_11)==1)){

           $test = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);

           if ($test)
           {
               $user= User::create([
                   'name' => $request->input('name'),
                   'email' => $request->input('email'),
                   'password' => Hash::make($request->input('password')),
                   'mod_1' => $request->input('mod_1'),
                   'mod_2' => $request->input('mod_2'),
                   'mod_3' => $request->input('mod_3'),
                   'mod_4' => $request->input('mod_4'),
                   'mod_5' => $request->input('mod_5'),
                   'mod_6' => $request->input('mod_6'),
                   'mod_7' => $request->input('mod_7'),
                   'mod_8' => $request->input('mod_8'),
                   'mod_9' => $request->input('mod_9'),
                   'mod_10' => $request->input('mod_10'),
                   'mod_11' => $request->input('mod_11'),
                   'mod_12' => 0,
                   'mod_13' => 0,
                   'mod_14' => 0,
                   'mod_15' => 0,

               ]);
               if ($user) {
                   $ip_adresa = $request->getClientIp();
                   $cas = Carbon::now()->toDateTimeString();
                   Log::channel('users')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril uzivatela s ID: '.$user->id. ' Meno = '.$user->name.' E-MAIL = '.$user->email.' S pravami: MOD1='.$user->mod_1.' MOD2='.$user->mod_2.' MOD3='.$user->mod_3.' MOD4='.$user->mod_4.' MOD5='.$user->mod_5.' MOD6='.$user->mod_6.' MOD7='.$user->mod_7.' MOD8='.$user->mod_8.' MOD9='.$user->mod_9.' MOD10='.$user->mod_10.' MOD11='.$user->mod_11.' MOD12='.$user->mod_12.' MOD13='.$user->mod_13.' MOD14='.$user->mod_14.' MOD15='.$user->mod_15.' z IP: '.$ip_adresa.' v čase: '.$cas);

                   return redirect()->route('users.index')->with('success_message', 'Uživateľ bol úspešne vytvorený');
               }

           }
            return back()->withInput()->with('danger_message', 'Nespravne udaje.');

            }

            return back()->withInput()->with('danger_message', 'Vytvorenie uživateľa skončilo chybou.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(($id!=1)&&(Auth::check())&&((Auth::user()->mod_11)==1)){
            $user= User::find($id);
            return view('users.edit',['user'=>$user]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if((Auth::check())&&((Auth::user()->mod_11)==1)) {
            if (($request->input('password')) == null) {
                $updated = $user->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'mod_1' => $request->input('mod_1'),
                    'mod_2' => $request->input('mod_2'),
                    'mod_3' => $request->input('mod_3'),
                    'mod_4' => $request->input('mod_4'),
                    'mod_5' => $request->input('mod_5'),
                    'mod_6' => $request->input('mod_6'),
                    'mod_7' => $request->input('mod_7'),
                    'mod_8' => $request->input('mod_8'),
                    'mod_9' => $request->input('mod_9'),
                    'mod_10' => $request->input('mod_10'),
                    'mod_11' => $request->input('mod_11'),
                    'mod_12' => 0,
                    'mod_13' => 0,
                    'mod_14' => 0,
                    'mod_15' => 0,
                ]);
                if ($updated) {
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('users')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval uzivatela BEZ ZMENY HESLA s ID: '.$user->id. ' Meno = '.$user->name.' E-MAIL = '.$user->email.' S pravami: MOD1='.$user->mod_1.' MOD2='.$user->mod_2.' MOD3='.$user->mod_3.' MOD4='.$user->mod_4.' MOD5='.$user->mod_5.' MOD6='.$user->mod_6.' MOD7='.$user->mod_7.' MOD8='.$user->mod_8.' MOD9='.$user->mod_9.' MOD10='.$user->mod_10.' MOD11='.$user->mod_11.' MOD12='.$user->mod_12.' MOD13='.$user->mod_13.' MOD14='.$user->mod_14.' MOD15='.$user->mod_15.' z IP: '.$ip_adresa.' v čase: '.$cas);
                    return redirect()->route('users.index')->with('success_message', 'U6ivateľ bol úspešne, bez zmeny hesla');
                }
            } else if (($request->input('password'))==($request->input('password_confirmation')))
            {
                $updated = $user->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => Hash::make($request->input('password')),
                    'mod_1' => $request->input('mod_1'),
                    'mod_2' => $request->input('mod_2'),
                    'mod_3' => $request->input('mod_3'),
                    'mod_4' => $request->input('mod_4'),
                    'mod_5' => $request->input('mod_5'),
                    'mod_6' => $request->input('mod_6'),
                    'mod_7' => $request->input('mod_7'),
                    'mod_8' => $request->input('mod_8'),
                    'mod_9' => $request->input('mod_9'),
                    'mod_10' => $request->input('mod_10'),
                    'mod_11' => $request->input('mod_11'),
                    'mod_12' => 0,
                    'mod_13' => 0,
                    'mod_14' => 0,
                    'mod_15' => 0,
                ]);
                if ($updated) {
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('users')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval uzivatela SO ZMENOU HESLA ID: '.$user->id. ' Meno = '.$user->name.' E-MAIL = '.$user->email.' S pravami: MOD1='.$user->mod_1.' MOD2='.$user->mod_2.' MOD3='.$user->mod_3.' MOD4='.$user->mod_4.' MOD5='.$user->mod_5.' MOD6='.$user->mod_6.' MOD7='.$user->mod_7.' MOD8='.$user->mod_8.' MOD9='.$user->mod_9.' MOD10='.$user->mod_10.' MOD11='.$user->mod_11.' MOD12='.$user->mod_12.' MOD13='.$user->mod_13.' MOD14='.$user->mod_14.' MOD15='.$user->mod_15.' z IP: '.$ip_adresa.' v čase: '.$cas);
                    return redirect()->route('users.index')->with('success_message', 'U6ivateľ bol úspešne, so zmenou hesla');
                }
            }
            else if  (($request->input('password'))!=($request->input('password_confirmation')))
            {
                return back()->withInput()->with('danger_message', 'Nastala chyba: hesla sa nezhoduju!');
            }
            else
            {
                return back()->withInput()->with('danger_message', 'Nastala chyba!');
            }

        }

        return back()->withInput()->with('danger_message', 'Nastala chyba!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
