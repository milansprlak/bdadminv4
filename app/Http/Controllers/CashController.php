<?php

namespace App\Http\Controllers;

use App\Cash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class CashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_6)==1)){
            $fromdatum =$request->fromdatum;
            $todatum =$request->todatum;

            if (($request->fromdatum == null) || ($request->todatum == null)) {
               // $fromdatum = date("Y-m-d", strtotime('-1 year'));
                $fromdatum = date("Y-01-01");
                $todatum = date("Y-m-d");
            }


            $cashes= Cash::whereDate('created_at', '>=', $fromdatum)->whereDate('created_at', '<=', $todatum)->orderBy('id', 'DESC')->paginate(100);
            // Štatistiky
            $sum_1ucet = Cash::where('typ', 1)->sum('suma');
            $sum_2ucet = Cash::where('typ', 2)->sum('suma');
            $celkova_suma = $sum_1ucet + $sum_2ucet;
            //

            return view('cashes.index',['cashes'=>$cashes,'fromdatum'=>$fromdatum,'todatum'=>$todatum,'sum_1ucet'=>$sum_1ucet,'sum_2ucet'=>$sum_2ucet,'celkova_suma'=>$celkova_suma]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_6)==1)){
            return view('cashes.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_6)==1)) {
            $operacia = $request->input('operacia');
            $suma = str_replace(",", ".",$request->input('suma'));
            if (($operacia == 2)&&($suma>0)) {
                $suma = -1*$suma;
            }
            if (($operacia == 1)&&($suma<0)) {
                $suma = -1*$suma;
            }
            $cash = Cash::create([
                'typ' => $request->input('typuctu'),
                'poznamka' => $request->input('poznamka'),
                'suma' => $suma,
                'user_id' => Auth::id(),


            ]);
            if($cash ){

                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('cash')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril do pokladne záznam s ID: '.$cash->id.' Typ účtu: '.$cash->typ.' Suma: '.$cash->suma.' € Poznámka: '.$cash->poznamka.' z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('cash.index');
            }
        }

        return back()->withInput()->with('errors', 'Vyskytla sa chyba.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cash  $cash
     * @return \Illuminate\Http\Response
     */
    public function show(Cash $cash)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cash  $cash
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $findcash = Cash::find($id);
        if(($findcash->created_at->isToday())&&(((Auth::check())&&((Auth::user()->mod_6)==1)))){

            return view('cashes.edit',['cashes'=>$findcash]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cash  $cash
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cash $cash)
    {
        if((Auth::check())&&((Auth::user()->mod_6)==1)){
            $operacia = $request->input('operacia');
            $suma = str_replace(",", ".",$request->input('suma'));
            if (($operacia == 2)&&($suma>0)) {
                $suma = -1*$suma;
            }
            if (($operacia == 1)&&($suma<0)) {
                $suma = -1*$suma;
            }
            $updated = $cash->update([
                'typ' => $request->input('typuctu'),
                'poznamka' => $request->input('poznamka'),
                'suma' => $suma,
                'user_id' => Auth::id()
            ]);
            if (! $updated){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('cash')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' sa pokúsil pridať záznam, avšak skončilo to chybou. IP: '.$ip_adresa.' čas: '.$cas);
                return back()->withInput();
            }
            else
            {
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('cash')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' upravil záznam s ID: '.$cash->id.' do pokladne, Typ účtu: '.$cash->typ.' Suma: ' .$cash->suma. ' € Poznámka: '.$cash->poznamka.' z IP: '.$ip_adresa.' v čase: '.$cas);
            }
            return redirect()->route('cash.index')
                ->with('success','Defined task update successfully');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cash  $cash
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cash $cash)
    {
        //
    }
}
