<?php

namespace App\Http\Controllers;

use App\InvoiceDetail;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class InvoiceDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            // pomocne premenne a vypocty
            $invoice_id = $request->input('invoice_id');
            $pocet_ks = $request->input('pocet_ks');
            $cena_bez_dph_ks = str_replace(",", ".", $request->input('cenabezdph'));
            $vyska_dph = $request->input('vyskadph');

            $cena_bez_dph = $cena_bez_dph_ks*$pocet_ks;
            $celkova_cena = $cena_bez_dph + ($cena_bez_dph*($vyska_dph/100));
            $ciastka_dph =  $celkova_cena - $cena_bez_dph ;
            $kontrolaskladu =true;


            // Odpocet mnozstva zo skladu
            if ($request->input('product_id')!=1)
            {
                $model = Stock::find($request->input('product_id'));
                //kontrola ci nie je zaporna hodnota
                $kontrola_ks = $model->pocet_ks - $request->input('pocet_ks');
                if ($kontrola_ks < 0)
                {
                    return back()->withInput()->with('danger_message', 'Na sklade nie je dostatok kusov daného produktu !!!');
                }
                //
                else
                {
                    $model->pocet_ks -= $request->input('pocet_ks');
                }


                if (!$model->save())
                {
                    $kontrolaskladu =false;
                }
                else
                {
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('stock')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vybral zo skladu produkt ID= '.$model->id.' pocet ks = '.$request->input('pocet_ks').'ks a vlozil do faktury id= '.$invoice_id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                }
            }
            //


            $invoice_detail = InvoiceDetail::create([
                'invoice_id' => $invoice_id,
                'stock_id' => $request->input('product_id'),
                'nazov_polozky' => $request->input('nazovpolozky'),
                'pocet_ks' => $pocet_ks,
                'cena_bez_dph_ks' => number_format($cena_bez_dph_ks, 2, '.', ''),
                'celkova_cena_bez_dph' => number_format($cena_bez_dph, 2, '.', ''),
                'celkova_cena' => number_format($celkova_cena, 2, '.', ''),
                'ciastka_dph' => number_format($ciastka_dph, 2, '.', ''),
                'vyska_dph' => $vyska_dph,
                'jednotka' => $request->input('jednotka'),
                'user_id' => Auth::id()


            ]);

            if($invoice_detail&&$kontrolaskladu){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('invoicedetail')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' pridal do faktury s ID: '.$invoice_id.' polozku s ID: '.$invoice_detail->id.' stock_id = '.$invoice_detail->stock_id.' nazov_polozky= '.$invoice_detail->nazov_polozky.' pocet_ks= '.$invoice_detail->pocet_ks.' cena_bez_dph_ks= '.$invoice_detail->cena_bez_dph_ks.' vyska_dph= '.$invoice_detail->vyska_dph.'%  z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('invoice.show', ['invoice'=> $invoice_id])->with('success_message', 'Položka bola úspešne pridaná do faktúry');
            }
        }

        return back()->withInput()->with('danger_message', 'Vytvorenie faktúry skončilo chybou.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvoiceDetail  $invoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceDetail $invoiceDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvoiceDetail  $invoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceDetail $invoiceDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvoiceDetail  $invoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $invoiceDetail = InvoiceDetail::findOrFail($request->item_id);
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
        // pomocne premenne a vypocty
        $pocet_ks_old = $invoiceDetail->pocet_ks;
        $kontrolaskladu =true;
        $invoice_id = $request->input('editinvoice_id');
        $pocet_ks = $request->input('editpocet_ks');
        $cena_bez_dph_ks = str_replace(",", ".", $request->input('editcenabezdph'));
        $vyska_dph = $request->input('editvyskadph');

        $cena_bez_dph = $cena_bez_dph_ks*$pocet_ks;
        $celkova_cena = $cena_bez_dph + ($cena_bez_dph*($vyska_dph/100));
        $ciastka_dph =  $celkova_cena - $cena_bez_dph ;

            // Pripocitanie alebo Odpocet mnozstva zo skladu
            if ($request->input('editproduct_id')!=1)
            {
                if ($pocet_ks_old > $pocet_ks)
                {
                    $model = Stock::find($request->input('editproduct_id'));
                    $pocet_ks_rozdiel = $pocet_ks_old - $pocet_ks;
                    $model->pocet_ks += $pocet_ks_rozdiel;


                    if (!$model->save())
                    {
                        $kontrolaskladu =false;
                    }
                    else
                    {
                        $ip_adresa = $request->getClientIp();
                        $cas = Carbon::now()->toDateTimeString();
                        Log::channel('stock')->info('Použivateľ : '.Auth::user()->name.' ID : '.Auth::user()->id.' vlozil do skladu produkt ID = '.$model->id.' pocet ks = '.$pocet_ks_rozdiel.'ks a aktualizoval polozku faktury ID:'. $invoiceDetail->id.' vo fakture id= '.$invoice_id.' z IP: '.$ip_adresa.' v čase: '.$cas);

                    }

                }
                else if ($pocet_ks_old < $pocet_ks)
                {
                    $model = Stock::find($request->input('editproduct_id'));

                    //kontrola ci nie je zaporna hodnota
                    $pocet_ks_rozdiel = $pocet_ks - $pocet_ks_old;
                    $kontrola_ks = $model->pocet_ks - $pocet_ks_rozdiel;
                    if ($kontrola_ks < 0)
                    {
                        return back()->withInput()->with('danger_message', 'Na sklade nie je dostatok kusov daného produktu !!!');
                    }
                    else
                    {
                        $model->pocet_ks -= $pocet_ks_rozdiel;
                    }
                    //

                    if (!$model->save())
                    {
                        $kontrolaskladu =false;
                    }
                    else
                    {
                        $ip_adresa = $request->getClientIp();
                        $cas = Carbon::now()->toDateTimeString();
                        Log::channel('stock')->info('Použivateľ : '.Auth::user()->name.' ID : '.Auth::user()->id.' vybral zo skladu produkt ID = '.$model->id.' pocet ks = '.$pocet_ks_rozdiel.'ks a aktualizoval polozku faktury ID:'. $invoiceDetail->id.' vo fakture id= '.$invoice_id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                    }

                }
                else
                {
                    $kontrolaskladu =true;
                }

            }
            //


            $updated = $invoiceDetail->update([
                'invoice_id' => $invoice_id,
                'stock_id' => $request->input('editproduct_id'),
                'nazov_polozky' => $request->input('editnazovpolozky'),
                'pocet_ks' => $pocet_ks,
                'cena_bez_dph_ks' => number_format($cena_bez_dph_ks, 2, '.', ''),
                'celkova_cena_bez_dph' => number_format($cena_bez_dph, 2, '.', ''),
                'celkova_cena' => number_format($celkova_cena, 2, '.', ''),
                'ciastka_dph' => number_format($ciastka_dph, 2, '.', ''),
                'vyska_dph' => $vyska_dph,
                'jednotka' => $request->input('editjednotka'),
                'user_id' => Auth::id()
            ]);



            if (($updated)&&($kontrolaskladu)){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('invoicedetail')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval do faktury s ID: '.$invoice_id.' polozku s ID: '.$invoiceDetail->id.' na stock_id = '.$invoiceDetail->stock_id.' nazov_polozky= '.$invoiceDetail->nazov_polozky.' pocet_ks= '.$invoiceDetail->pocet_ks.' cena_bez_dph_ks= '.$invoiceDetail->cena_bez_dph_ks.' vyska_dph= '.$invoiceDetail->vyska_dph.'%  z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('invoice.show', ['invoice'=> $invoice_id])->with('success_message', 'Položka bola úspešne aktualizovaná');

            }
            return back()->withInput()->with('danger_message', 'Vytvorenie faktúry skončilo chybou.');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvoiceDetail  $invoiceDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
        $invoice_id =$request->input('invoice_id');

        try {
            $invoiceDetail = InvoiceDetail::find($id);
            // Pridanie do skladu mnozstvo

            if ($invoiceDetail->stock_id !=1)
            {
                $model = Stock::find($invoiceDetail->stock_id);
                $model->pocet_ks += $invoiceDetail->pocet_ks;
                $model->save();
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('stock')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vlozil do skladu s produktom ID= '.$model->id.' pocet ks = '.$invoiceDetail->pocet_ks.'ks a odstranil  polozku faktury id = '.$invoiceDetail->id. ' z faktury ID = ' .$invoice_id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                Log::channel('invoicedetail')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' odstranil z faktury s ID: '.$invoice_id.' polozku s ID: '.$invoiceDetail->id.' na stock_id = '.$invoiceDetail->stock_id.' nazov_polozky= '.$invoiceDetail->nazov_polozky.' pocet_ks= '.$invoiceDetail->pocet_ks.' cena_bez_dph_ks= '.$invoiceDetail->cena_bez_dph_ks.' vyska_dph= '.$invoiceDetail->vyska_dph.'%  z IP: '.$ip_adresa.' v čase: '.$cas);

            }
            //
            $invoiceDetail->delete();

            return redirect()->route('invoice.show', ['invoice'=> $invoice_id])->with('success_message', 'Položka bola úspešne odstranená z faktúry');
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
    }
        return back();
        }
}
