<?php

namespace App\Http\Controllers;

use App\DownloadFile;
use App\Contract;
use App\Employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class DownloadFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contract= Contract::all();
        $download_files= DownloadFile::all();
        $employy= Employees::all();
        return view('upload.index',['contract'=>$contract,'download_files'=>$download_files,'employy'=>$employy]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contract= Contract::all();
        $employy= Employees::all();
        return view('upload.create',['contract'=>$contract,'employy'=>$employy]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** Treba zmenit v php.ini post_max_size a upload_max_filesize */
    public function store(Request $request)
    {
        $request->validate([
            'fileToUpload' => 'required|file',
        ]);

        $fileName = time(). request()->fileToUpload->getClientOriginalName();

        $request->fileToUpload->storeAs('files',$fileName);

        // zapis do DB

        $files = DownloadFile::create([
            'nazov' => $request->fileToUpload->getClientOriginalName(),
            'url' => ('/storage/files/').time(). $request->fileToUpload->getClientOriginalName(),
            'contract_id' => $request->input('zakazka_id'),
            'employy_id' => $request->input('zamestnanec_id')


        ]);
        //

        return back()
            ->with('success','Úspešne ste odovzdali súbor.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DownloadFile  $downloadFile
     * @return \Illuminate\Http\Response
     */
    public function show(DownloadFile $downloadFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DownloadFile  $downloadFile
     * @return \Illuminate\Http\Response
     */
    public function edit(DownloadFile $downloadFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DownloadFile  $downloadFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DownloadFile $downloadFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DownloadFile  $downloadFile
     * @return \Illuminate\Http\Response
     */




    public function destroy($id, Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        try {
        $findfile = DownloadFile::find($id);
        // Treba nastaviť cestu, zistíte to z documen root
        //unlink($_SERVER['DOCUMENT_ROOT'] .$findfile->url);
         unlink('/nfsmnt/hosting1_2/f/0/f08948d4-f4ae-484d-8c25-f2891d22372d/bdadmin.eu/web/badmin/public' .$findfile->url);

            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('uploadfiles')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' odstranil subor s ID: '.$findfile->id.' Názov: '.$findfile->nazov.' URL: '.$findfile->url.' Contract id: '.$findfile->contract_id.' Zamestnanec id: '.$findfile->employy_id.' z IP: '.$ip_adresa.' v čase: '.$cas);
            $findfile ->delete();
            return redirect()->route('contracts.index');
             }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        }
        return back();

    }
}
