<?php

namespace App\Http\Controllers;

use App\PriceOfferDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Log;

class PriceOfferDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            // pomocne premenne a vypocty
            $priceoffer_id = $request->input('priceoffer_id');
            $pocet_ks = $request->input('pocet_ks');
            $cena_bez_dph_ks = str_replace(",", ".", $request->input('cenabezdph'));
            $vyska_dph = $request->input('vyskadph');

            $cena_bez_dph = $cena_bez_dph_ks*$pocet_ks;
            $celkova_cena = $cena_bez_dph + ($cena_bez_dph*($vyska_dph/100));
            $ciastka_dph =  $celkova_cena - $cena_bez_dph ;



            $priceoffer_detail = PriceOfferDetail::create([
                'price_offer_id' => $priceoffer_id,
                'nazov_polozky' => $request->input('nazovpolozky'),
                'jednotka' => $request->input('jednotka'),
                'pocet_ks' => $pocet_ks,
                'cena_bez_dph_ks' => number_format($cena_bez_dph_ks, 2, '.', ''),
                'celkova_cena_bez_dph' => number_format($cena_bez_dph, 2, '.', ''),
                'celkova_cena' => number_format($celkova_cena, 2, '.', ''),
                'ciastka_dph' => number_format($ciastka_dph, 2, '.', ''),
                'vyska_dph' => $vyska_dph,
                'user_id' => Auth::id()


            ]);

            if($priceoffer_detail){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('priceofferdetail')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' pridal do cenovej ponuky s ID: '.$priceoffer_id.' polozku s ID: '.$priceoffer_detail->id.' nazov_polozky= '.$priceoffer_detail->nazov_polozky.' pocet_ks= '.$priceoffer_detail->pocet_ks.' cena_bez_dph_ks= '.$priceoffer_detail->cena_bez_dph_ks.' vyska_dph= '.$priceoffer_detail->vyska_dph.'%  z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('price_offers.show', ['price_offers'=> $priceoffer_id])->with('success_message', 'Položka bola úspešne pridaná do faktúry');
            }
        }

        return back()->withInput()->with('danger_message', 'Vytvorenie faktúry skončilo chybou.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PriceOfferDetail  $priceOfferDetail
     * @return \Illuminate\Http\Response
     */
    public function show(PriceOfferDetail $priceOfferDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PriceOfferDetail  $priceOfferDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(PriceOfferDetail $priceOfferDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PriceOfferDetail  $priceOfferDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $priceOfferDetail = PriceOfferDetail::findOrFail($request->item_id);
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            // pomocne premenne a vypocty

            $priceoffer_id = $request->input('editpriceoffer_id');
            $pocet_ks = $request->input('editpocet_ks');
            $cena_bez_dph_ks = str_replace(",", ".", $request->input('editcenabezdph'));
            $vyska_dph = $request->input('editvyskadph');

            $cena_bez_dph = $cena_bez_dph_ks*$pocet_ks;
            $celkova_cena = $cena_bez_dph + ($cena_bez_dph*($vyska_dph/100));
            $ciastka_dph =  $celkova_cena - $cena_bez_dph ;


            $updated = $priceOfferDetail->update([
                'price_offer_id' => $priceoffer_id,
                'nazov_polozky' => $request->input('editnazovpolozky'),
                'jednotka' => $request->input('editjednotka'),
                'pocet_ks' => $pocet_ks,
                'cena_bez_dph_ks' => number_format($cena_bez_dph_ks, 2, '.', ''),
                'celkova_cena_bez_dph' => number_format($cena_bez_dph, 2, '.', ''),
                'celkova_cena' => number_format($celkova_cena, 2, '.', ''),
                'ciastka_dph' => number_format($ciastka_dph, 2, '.', ''),
                'vyska_dph' => $vyska_dph,
                'user_id' => Auth::id()
            ]);



            if ($updated){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('priceofferdetail')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval do cenovej ponuky s ID: '.$priceoffer_id.' polozku s ID: '.$priceOfferDetail->id.' nazov_polozky= '.$priceOfferDetail->nazov_polozky.' pocet_ks= '.$priceOfferDetail->pocet_ks.' cena_bez_dph_ks= '.$priceOfferDetail->cena_bez_dph_ks.' vyska_dph= '.$priceOfferDetail->vyska_dph.'%  z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('price_offers.show', ['price_offers'=> $priceoffer_id])->with('success_message', 'Položka bola úspešne aktualizovaná');

            }
            return back()->withInput()->with('danger_message', 'Aktualizovanie cenovej ponuky skončilo chybou.');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PriceOfferDetail  $priceOfferDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $priceoffer_id =$request->input('priceoffer_id');

            try {
                $priceofferDetail = PriceOfferDetail::find($id);
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('priceofferdetail')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' odstranil z cenovej ponuky s ID: '.$priceoffer_id.' polozku s ID: '.$priceofferDetail->id.' nazov_polozky= '.$priceofferDetail->nazov_polozky.' pocet_ks= '.$priceofferDetail->pocet_ks.' cena_bez_dph_ks= '.$priceofferDetail->cena_bez_dph_ks.' vyska_dph= '.$priceofferDetail->vyska_dph.'%  z IP: '.$ip_adresa.' v čase: '.$cas);
                $priceofferDetail->delete();

                return redirect()->route('price_offers.show', ['price_offers'=> $priceoffer_id])->with('success_message', 'Položka bola úspešne odstranená z cenovej ponuky');
            }
            catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return back();
    }
}
