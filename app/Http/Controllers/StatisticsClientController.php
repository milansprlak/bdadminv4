<?php

namespace App\Http\Controllers;

use App\ContractStatistic;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class StatisticsClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ((Auth::check())&&((Auth::user()->mod_4)==1)){
            $clients = Client::all();
            $fromdatum = $request->fromdatum;
            $todatum = $request->todatum;
            $definovane_obdobie = $request->obdobie_value;

            if ($definovane_obdobie == null || $definovane_obdobie == 0) {
                $definovane_obdobie = 0;
                if (($fromdatum == null) || ($todatum == null)) {

                    $fromdatum = date("Y-01-01");
                    $todatum = date("Y-m-d");
                } else {
                    $fromdatum = $request->fromdatum;
                    $todatum = $request->todatum;
                }
                foreach ($clients as $client) {
                    $obchodne_meno = $client->nazov_firmy;
                    $sum_celkova_predajna_cena = ContractStatistic::whereDate('updated_at', '>=', $fromdatum)
                        ->whereDate('updated_at', '<=', $todatum)->where('client_id', '=', $client->id)->sum('celkova_predajna_cena');
                    $sum_celkovy_zisk = ContractStatistic::whereDate('updated_at', '>=', $fromdatum)
                        ->whereDate('updated_at', '<=', $todatum)->where('client_id', '=', $client->id)->sum('zisk');
                    $sum_celkovy_cas = ContractStatistic::whereDate('updated_at', '>=', $fromdatum)
                        ->whereDate('updated_at', '<=', $todatum)->where('client_id', '=', $client->id)->sum('celkovy_cas');
                    $sum_celkovy_cas_hod = floor($sum_celkovy_cas / 60);
                    $sum_celkovy_cas_min  = $sum_celkovy_cas % 60;
                    $klient[] = ['obchodne_meno' => $obchodne_meno, 'sum_celkova_predajna_cena' => $sum_celkova_predajna_cena, 'sum_celkovy_zisk' => $sum_celkovy_zisk,'sum_celkovy_cas_hod' => $sum_celkovy_cas_hod,'sum_celkovy_cas_min' => $sum_celkovy_cas_min, 'sum_celkovy_cas' => $sum_celkovy_cas];
                }

            } else
            {
                foreach ($clients as $client) {
                    $obchodne_meno = $client->nazov_firmy;
                    $sum_celkova_predajna_cena = ContractStatistic::where('client_id', '=', $client->id)->sum('celkova_predajna_cena');
                    $sum_celkovy_zisk = ContractStatistic::where('client_id', '=', $client->id)->sum('zisk');
                    $sum_celkovy_cas = ContractStatistic::where('client_id', '=', $client->id)->sum('celkovy_cas');
                    $sum_celkovy_cas_hod = floor($sum_celkovy_cas / 60);
                    $sum_celkovy_cas_min  = $sum_celkovy_cas % 60;
                    $klient[] = ['obchodne_meno' => $obchodne_meno, 'sum_celkova_predajna_cena' => $sum_celkova_predajna_cena, 'sum_celkovy_zisk' => $sum_celkovy_zisk,'sum_celkovy_cas_hod' => $sum_celkovy_cas_hod,'sum_celkovy_cas_min' => $sum_celkovy_cas_min, 'sum_celkovy_cas' => $sum_celkovy_cas];
                }
            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('statisticsclient')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' zobrazil statistiky klientov  z IP: '.$ip_adresa.' v čase: '.$cas);
            return view('statistics.statisticsclient', ['klient' => $klient,'fromdatum' => $fromdatum, 'todatum' => $todatum, 'definovane_obdobie' => $definovane_obdobie]);
        }
     return back();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContractStatistic  $contractStatistic
     * @return \Illuminate\Http\Response
     */
    public function show(ContractStatistic $contractStatistic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContractStatistic  $contractStatistic
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractStatistic $contractStatistic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContractStatistic  $contractStatistic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContractStatistic $contractStatistic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContractStatistic  $contractStatistic
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractStatistic $contractStatistic)
    {
        //
    }
}
