<?php

namespace App\Http\Controllers;

use App\SubdefinedTask;
use App\DefinedTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Log;

class SubdefinedTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $subdefinedTasks= SubdefinedTask::all();
        $definedtasks=DefinedTask::all();
        return view('subdefined_tasks.index',['definedTasks'=>$definedtasks,'subdefinedTasks'=>$subdefinedTasks]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $definedtask=DefinedTask::all();
        return view('subdefined_tasks.create',['definedtask'=>$definedtask]);
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $definedtask=DefinedTask::all();
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            $subdefinedtask = SubdefinedTask::create([
                'nazov' => $request->input('nazov'),
                'nadradena_kat_id' => $request->input('nadradena_kat_id'),
                'cena_material_kg' => $request->input('cena_material_kg')

            ]);
            if($subdefinedtask){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('subdefinedtask')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril podkategoriu s ID: '.$subdefinedtask->id.' Nazov = '.$subdefinedtask->nazov.' Nadradena kat id = :'.$subdefinedtask->nadradena_kat_id.' Cena material kg = '.$subdefinedtask->cena_material_kg.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('subdefinedtasks.index');
            }
        }

        return back()->withInput()->with('errors', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubdefinedTask  $subdefinedTask
     * @return \Illuminate\Http\Response
     */
    public function show(SubdefinedTask $subdefinedTask)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubdefinedTask  $subdefinedTask
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $findsubdefinedTask= SubdefinedTask::find($id);
        $definedtask=DefinedTask::all();
        return view('subdefined_tasks.edit',['subdefinedTask'=>$findsubdefinedTask,'definedTask'=>$definedtask]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubdefinedTask  $subdefinedTask
     * @return \Illuminate\Http\Response
     */
  /*  public function update(Request $request, SubdefinedTask $subdefinedTask)
    {
        $updated = $subdefinedTask->update([
            'nazov' => $request->input('nazov'),
            'nadradena_kat_id' => $request->input('nadradena_kat_id')
        ]);

        if (! $updated){

            return back()->withInput();
        }
        return redirect()->route('subdefinedtasks.index')
            ->with('success','Defined task update successfully');

    } */
    public function update($id, Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        try {
            $task = SubdefinedTask::findOrFail($id);

            $this->validate($request, [
                'nazov' => 'required',
                'nadradena_kat_id' => 'required'
            ]);

            $input = $request->all();

            $task->fill($input)->save();

            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('subdefinedtask')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval podkategoriu s ID: '.$task->id.' na Nazov = '.$task->nazov.' Nadradena kat id = :'.$task->nadradena_kat_id.' Cena material kg = '.$task->cena_material_kg.' z IP: '.$ip_adresa.' v čase: '.$cas);

            return redirect()->route('subdefinedtasks.index');

        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        }
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubdefinedTask  $subdefinedTask
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        try {
            $findsubdefinedTask = SubdefinedTask::find($id);

            $findsubdefinedTask ->delete();
            return redirect()->route('subdefinedtasks.index');
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        }
        return back();
    }
}
