<?php

namespace App\Http\Controllers;

use App\Task;
use App\DefinedTask;
use Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskStatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ((Auth::check())&&((Auth::user()->mod_5)==1)) {

            $definedTasks = DefinedTask::all();
            $fromdatum = $request->fromdatum;
            $todatum = $request->todatum;
            $definovane_obdobie = $request->obdobie_value;

            if ($definovane_obdobie == null || $definovane_obdobie == 0) {
                $definovane_obdobie = 0;
                if (($fromdatum == null) || ($todatum == null)) {

                    $fromdatum = date("Y-01-01");
                    $todatum = date("Y-m-d");
                } else {
                    $fromdatum = $request->fromdatum;
                    $todatum = $request->todatum;
                }
                foreach ($definedTasks as $definedTask) {
                    $nazov_ulohy = $definedTask->nazov;
                    $trvaniehod= Task::whereDate('updated_at', '>=', $fromdatum)
                        ->whereDate('updated_at', '<=', $todatum)->where('defined_task_id', '=', $definedTask->id)->sum('trvanie_hod');
                    $trvaniemin= Task::whereDate('updated_at', '>=', $fromdatum)
                        ->whereDate('updated_at', '<=', $todatum)->where('defined_task_id', '=', $definedTask->id)->sum('trvanie_min');
                    $cena= Task::whereDate('updated_at', '>=', $fromdatum)
                        ->whereDate('updated_at', '<=', $todatum)->where('defined_task_id', '=', $definedTask->id)->sum('celkova_cena_cinnost');

                    $sum_celkovy_cas=($trvaniehod*60)+$trvaniemin;
                    $sum_celkovy_cas_hod = floor($sum_celkovy_cas / 60);
                    $sum_celkovy_cas_min  = $sum_celkovy_cas % 60;
                    $task[] = ['nazov_ulohy' => $nazov_ulohy, 'sum_celkovy_cas_hod' => $sum_celkovy_cas_hod,'sum_celkovy_cas_min' => $sum_celkovy_cas_min, 'cena' => $cena];
                }

            } else
            {
                foreach ($definedTasks as $definedTask) {
                    $nazov_ulohy = $definedTask->nazov;
                    $trvaniehod= Task::where('defined_task_id', '=', $definedTask->id)->sum('trvanie_hod');
                    $trvaniemin= Task::where('defined_task_id', '=', $definedTask->id)->sum('trvanie_min');
                    $cena= Task::where('defined_task_id', '=', $definedTask->id)->sum('celkova_cena_cinnost');
                    $sum_celkovy_cas=($trvaniehod*60)+$trvaniemin;
                    $sum_celkovy_cas_hod = floor($sum_celkovy_cas / 60);
                    $sum_celkovy_cas_min  = $sum_celkovy_cas % 60;
                    $task[] = ['nazov_ulohy' => $nazov_ulohy, 'sum_celkovy_cas_hod' => $sum_celkovy_cas_hod,'sum_celkovy_cas_min' => $sum_celkovy_cas_min, 'cena' => $cena];
                }
            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('statisticstasks')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' zobrazil statistiku uloh z IP: '.$ip_adresa.' v čase: '.$cas);
            return view('statistics.taskstatistics', ['task' => $task,'fromdatum' => $fromdatum, 'todatum' => $todatum, 'definovane_obdobie' => $definovane_obdobie]);
        }
        return back();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
