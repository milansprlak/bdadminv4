<?php

namespace App\Http\Controllers;

use App\Contractor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $contractors = Contractor::all();
            return view('contractors.index',['contractors'=>$contractors]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            return view('contractors.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $contractor = Contractor::create([
                'nazov_firmy' => $request->input('nazov_firmy'),
                'kontaktna_osoba' => $request->input('kontaktna_osoba'),
                'ico' => $request->input('ico'),
                'dic' => $request->input('dic'),
                'icdph' => $request->input('icdph'),
                'fa_adresa' => $request->input('fa_adresa'),
                'fa_mesto' => $request->input('fa_mesto'),
                'fa_psc' => $request->input('fa_psc'),
                'fa_krajina' => $request->input('fa_krajina'),
                'telefon' => $request->input('telefon'),
                'mobil' => $request->input('mobil'),
                'email' => $request->input('email'),
                'www_site' => $request->input('www_site'),
                'nazov_banky' => $request->input('nazov_banky'),
                'iban' => $request->input('iban'),
                'swift' => $request->input('swift'),

            ]);
            if($contractor){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('contractor')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril dodávateľa (contractor) s ID: '.$contractor->id.' Názov firmy: '.$contractor->nazov_firmy.' Kontaktná osoba: '.$contractor->kontaktna_osoba.' IČO: '.$contractor->ico.' DIČ: '.$contractor->dic.' IČDPH: '.$contractor->icdph.' Fakturačná adresa: '.$contractor->fa_adresa.' Mesto: '.$contractor->fa_mesto.' PSČ: '.$contractor->fa_psc.' Krajina: '.$contractor->fa_krajina.' Telefón: '.$contractor->telefon.' Mobil: '.$contractor->mobil.' E-MAIL: '.$contractor->email.' WWW: '.$contractor->www_site.' Názov banky: '.$contractor->nazov_banky.' IBAN: '.$contractor->iban.' SWIFT: '.$contractor->swift.' z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('contractors.index')->with('success_message', 'Dodávateľ bol úspešne vytvorený');
            }
        }

        return back()->withInput()->with('danger_message', 'Vytvorenie klienta skončilo chybou.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function show(Contractor $contractor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $findcontractor = Contractor::find($id);
            return view('contractors.edit',['contractor'=>$findcontractor]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contractor $contractor)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $updated = $contractor->update([
                'nazov_firmy' => $request->input('nazov_firmy'),
                'kontaktna_osoba' => $request->input('kontaktna_osoba'),
                'ico' => $request->input('ico'),
                'dic' => $request->input('dic'),
                'icdph' => $request->input('icdph'),
                'fa_adresa' => $request->input('fa_adresa'),
                'fa_mesto' => $request->input('fa_mesto'),
                'fa_psc' => $request->input('fa_psc'),
                'fa_krajina' => $request->input('fa_krajina'),
                'telefon' => $request->input('telefon'),
                'mobil' => $request->input('mobil'),
                'email' => $request->input('email'),
                'www_site' => $request->input('www_site'),
                'nazov_banky' => $request->input('nazov_banky'),
                'iban' => $request->input('iban'),
                'swift' => $request->input('swift'),
            ]);
            if (! $updated){
                return back()->withInput();
            }
            // Logovanie
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('contractor')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval dodávateľa (contractor) s ID: '.$contractor->id.' Názov firmy: '.$contractor->nazov_firmy.' Kontaktná osoba: '.$contractor->kontaktna_osoba.' IČO: '.$contractor->ico.' DIČ: '.$contractor->dic.' IČDPH: '.$contractor->icdph.' Fakturačná adresa: '.$contractor->fa_adresa.' Mesto: '.$contractor->fa_mesto.' PSČ: '.$contractor->fa_psc.' Krajina: '.$contractor->fa_krajina.' Telefón: '.$contractor->telefon.' Mobil: '.$contractor->mobil.' E-MAIL: '.$contractor->email.' WWW: '.$contractor->www_site.' Názov banky: '.$contractor->nazov_banky.' IBAN: '.$contractor->iban.' SWIFT: '.$contractor->swift.' z IP: '.$ip_adresa.' v čase: '.$cas);
            // END Log
            return redirect()->route('contractors.index')
                ->with('success_message','Dodávateľ bol úspešne aktualizovaý.');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contractor $contractor)
    {
        //
    }
}
