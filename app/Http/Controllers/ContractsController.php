<?php

namespace App\Http\Controllers;


use App\Contract;
use App\SendStock;
use App\Stock;
use Carbon\Carbon;
use App\DefinedTask;
use App\SubdefinedTask;
use App\Task;
use App\Employees;
use App\Client;
use App\ContractsStatus;
use App\ContractsType;
use App\ContractStatistic;
use App\ContractsName;
use App\DownloadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;
use Log;

class ContractsController extends Controller
{

    /**
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contractstatus = ContractsStatus::all();

        $clients = Client::all();

        if (($request->fromdatum == null) || ($request->todatum == null)) {
            $value_stav_zakazky = 1;
            $value_klient = 0;
            $contracts = Contract::where('stav_zakazky', '1')->where('contracts_type_id', '!=', '4')
                ->orderBy('id', 'DESC')
                ->paginate(100);

            // $from = date("Y-m-d", strtotime('-31 days'));
            $from = ('2018-06-01');
            $to = date("Y-m-d");
        } else {
            $value_stav_zakazky = $request->stav_zakazky;
            $value_klient = $request->klient;
            $from = $request->fromdatum;
            $to = $request->todatum;

            if ($request->klient == 0) {
                $contracts = Contract::where('stav_zakazky', $request->stav_zakazky)
                    ->where('contracts_type_id', '!=', '4')
                    ->whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->orderBy('id', 'DESC')
                    ->paginate(100);

            } else {
                $contracts = Contract::where('stav_zakazky', $request->stav_zakazky)
                    ->where('contracts_type_id', '!=', '4')
                    ->where('client_id', $request->klient)->whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->orderBy('id', 'DESC')
                    ->paginate(100);
            }
        }
        // Odpracovane hodiny za dnesny den

        $today = date("Y-m-d");
        $employyes = Employees::all();

        foreach ($employyes as $employy)
        {
            $trvanie_hod= Task::whereDate('datum', $today)->where('employy_id', '=', $employy->id)->sum('trvanie_hod');
            $trvanie_min = Task::whereDate('datum', $today)->where('employy_id', '=', $employy->id)->sum('trvanie_min');
            $meno = $employy->meno;
            if ($trvanie_min > 59) {
                $pom_hod = floor($trvanie_min / 60);
                $trvanie_hod = $trvanie_hod + $pom_hod;
                $pom_min = $pom_hod * 60;
                $trvanie_min = $trvanie_min - $pom_min;
            }
            $zamestnanec[] = ['meno' => $meno, 'hod' => $trvanie_hod, 'min' => $trvanie_min];
        }
        //


        return view('contracts.index',['contracts'=>$contracts,'contractstatus'=>$contractstatus,'value_stav_zakazky'=>$value_stav_zakazky,'zamestnanci'=>$zamestnanec,'fromdatum'=>$from,'todatum'=>$to,'clients'=>$clients,'value_klient'=>$value_klient]);

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)) {

     // Vypocet dalsieho poradovaho cisla
           $posledne_poradove_cislo = Contract::orderBy('id', 'desc')->first()->poradove_cislo;
           // $posledne_poradove_cislo = null;
            $expNum = explode('/', $posledne_poradove_cislo);
            $date = date('y');

            if ( ! isset($expNum[1])) {
                $expNum[1] = null;
            }

            if (($expNum != null)&&($expNum[1]!= null)) {
                // Overenie roku
                if (($expNum[1]) == $date) {
                    $nextContractNumber = ($expNum[0] + 1) . '/' . $date;

                } else if (($expNum[1]) < $date) {
                    $expNum[0] = 0;
                    $nextContractNumber = ($expNum[0] + 1) . '/' . $date;
                } else {
                    $nextContractNumber = ($expNum[0] + 1) . '/' . $date;
                }
            } else
            {

                $nextContractNumber = 1 . '/' . $date;
            }
            // Koniec vypoctu
            $products = Stock::all()->except(1);
            $employy= Employees::all();
            $client=Client::all();
            $contractstatus=ContractsStatus::all();
            $contractstypes=ContractsType::all()->except(4);
            return view('contracts.create',['client'=>$client, 'employy'=>$employy, 'contractstatus'=>$contractstatus,'contractstypes'=>$contractstypes, 'nextContractNumber'=>$nextContractNumber,'products'=>$products]);
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()){
            $contract = Contract::create([
                'contracts_type_id' => $request->input('contractstype'),
                'product_id' => $request->input('product_id'),
                'popis' => $request->input('popis'),
                'client_id' => $request->input('client_id'),
                'pocet_ks' => $request->input('pocet_ks'),
                'podiel_zodp_osoba_perc' => $request->input('podiel_zodp_osoba_perc'),
                'poradove_cislo' => $request->input('poradove_cislo'),
                'cislo_objednavky' => $request->input('cislo_objednavky'),
                'cislo_faktury' => $request->input('cislo_faktury'),
                'zodpovedna_osoba_id' => $request->input('zodpovedna_osoba_id'),
                'odhadovany_cas' => $request->input('odhadovany_cas'),
                'datumodovzdania' => $request->input('datumodovzdania'),
                'stav_zakazky' => $request->input('stav_zakazky'),
                'hodnota_zakazky_za_ks' => $request->input('hodnota_zakazky_za_ks'),
                'nadradena_id' => $request->input('nadradena_id'),
                'user_id' => Auth::user()->id
            ]);
            if($contract){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('contract')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril zákazku ID: '.$contract->id.' s parametrami: Poradové číslo: '.$contract->poradove_cislo.' Contracts_type_id: '.$contract->contracts_type_id.' Product_id: '.$contract->product_id.' Popis: '.$contract->popis.' Počet ks: '.$contract->pocet_ks.' Podiel_zodp_osoba_perc: '.$contract->podiel_zodp_osoba_perc.' Číslo objednávky: '.$contract->cislo_objednavky.' Číslo faktúry: '.$contract->cislo_faktury.' Zodpovedná osoba id: '.$contract->zodpovedna_osoba_id.' Odhadovaný čas: '.$contract->odhadovany_cas.' Dátum odovzdania: '.$contract->datumodovzdania.' Stav zákazky: '.$contract->stav_zakazky.' Hodnota zákazky za ks: '.$contract->hodnota_zakazky_za_ks.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('contracts.show', ['contract'=> $contract->id])
                    ->with('success' , 'Company created successfully');
            }
        }

        return back()->withInput()->with('errors', 'Error creating new company');
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        $now = Carbon::now()->format('Y-m-d');
        // $contract = Contract::where('id',$contract->id);
        $contract= Contract::find($contract->id); // Najde id konktetnej zakazky

        $contractstatus = ContractsStatus::where('id',$contract->stav_zakazky)->get();
        $client=Client::where('id',$contract->client_id)->get();
        $zodpovedna_osoba= Employees::where('id',$contract->zodpovedna_osoba_id)->get();

        // Standartne, rezijne, podradene zakazky
        if ($contract->contracts_type_id != 3)
        {

        $defined_task= DefinedTask::all();
        $employy= Employees::all();
        $subdefined_task= SubdefinedTask::all();
        $download_files= DownloadFile::where('contract_id',$contract->id)->get(); //nacita zoznam suborov priradenych ku konkretnej zakazke
        $contractstatistics = ContractStatistic::where('contract_id',$contract->id)->get();
        return view('contracts.show',['contract'=>$contract, 'defined_task'=>$defined_task, 'employy'=>$employy, 'subdefined_task'=>$subdefined_task, 'download_files'=>$download_files,'contractstatus'=>$contractstatus,'client'=>$client,'zodpovedna_osoba'=>$zodpovedna_osoba,'contractstatistics'=>$contractstatistics,'now'=>$now]);
        }
        // Nadaradene zakazky
        else
        {
            // pocitadlo podzakazok

           $posledne_poradove_cislo = Contract::where('nadradena_id',$contract->id)->orderBy('id', 'desc')->first();


            if ($posledne_poradove_cislo == null)
            {
                $nextnumber = ($contract->poradove_cislo.'-1');
            }
            else
                {
                    $expNum = explode('-', $posledne_poradove_cislo->poradove_cislo);
                    $nextnumber = ($expNum[0].'-'.($expNum[1]+1));
                }

            // koniec poradoveho cisla

           $employyes= Employees::all();
           $podradene_zakazky = Contract::where('nadradena_id',$contract->id)->orderBy('id', 'desc')->get();

           // Statistiky nadradenej zakazky
            $celkova_predajna_cena_ks = $contract->hodnota_zakazky_za_ks;
            $pocet_ks = $contract->pocet_ks;
            $celkova_predajna_cena =($contract->hodnota_zakazky_za_ks)*$pocet_ks;
           // $celkova_predajna_cena_zakazok = Contract::where('nadradena_id',$contract->id)->sum('hodnota_zakazky_za_ks'); // vsetkych nielen neaktivnych
            $celkova_predajna_cena_zakazok = 0;
            $celkova_vyrobna_cena=0;
            $hodnota_zakazky_ks=0;
            $podiel_zodp_osoba_perc=$contract->podiel_zodp_osoba_perc;
            $podiel_zodp_osoba_hodnota=0;
            $zisk = 0;
            $zisk_firmu=0;
            $zisk_zamestnanci=0;
            $podiel_zamestnanci=0;
            $celkova_cena_cinnost=0;
            $celkova_cena_material=0;


            foreach ($podradene_zakazky as $podradena_zakazka )
            {
                // vypocet predajnej ceny podradenych zakazkach
                $pocetks = $podradena_zakazka->pocet_ks;
                $predajcena = $podradena_zakazka->hodnota_zakazky_za_ks;
                $celkova_predajna_cena_zakazok += $predajcena* $pocetks; // vsetkych nielen neaktivnych

                //

                $zakazka= ContractStatistic::where('contract_id',$podradena_zakazka->id)->first();

                if ($zakazka)
                {

                    $celkova_vyrobna_cena += $zakazka->celkova_vyrobna_cena;
                    $hodnota_zakazky_ks += $zakazka->hodnota_zakazky_ks;
                    $podiel_zodp_osoba_hodnota += $zakazka->podiel_zodp_osoba_hodnota;
                    $zisk += $zakazka->zisk;
                    $zisk_firmu += $zakazka->zisk_firmu;
                    $zisk_zamestnanci += $zakazka->zisk_zamestnanci;
                    $podiel_zamestnanci += $zakazka->podiel_zamestnanci;
                    $celkova_cena_cinnost += $zakazka->celkova_cena_cinnost;
                    $celkova_cena_material += $zakazka->celkova_cena_material;
                }

            }
            $rozdiel_pred_cien = $celkova_predajna_cena - $celkova_predajna_cena_zakazok;

           // Koniec statistik nadradenej zakazky


            return view('contracts.show',['nextnumber'=>$nextnumber,'contract'=>$contract,'podradene_zakazky'=>$podradene_zakazky,'employyes'=>$employyes, 'contractstatus'=>$contractstatus,'client'=>$client,'zodpovedna_osoba'=>$zodpovedna_osoba,'now'=>$now,'celkova_predajna_cena_ks'=>$celkova_predajna_cena_ks,'pocet_ks'=>$pocet_ks,'celkova_predajna_cena'=>$celkova_predajna_cena,'celkova_vyrobna_cena'=>$celkova_vyrobna_cena,'hodnota_zakazky_ks'=>$hodnota_zakazky_ks,'podiel_zodp_osoba_perc'=>$podiel_zodp_osoba_perc,'podiel_zodp_osoba_hodnota'=>$podiel_zodp_osoba_hodnota,'zisk'=>$zisk,'zisk_firmu'=>$zisk_firmu,'zisk_zamestnanci'=>$zisk_zamestnanci,'podiel_zamestnanci'=> $podiel_zamestnanci,'celkova_cena_cinnost'=>$celkova_cena_cinnost,'celkova_cena_material'=>$celkova_cena_material,'celkova_predajna_cena_zakazok'=>$celkova_predajna_cena_zakazok,'rozdiel_pred_cien'=>$rozdiel_pred_cien]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit(Contract $contract)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)) {

            $contractstatus=ContractsStatus::all();
            $contractstypes=ContractsType::all()->except(4);
            $employy= Employees::all();
            $client=Client::all();
            $contract = Contract::find($contract->id);
            $products = Stock::all()->except(1);
            $typ_zakazky =$contract->contracts_type_id;

            //overenie editacie na zmenu typu zakazky zo standarna na rezijna a opacne
            $overenie = 0;
            $id_zakazky=$contract->id;
            $stav_zakazky = $contract->stav_zakazky;

            if ($typ_zakazky != 3) // Ak je zakazka nadradena nekontroluj
            {
                $task = Task::where('contract_id',$id_zakazky)->count();

                if (($task == 0)&&($stav_zakazky  == 1))
                {
                    $overenie = 1;

                }
                else
                {
                    $overenie = 0;
                }

            }
            else
            {
                $overenie = 0;
            }




            //
            // overenie skladu
            $nasklade = SendStock::where('contract_id',$contract->id)->count();

           if ($nasklade == 0)
           {
              $overenie_skladu = true;
           }
           else
           {
               $overenie_skladu = false;
           }
            //

            // overenie ci je mozne ukoncit zakazku kvoli rozdielu predajnych cien

           if ($typ_zakazky == 4) // A je zakazka podradena
               {
                   $id_nadradenej = $contract->nadradena_id;
                 //  $sum_predaj_podradene_zakazky = Contract::where('nadradena_id',$id_nadradenej)->sum('hodnota_zakazky_za_ks');
                   $sum_predaj_hodnota = Contract::where('id',$id_nadradenej)->sum('hodnota_zakazky_za_ks');
                   $sum_predaj_ks = Contract::where('id',$id_nadradenej)->sum('pocet_ks');

                   $sum_predaj =$sum_predaj_hodnota *$sum_predaj_ks  ; // ok

                   //
                   // Hodnota skladu
                   $predaj_podradene_zakazky =0;
                   $podradene = Contract::where('nadradena_id',$id_nadradenej)->get();
                   foreach ($podradene as $podradena)
                   {
                       $predaj_podradene_zakazky +=  $podradena->pocet_ks * $podradena->hodnota_zakazky_za_ks;
                   }
                   //



                   if($sum_predaj == $predaj_podradene_zakazky )
                   {
                       $overenie_ukoncenia = true;

                   }
                   else
                   {
                       $overenie_ukoncenia = false ;
                   }
               }
           else
           {
               $overenie_ukoncenia = true;
           }


            // Koniec overenia ukoncenia


            return view('contracts.edit',['contract'=>$contract,'client'=>$client, 'employy'=>$employy,'contractstatus'=>$contractstatus,'contractstypes'=>$contractstypes,'overenie'=>$overenie,'products' =>$products,'overenie_skladu' =>$overenie_skladu,'overenie_ukoncenia' =>$overenie_ukoncenia]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Contract $contract)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)) {
            $stav_zakazky =$request->input('stav_zakazky');
            $typ_zakazky =  $request->input('contractstype');
            // Ak je stav zakazky = 1 (aktivna) tak iba aktualizuje konktetnu zakazku
            // Ak je typ zakazky = 3 (cize ide o nadradenu zakazku tak netreba aktualizovat tasky ani posielat do statistik)
            if (($stav_zakazky==1)||($typ_zakazky == 3))
            {
            $contractUpdate = Contract::where('id',$contract->id)->update([
                'product_id' => $request->input('product_id'),
                'popis' => $request->input('popis'),
                'contracts_type_id' => $request->input('contractstype'),
                'client_id' => $request->input('client_id'),
                'pocet_ks' => $request->input('pocet_ks'),
                'podiel_zodp_osoba_perc' => $request->input('podiel_zodp_osoba_perc'),
                'poradove_cislo' => $request->input('poradove_cislo'),
                'cislo_objednavky' => $request->input('cislo_objednavky'),
                'cislo_faktury' => $request->input('cislo_faktury'),
                'zodpovedna_osoba_id' => $request->input('zodpovedna_osoba_id'),
                'odhadovany_cas' => $request->input('odhadovany_cas'),
                'datumodovzdania' => $request->input('datumodovzdania'),
                'stav_zakazky' => $request->input('stav_zakazky'),
                'hodnota_zakazky_za_ks' => $request->input('hodnota_zakazky_za_ks')
            ]);

                if ($contractUpdate){
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('contract')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval zákazku ID: '.$contract->id.' s parametrami: Poradové číslo: '.$request->input('poradove_cislo').' Contracts_type_id: '.$request->input('contractstype').' Product_id: '.$request->input('product_id').' Popis: '.$request->input('popis').' Klient id: '.$request->input('client_id').' Počet ks: '.$request->input('pocet_ks').' Podiel_zodp_osoba_perc: '.$request->input('podiel_zodp_osoba_perc').' Číslo objednávky: '.$request->input('cislo_objednavky').' Číslo faktúry: '.$request->input('cislo_faktury').' Zodpovedná osoba id: '.$request->input('zodpovedna_osoba_id').' Odhadovaný čas: '.$request->input('odhadovany_cas').' Dátum odovzdania: '.$request->input('datumodovzdania').' Stav zákazky: '.$request->input('stav_zakazky').' Hodnota zákazky za ks: '.$request->input('hodnota_zakazky_za_ks').' z IP: '.$ip_adresa.' v čase: '.$cas);
                    return redirect()->route('contracts.show',['contract'=>$contract->id])
                        ->with('success','Company update successfully');

                }
                return back()->withInput();
            }
            // Ak stav zakazky != 1 (cize je neaktivna) tak:
            // Aktualizuje konkretnu zakazku
            // Vytvori alebo aktualizuje statistiku zakazky
            // Urobi update taskov, kde doplni hodnoty
            // Ak je typ zakazky !=3 cize ide o standarnu,rezijna, podradenu zakazku tak vykonaj
            else if ($typ_zakazky != 3)
            {
                $contractUpdate = Contract::where('id', $contract->id)->update([
                    'product_id' => $request->input('product_id'),
                    'popis' => $request->input('popis'),
                    'client_id' => $request->input('client_id'),
                    'pocet_ks' => $request->input('pocet_ks'),
                    'podiel_zodp_osoba_perc' => $request->input('podiel_zodp_osoba_perc'),
                    'poradove_cislo' => $request->input('poradove_cislo'),
                    'cislo_objednavky' => $request->input('cislo_objednavky'),
                    'cislo_faktury' => $request->input('cislo_faktury'),
                    'zodpovedna_osoba_id' => $request->input('zodpovedna_osoba_id'),
                    'odhadovany_cas' => $request->input('odhadovany_cas'),
                    'datumodovzdania' => $request->input('datumodovzdania'),
                    'stav_zakazky' => $request->input('stav_zakazky'),
                    'hodnota_zakazky_za_ks' =>$request->input('hodnota_zakazky_za_ks')
                ]);

                //Nacita tasky konkretnej zakazky
                $tasks = Task::where('contract_id', $contract->id)->get();

                $celkova_vyrobna_cena = 0;
                $celkova_cena_cinnost = 0;
                $celkova_cena_material = 0;
                $celkove_hod = 0;
                $celkove_min = 0;
                $pocet_ks = $request->input('pocet_ks');
                $celkova_predajna_cena_ks = str_replace(",", ".", $request->input('hodnota_zakazky_za_ks'));
                $celkova_predajna_cena = $celkova_predajna_cena_ks * $pocet_ks;


                foreach ($tasks as $task) {
                    $celkova_vyrobna_cena = $celkova_vyrobna_cena + $task->celkova_cena;
                    $celkova_cena_cinnost = $celkova_cena_cinnost + $task->celkova_cena_cinnost;
                    $celkova_cena_material = $celkova_cena_material + $task->celkova_cena_material;
                    $celkove_hod = $celkove_hod + $task->trvanie_hod;
                    $celkove_min = $celkove_min + $task->trvanie_min;
                }
                $celkovy_cas = ($celkove_hod * 60) + $celkove_min;
                $celkova_vyrobna_cena_ks = $celkova_vyrobna_cena / $pocet_ks;


                $zisk_pred_rozdelenim_zodp = $celkova_predajna_cena - $celkova_vyrobna_cena;
                // Zisk pre zodpovednu osobu
                $podiel_pre_zodpovednu_osobu_perc =$request->input('podiel_zodp_osoba_perc');
                $podiel_pre_zodpovednu_osobu_hodnota = ($zisk_pred_rozdelenim_zodp * ($podiel_pre_zodpovednu_osobu_perc/100));
                //
                $zisk = $zisk_pred_rozdelenim_zodp -$podiel_pre_zodpovednu_osobu_hodnota;

                $zisk_firma = $zisk * 0.67;
                $zisk_zamestnanci = $zisk - $zisk_firma;
                $podiel_zamestnanci = $zisk_zamestnanci / $celkovy_cas;

                // Najdenie konktretnej zakazky
                $contract2 = Contract::find($contract->id);
                //

                $contractstatistic_update = ContractStatistic::where('contract_id', $contract->id)->update([
                    'contract_id' => $contract->id, //ok
                    'contracts_type_id' => $contract2->contracts_type_id, //ok
                    'client_id' => $contract2->client_id, //ok
                    'zodpovedna_osoba_id' => $contract2->zodpovedna_osoba_id, //ok
                    'pocet_ks' => $pocet_ks, //ok
                    'hodnota_zakazky_ks' => number_format($celkova_vyrobna_cena_ks, 2, '.', ''), //ok
                    'celkovy_cas' => $celkovy_cas, //ok

                    'podiel_zodp_osoba_perc' => $podiel_pre_zodpovednu_osobu_perc,
                    'podiel_zodp_osoba_hodnota' => $podiel_pre_zodpovednu_osobu_hodnota,

                    'celkova_cena_cinnost' => number_format($celkova_cena_cinnost, 2, '.', ''), //ok
                    'celkova_cena_material' => number_format($celkova_cena_material, 2, '.', ''), //ok
                    'celkova_vyrobna_cena' => number_format($celkova_vyrobna_cena, 2, '.', ''), //ok
                    'celkova_predajna_cena_ks' => number_format($celkova_predajna_cena_ks, 2, '.', ''), //ok
                    'celkova_predajna_cena' => number_format($celkova_predajna_cena, 2, '.', ''), //ok
                    'zisk' => number_format($zisk, 2, '.', ''), //ok
                    'zisk_firmu' => number_format($zisk_firma, 2, '.', ''),
                    'zisk_zamestnanci' => number_format($zisk_zamestnanci, 2, '.', ''),
                    'podiel_zamestnanci' => number_format($podiel_zamestnanci, 2, '.', '')

                ]);



                if ($contractstatistic_update) {
                    // Zalogovanie
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('contractstatistics')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval ContractStatistic, kde contract id: '.$contract->id.' contracts_type_id: '.$contract2->contracts_type_id.' client_id: '.$contract2->client_id.' zodpovedna_osoba_id: '.$contract2->zodpovedna_osoba_id.' podiel_zodp_osoba_perc: '.$podiel_pre_zodpovednu_osobu_perc.' podiel_zodp_osoba_hodnota: '.$podiel_pre_zodpovednu_osobu_hodnota.' pocet_ks: '.$pocet_ks.' hodnota_zakazky_ks: '.number_format($celkova_vyrobna_cena_ks, 2, '.', '').' celkovy_cas: '.$celkovy_cas.' celkova_cena_cinnost: '.number_format($celkova_cena_cinnost, 2, '.', '').' celkova_cena_material: '.number_format($celkova_cena_material, 2, '.', '').' celkova_vyrobna_cena: '.number_format($celkova_vyrobna_cena, 2, '.', '').' celkova_predajna_cena_ks: '.number_format($celkova_predajna_cena_ks, 2, '.', '').' celkova_predajna_cena: '.number_format($celkova_predajna_cena, 2, '.', '').' zisk: '.number_format($zisk, 2, '.', '').' zisk_firmu: '.number_format($zisk_firma, 2, '.', '').' zisk_zamestnanci: '.number_format($zisk_zamestnanci, 2, '.', '').' podiel_zamestnanci: '.number_format($podiel_zamestnanci, 2, '.', '').'  z: '.$ip_adresa.' v čase: '.$cas);
                    //

                } // Update
                else {
                    $contractstatistic_create = ContractStatistic::create([
                        'contract_id' => $contract->id, //ok
                        'contracts_type_id' => $contract->contracts_type_id, //ok
                        'client_id' => $contract->client_id, //ok
                        'zodpovedna_osoba_id' => $contract->zodpovedna_osoba_id, //ok
                        'podiel_zodp_osoba_perc' => $podiel_pre_zodpovednu_osobu_perc,
                        'podiel_zodp_osoba_hodnota' => $podiel_pre_zodpovednu_osobu_hodnota,
                        'pocet_ks' => $pocet_ks, //ok
                        'hodnota_zakazky_ks' => number_format($celkova_vyrobna_cena_ks, 2, '.', ''), //ok
                        'celkovy_cas' => $celkovy_cas, //ok
                        'celkova_cena_cinnost' => number_format($celkova_cena_cinnost, 2, '.', ''), //ok
                        'celkova_cena_material' => number_format($celkova_cena_material, 2, '.', ''), //ok
                        'celkova_vyrobna_cena' => number_format($celkova_vyrobna_cena, 2, '.', ''), //ok
                        'celkova_predajna_cena_ks' => number_format($celkova_predajna_cena_ks, 2, '.', ''), //ok
                        'celkova_predajna_cena' => number_format($celkova_predajna_cena, 2, '.', ''), //ok
                        'zisk' => number_format($zisk, 2, '.', ''), //ok
                        'zisk_firmu' => number_format($zisk_firma, 2, '.', ''),
                        'zisk_zamestnanci' => number_format($zisk_zamestnanci, 2, '.', ''),
                        'podiel_zamestnanci' => number_format($podiel_zamestnanci, 2, '.', '')

                    ]);
                    // Zalogovanie
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('contractstatistics')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril ContractStatistic ID: '.$contractstatistic_create->id.' , kde contract id: '.$contract->id.' contracts_type_id: '.$contractstatistic_create->contracts_type_id.' client_id: '.$contractstatistic_create->client_id.' zodpovedna_osoba_id: '.$contractstatistic_create->zodpovedna_osoba_id.' podiel_zodp_osoba_perc: '.$contractstatistic_create->podiel_zodp_osoba_perc.' podiel_zodp_osoba_hodnota: '.$contractstatistic_create->podiel_zodp_osoba_hodnota.' pocet_ks: '.$contractstatistic_create->pocet_ks.' hodnota_zakazky_ks: '.$contractstatistic_create->hodnota_zakazky_ks.' celkovy_cas: '.$contractstatistic_create->celkovy_cas.' celkova_cena_cinnost: '.$contractstatistic_create->celkova_cena_cinnost.' celkova_cena_material: '.$contractstatistic_create->celkova_cena_material.' celkova_vyrobna_cena: '.$contractstatistic_create->celkova_vyrobna_cena.' celkova_predajna_cena_ks: '.$contractstatistic_create->celkova_predajna_cena_ks.' celkova_predajna_cena: '.$contractstatistic_create->celkova_predajna_cena.' zisk: '.$contractstatistic_create->zisk.''.$contractstatistic_create->id.' zisk_firmu: '.$contractstatistic_create->zisk_firmu.' zisk_zamestnanci: '.$contractstatistic_create->zisk_zamestnanci.' podiel_zamestnanci: '.$contractstatistic_create->podiel_zamestnanci.'  z: '.$ip_adresa.' v čase: '.$cas);
                    //
                }


                //Update TASKOV
                $stav_tasku = 2;

                    foreach ($tasks as $task) {
                        $trvanie_hod = $task->trvanie_hod;
                        $trvanie_min = $task->trvanie_min;

                        $cas = ($trvanie_hod * 60) + $trvanie_min;
                        $podiel_pre_zamestnanca = $cas * $podiel_zamestnanci;

                        $updated = $task->update([

                            'hodnota_tasku' => $podiel_pre_zamestnanca,
                            'stav' => $stav_tasku
                        ]);
                    }


                if ($contractUpdate&&$updated){
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('contract')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval zákazku ID: '.$contract->id.' s parametrami: Poradové číslo: '.$request->input('poradove_cislo').' Contracts_type_id: '.$request->input('contractstype').' Product_id: '.$request->input('product_id').' Popis: '.$request->input('popis').' Klient id: '.$request->input('client_id').' Počet ks: '.$request->input('pocet_ks').' Podiel_zodp_osoba_perc: '.$request->input('podiel_zodp_osoba_perc').' Číslo objednávky: '.$request->input('cislo_objednavky').' Číslo faktúry: '.$request->input('cislo_faktury').' Zodpovedná osoba id: '.$request->input('zodpovedna_osoba_id').' Odhadovaný čas: '.$request->input('odhadovany_cas').' Dátum odovzdania: '.$request->input('datumodovzdania').' Stav zákazky: '.$request->input('stav_zakazky').' Hodnota zákazky za ks: '.$request->input('hodnota_zakazky_za_ks').' z IP: '.$ip_adresa.' v čase: '.$cas);
                    return redirect()->route('contracts.show',['contract'=>$contract->id])
                        ->with('success','Company update successfully');

                }

                return back()->withInput();
            }
            else
            {
                return back()->withInput();
            }



        }
        return back();
    }

    public function generatePDF(Request $request,$id)
    {
        // https://github.com/barryvdh/laravel-dompdf
        

        $contract= Contract::find($id); // Najde id konktetnej zakazky
        $contractstatus = ContractsStatus::where('id',$contract->stav_zakazky)->get();
        $client=Client::where('id',$contract->client_id)->get();
        $zodpovedna_osoba= Employees::where('id',$contract->zodpovedna_osoba_id)->get();
        $defined_task= DefinedTask::all();
        $employy= Employees::all();
        $subdefined_task= SubdefinedTask::all();
        $download_files= DownloadFile::where('contract_id',$contract->id)->get(); //nacita zoznam suborov priradenych ku konkretnej zakazke
        $contractstatistics = ContractStatistic::where('contract_id',$contract->id)->get();

        $pdf = PDF::loadView('contracts.showprint',['contract'=>$contract, 'defined_task'=>$defined_task, 'employy'=>$employy, 'subdefined_task'=>$subdefined_task, 'download_files'=>$download_files,'contractstatus'=>$contractstatus,'client'=>$client,'zodpovedna_osoba'=>$zodpovedna_osoba,'contractstatistics'=>$contractstatistics]);
        $pdf->setPaper('A4', 'landscape');

        return $pdf->stream('result.pdf', array('Attachment'=>0));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */

    public function destroy(Contract $contract)
    {
        //
    }
}
