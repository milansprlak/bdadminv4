<?php

namespace App\Http\Controllers;

use App\Diary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class DiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_8)==1)){
            $fromdatum =$request->fromdatum;
            $todatum =$request->todatum;

            if (($request->fromdatum == null) || ($request->todatum == null)) {
                $fromdatum = date("Y-m-d", strtotime('-1 year'));
                $todatum = date("Y-m-d");
            }


            $diaries= Diary::whereDate('created_at', '>=', $fromdatum)->whereDate('created_at', '<=', $todatum)->orderBy('id', 'DESC')->paginate(100);
            return view('diaries.index',['diaries'=>$diaries,'fromdatum'=>$fromdatum,'todatum'=>$todatum]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_8)==1)){
            return view('diaries.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_8)==1)){
            $diary = Diary::create([
                'poznamka' => $request->input('poznamka'),
                'user_id' => Auth::id(),


            ]);
            if($diary){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('diary')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril zapis do dennika s ID: '.$diary->id.' Poznámka: '.$diary->poznamka.' z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('diary.index');
            }
        }

        return back()->withInput()->with('errors', 'Vyskytla sa chyba.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Diary  $diary
     * @return \Illuminate\Http\Response
     */
    public function show(Diary $diary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Diary  $diary
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_8)==1)){
        $finddiary = Diary::find($id);
        if(($finddiary->created_at->isToday())&&(Auth::check())){

            return view('diaries.edit',['diaries'=>$finddiary]);
        }
        return back();
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Diary  $diary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Diary $diary)
    {
        if((Auth::check())&&((Auth::user()->mod_8)==1)){
            $updated = $diary->update([
                'poznamka' => $request->input('poznamka'),
                'user_id' => Auth::id()
            ]);
            if (! $updated){
                return back()->withInput();
            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('diary')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval zapis dennika s ID: '.$diary->id.' Poznámka: '.$diary->poznamka.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return redirect()->route('diary.index')
                ->with('success','Defined task update successfully');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Diary  $diary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diary $diary)
    {
        //
    }
}
