<?php

namespace App\Http\Controllers;

use App\ClientExport;
use App\Client;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Log;
use Auth;
use Hash;

class ClientExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_11)==1)){
            $users = ClientExport::all();
            return view('client_export.index',['users'=>$users]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_11)==1)){
            $clients=Client::all();
            return view('client_export.create',['clients'=>$clients]);
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_11)==1)){

            $test = $request->validate([
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:client_exports',
                'password' => 'required|string|min:6|confirmed',
            ]);
            if ($test) {
                $user = ClientExport::create([
                    'username' => $request->input('username'),
                    'email' => $request->input('email'),
                    'password' => md5($request->input('password')),
                    'client_id' => $request->input('client_id'),


                ]);
                if ($user) {
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('client_export')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril noveho uzivatela s ID: '.$user->id. ' Meno = '.$user->username.' E-MAIL = '.$user->email.' S klientom id: '.$user->client_id.' z IP: '.$ip_adresa.' v čase: '.$cas);

                    return redirect()->route('client_export.index')->with('success_message', 'Uživateľ bol úspešne vytvorený');
                }
            }
            return back()->withInput()->with('danger_message', 'Nespravne udaje.');


        }
        return back()->withInput()->with('danger_message', 'Vytvorenie uživateľa skončilo chybou.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClientExport  $clientExport
     * @return \Illuminate\Http\Response
     */
    public function show(ClientExport $clientExport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClientExport  $clientExport
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_11)==1)){

            $user= ClientExport::find($id);
            $clients=Client::all();
            return view('client_export.edit',['user'=>$user,'clients'=>$clients]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientExport  $clientExport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientExport $clientExport)
    {
        if((Auth::check())&&((Auth::user()->mod_11)==1)) {
            if (($request->input('password')) == null) {
                $updated = $clientExport->update([
                    'username' => $request->input('username'),
                    'email' => $request->input('email'),
                    'client_id' => $request->input('client_id'),

                ]);
                if ($updated) {
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('client_export')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval uzivatela s ID: '.$clientExport->id. ' Meno = '.$clientExport->username.' E-MAIL = '.$clientExport->email.' S klientom: '.$clientExport->client_id.' z IP: '.$ip_adresa.' v čase: '.$cas);

                    return redirect()->route('client_export.index')->with('success_message', 'U6ivateľ bol úspešne, bez zmeny hesla');
                }
            } else if (($request->input('password'))==($request->input('password_confirmation')))
            {
                $updated = $clientExport->update([
                    'username' => $request->input('username'),
                    'email' => $request->input('email'),
                    'client_id' => $request->input('client_id'),
                    'password' => md5($request->input('password')),

                ]);
                if ($updated) {
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('client_export')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval uzivatela s ID: '.$clientExport->id. ' a zmenil heslo Meno = '.$clientExport->username.' E-MAIL = '.$clientExport->email.' S klientom: '.$clientExport->client_id.' z IP: '.$ip_adresa.' v čase: '.$cas);

                    return redirect()->route('client_export.index')->with('success_message', 'U6ivateľ bol úspešne, so zmenou hesla');
                }
            }
            else if  (($request->input('password'))!=($request->input('password_confirmation')))
            {
                return back()->withInput()->with('danger_message', 'Nastala chyba: heslá sa nezhoduju!');
            }
            else
            {
                return back()->withInput()->with('danger_message', 'Nastala chyba!');
            }

        }

        return back()->withInput()->with('danger_message', 'Nastala chyba!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClientExport  $clientExport
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientExport $clientExport)
    {
        //
    }
}
