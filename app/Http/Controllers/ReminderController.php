<?php

namespace App\Http\Controllers;

use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use View;
use Log;



class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_7)==1)){
            $today = Carbon::now()->toDateString();
            $opakujuce_pripomienky=null;
            $neopakujuce_pripomienky=null;


            // Neopakujuce sa pripomienky
            $reminders = Reminder::where('stav', '1')->where('opakovanie_val','=','0')
                ->whereDate('datum', '>=', $today )->get();

            foreach ($reminders as $reminder) {
                $pripomienka_val = $reminder->pripomenutie_val;
                $pripomienka_num = $reminder->pripomenutie_num;
                //PRIPOMIENKA 1: dni; 2: tyzdne; 3:mesiace
                switch ($pripomienka_val) {
                    case 1:
                        $pripomienka_dni = $pripomienka_num;
                        break;
                    case 2:
                        $pripomienka_dni = $pripomienka_num * 7;
                        break;
                    case 3:
                        $pripomienka_dni = $pripomienka_num * 30;
                        break;
                }

              //  $date =date("Y-m-d", strtotime("$reminder->datum - $pripomienka_dni day"));
                $date_start= Carbon::parse($reminder->datum);
                $date = $date_start->subDays($pripomienka_dni);
                $datum = $reminder->datum;


                if ($date <= $today) {
                    //$neopakujuce_pripomienky[] = $reminder;
                    $neopakujuce_pripomienky[] = ['id'=>$reminder->id,'pripomienka'=>$reminder->pripomienka,'datum'=>$datum,'datumod'=>$date,'opakovanie_val'=>$reminder->opakovanie_val,'opakovanie_num'=>$reminder->opakovanie_num,'pripomenutie_num'=>$reminder->pripomenutie_num,'pripomenutie_val'=>$reminder->pripomenutie_val,'stav'=>$reminder->stav,'created_at'=>$reminder->created_at];

                }
            }
            // Koniec neopakujucich pripomienok
            // Začiatok opakovacích
            $reminders_opakujuce = Reminder::where('stav', '1')->where('opakovanie_val','!=','0')->get();

            foreach ($reminders_opakujuce as $reminder_opak)
            {
                $opkaovanie_val = $reminder_opak->opakovanie_val;
                $opakovanie_num = $reminder_opak->opakovanie_num;

                $pripomienka_val = $reminder_opak->pripomenutie_val;
                $pripomienka_num = $reminder_opak->pripomenutie_num;


                $start= Carbon::parse($reminder_opak->datum);
                $datenew = Carbon::parse($reminder_opak->datum);
                $end = Carbon::now()->toDateString();

                //OPAKOVANIE 1: dni; 2: tyzdne; 3: mesiace; 4: roky
                // https://carbon.nesbot.com/docs/
                switch ($opkaovanie_val) {
                    case 1: // Pridať dni
                        while($start < $end){

                            $start->addDays($opakovanie_num);
                            $datenew->addDays($opakovanie_num);
                        }

                        break;
                    case 2: // Pridať týždne

                        while($start < $end){

                            $start->addWeeks($opakovanie_num);
                            $datenew->addWeeks($opakovanie_num);
                        }
                        break;
                    case 3: // Pridať mesiace
                         while($start < $end){

                            $start->addMonths($opakovanie_num);
                            $datenew->addMonths($opakovanie_num);
                        }

                        break;
                    case 4: // Pridať roky
                        while($start < $end){

                            $start->addYears($opakovanie_num);
                            $datenew->addYears($opakovanie_num);
                            }
                        break;
                }

                // Pripomienka switch
                switch ($pripomienka_val) {
                    case 1:
                       $pripomienka_dni = $pripomienka_num;

                        break;
                    case 2:
                      $pripomienka_dni = $pripomienka_num * 7;

                        break;
                    case 3:
                       $pripomienka_dni = $pripomienka_num * 30;
                        break;
                                         }

                        $date = $start->subDays($pripomienka_dni);
                        $datum = $reminder_opak->datum;

                if ($date <= $today) {
                    $opakujuce_pripomienky[] = ['id'=>$reminder_opak->id,'pripomienka'=>$reminder_opak->pripomienka,'datum'=>$datum,'datumod'=>$date,'datumnew' => $datenew,'opakovanie_val'=>$reminder_opak->opakovanie_val,'opakovanie_num'=>$reminder_opak->opakovanie_num,'pripomenutie_num'=>$reminder_opak->pripomenutie_num,'pripomenutie_val'=>$reminder_opak->pripomenutie_val,'stav'=>$reminder_opak->stav,'created_at'=>$reminder_opak->created_at];
               }

            }
            // Koniec opakovacích pripomienok

            return view('reminders.index',['reminders'=>$neopakujuce_pripomienky, 'reminders_rep'=>$opakujuce_pripomienky]);
        }
        return back();
    }

    public function all()
    {
        if((Auth::check())&&((Auth::user()->mod_7)==1)){
            $reminders = Reminder::orderBy('id', 'DESC')->paginate(100);
            return view('reminders.all',['reminders'=>$reminders]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
            return view('reminders.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_7)==1)){
            $reminder = Reminder::create([
                'pripomienka' => $request->input('pripomienka'),
                'datum' => $request->input('terminpripomienky'),
                'opakovanie_val' => $request->input('opakovanie_val'),
                'opakovanie_num' => $request->input('opakovanie_num'),
                'pripomenutie_num' => $request->input('upozornenie_num'),
                'pripomenutie_val' => $request->input('upozornenie_val'),
                'stav' => $request->input('stavpripomienky'),
                'user_id' => Auth::id()

            ]);
            if($reminder){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('reminders')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril pripomienku s ID: '.$reminder->id.' Pripomienka: '.$reminder->pripomienka.' Dátum pripomienky: '.$reminder->datum.' Opakovanie val: '.$reminder->opakovanie_val.' Opakovanie num: '.$reminder->opakovanie_num.' Pripomenutie num: '.$reminder->pripomenutie_num.' Pripomenutie val: '.$reminder->pripomenutie_val.' Stav: '.$reminder->stav.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('reminders.index');
            }
        }

        return back()->withInput()->with('errors', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function show(Reminder $reminder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $findreminder = Reminder::find($id);
        if((Auth::check())&&((Auth::user()->mod_7)==1)){

            return view('reminders.edit',['reminder'=>$findreminder]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reminder $reminder)
    {
        if((Auth::check())&&((Auth::user()->mod_7)==1)){
            $updated = $reminder->update([
                'pripomienka' => $request->input('pripomienka'),
                'datum' => $request->input('terminpripomienky'),
                'opakovanie_val' => $request->input('opakovanie_val'),
                'opakovanie_num' => $request->input('opakovanie_num'),
                'pripomenutie_num' => $request->input('upozornenie_num'),
                'pripomenutie_val' => $request->input('upozornenie_val'),
                'stav' => $request->input('stavpripomienky'),
                'user_id' => Auth::id()
            ]);
            if (! $updated){
                return back()->withInput();
            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('reminders')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval pripomienku s ID: '.$reminder->id.' Pripomienka: '.$reminder->pripomienka.' Dátum pripomienky: '.$reminder->datum.' Opakovanie val: '.$reminder->opakovanie_val.' Opakovanie num: '.$reminder->opakovanie_num.' Pripomenutie num: '.$reminder->pripomenutie_num.' Pripomenutie val: '.$reminder->pripomenutie_val.' Stav: '.$reminder->stav.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return redirect()->route('reminders.index')
                ->with('success','Defined task update successfully');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reminder $reminder)
    {
        //
    }
}
