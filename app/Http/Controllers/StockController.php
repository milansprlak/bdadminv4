<?php

namespace App\Http\Controllers;

use App\Stock;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;
use DB;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
            $clients = Client::all();

            $value_klient = 0;
            if (($request->klient == null) || ($request->malemnozstvo == null)) {
               // $value_klient = 0;
                $value_klient = 5; // HMH s.r.o.
                $value_malemnozstvo = 0;
               // $products = Stock::all();
                $products = Stock::where('client_id', $value_klient)->orderBy('nazov_produktu')->get();
            }
            else if (($request->klient == 0) && ($request->malemnozstvo == 0) )
            {
                $value_klient = 0;
                $value_malemnozstvo = 0;
                $products = Stock::orderBy('nazov_produktu')->get();
            }
            else if (($request->klient != 0) && ($request->malemnozstvo == 0))
            {
                $value_klient = $request->klient ;
                $value_malemnozstvo = 0;
                $products = Stock::where('client_id', $value_klient)->orderBy('nazov_produktu')->get();
            }
            else if (($request->klient == 0) && ($request->malemnozstvo == 1))
            {
                $value_klient = 0 ;
                $value_malemnozstvo = 1;

                $products = Stock::whereRaw('min_mnozstvo >= pocet_ks')->orderBy('nazov_produktu')->get();
            }
            else if (($request->klient != 0) && ($request->malemnozstvo == 1))
            {
                $value_klient = $request->klient ;
                $value_malemnozstvo = $request->malemnozstvo;

                $products = Stock::where('client_id', $value_klient)->whereRaw('min_mnozstvo >= pocet_ks')->orderBy('nazov_produktu')->get();
            }
            else
            {
                $value_klient = 0;
                $value_malemnozstvo = 0;
                $products = Stock::all();
            }

            // Hodnota skladu
            $hodnota_skladu = Stock::all()->sum(function($t){
                return $t->cena__bez_dph * $t->pocet_ks;
            });
            //



            return view('stock.index',['products'=>$products,'clients'=>$clients,'value_klient'=>$value_klient,'value_malemnozstvo' =>$value_malemnozstvo,'hodnota_skladu'=> $hodnota_skladu]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
            $clients=Client::all();
            return view('stock.create',['clients'=>$clients]);
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * str_replace(",", ".", $request->input('cena__bez_dph'));
     */
    public function store(Request $request)
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
            $product = Stock::create([
                'nazov_produktu' => $request->input('nazov_produktu'),
                'pocet_ks' => $request->input('pocet_ks'),
                'min_mnozstvo' => $request->input('min_mnozstvo'),
                'dph' => $request->input('dph'),
                'cena__bez_dph' => str_replace(",", ".", $request->input('cena__bez_dph')),
                'client_id' => $request->input('client_id'),
                'user_id' => Auth::id(),

            ]);
            if($product ){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('stock')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' pridal novy produkt do skladu s ID: '.$product->id.' nazov_produktu = '.$product->nazov_produktu.' pocet_ks = '.$product->pocet_ks.'ks dph = '.$product->dph.'% cena__bez_dph = '.$product->cena__bez_dph.' €  client_id = ' .$product->client_id .' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('stock.index');
            }
        }

        return back()->withInput()->with('errors', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
            $product= Stock::find($id);
            $clients=Client::all();
            return view('stock.edit',['product'=>$product,'clients'=>$clients]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
            try {

                $update = Stock ::findOrFail($id);

                $this->validate($request, [
                    'nazov_produktu' => 'required',

                ]);

                $input = $request->all();

                $update->fill($input)->save();

                //Log
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('stock')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval produkt v sklade s ID: '.$update->id.' na nazov_produktu = '.$update->nazov_produktu.' pocet_ks = '.$update->pocet_ks.'ks dph = '.$update->dph.'% cena__bez_dph = '.$update->cena__bez_dph.' €  client_id = ' .$update->client_id .' z IP: '.$ip_adresa.' v čase: '.$cas);
                // END LOG

                return redirect()->route('stock.index');

            }
            catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }
}
