<?php

namespace App\Http\Controllers;

use App\StatusInvoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Log;
class StatusInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $invoice_statuses = StatusInvoice::all();
            return view('status_invoice.index',['invoice_statuses'=> $invoice_statuses]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
            return view('status_invoice.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()){
            $invoice_status = StatusInvoice::create([
                'nazov_stavu' => $request->input('nazov_stavu'),


            ]);
            if($invoice_status){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('invoicestatus')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril stav faktury s ID: '.$invoice_status->id.' Nazov stavu = '.$invoice_status->nazov_stavu.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('status_invoice.index')->with('success_message', 'Stav faktúry bol úspešne vytvorený.');
            }
        }

        return back()->withInput()->with('danger_message', 'Nastala chyba!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StatusInvoice  $statusInvoice
     * @return \Illuminate\Http\Response
     */
    public function show(StatusInvoice $statusInvoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StatusInvoice  $statusInvoice
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::check()){
            $invoice_status = StatusInvoice::find($id);
            return view('status_invoice.edit',['invoice_status'=>$invoice_status]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StatusInvoice  $statusInvoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusInvoice $statusInvoice)
    {
        if(Auth::check()){
            $updated = $statusInvoice->update([
                'nazov_stavu' => $request->input('nazov_stavu')
            ]);
            if (! $updated){
                return back()->withInput();
            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('invoicestatus')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval stav faktury s ID: '.$statusInvoice->id.' na Nazov stavu = '.$statusInvoice->nazov_stavu.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return redirect()->route('status_invoice.index')
                ->with('success_message','Stav faktúry bol úspešne aktualizovaný.');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StatusInvoice  $statusInvoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusInvoice $statusInvoice)
    {
        //
    }
}
