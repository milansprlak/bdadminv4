<?php

namespace App\Http\Controllers;

use App\PriceOffer;
use App\PriceOfferDetail;
use App\Contractor;
use App\Client;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mail;
use Log;
use PDF;
use Illuminate\Support\Facades\Storage;


class PriceOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){

            $price_offers = PriceOffer::orderBy('id', 'DESC')->paginate(100);

            return view('price_offers.index',['price_offers'=> $price_offers]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $clients=Client::all();
            $invoice_contractors=Contractor::all();
            //Poradove cislo

            $priceOffer = PriceOffer::latest()->first();
            $date = date('y');

            if ($priceOffer==null)
            {
                $latest_number =0;
                $nextPriceOfferNumber = "CP".$date."/1";
            }
            else
            {
                $expNum = explode('/', $priceOffer->cislo);
                if (isset($expNum[1]))
                {
                    $nextPriceOfferNumber = "CP".$date.'/'.($expNum[1] + 1);
                }
                else
                {
                    $nextPriceOfferNumber = "CP".$date."/1";
                }
            }
            //END Poradove cislo
            //Datumy
            $today = Carbon::now()->toDateString();
            $datumsplatnosti = Carbon::now()->addDays(14)->toDateString();
            //end Datumy
            return view('price_offers.create',['clients'=>$clients,'invoice_contractors'=>$invoice_contractors,'nextPriceOfferNumber'=>$nextPriceOfferNumber,'today'=>$today,  'datumsplatnosti'=>$datumsplatnosti]);
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $price_offer = PriceOffer::create([
                'client_id' => $request->input('client_id'),
                'email' => $request->input('email'),
                'poznamka' => $request->input('poznamka'),
                'datum_vystavenia' => $request->input('datum_vystavenia'),
                'datum_splatnosti' => $request->input('datum_splatnosti'),
                'contractor_id' => $request->input('contractor_id'),
                'cislo' => $request->input('poradove_cislo'),
                'cena_bez_dph' =>0,
                'cena_s_dph' => 0,
                'ciastka_dph' => 0,
                'odoslane' =>0,
                'user_id' => Auth::id()


            ]);
            if($price_offer){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('priceoffer')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril cenovu ponuku s ID: '.$price_offer->id.' z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('price_offers.show', ['price_offer'=> $price_offer->id])->with('success_message', 'Cenová ponuka bola úspešne vytvorená');
            }
        }

        return back()->withInput()->with('danger_message', 'Nastala chyba.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PriceOffer  $priceOffer
     * @return \Illuminate\Http\Response
     */
    public function show(PriceOffer $priceOffer,Request $request )
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $priceOffer = PriceOffer::find($priceOffer->id); //
            // statisticke funkcie
            $sum_celkova_bez_dph = PriceOfferDetail::where('price_offer_id', '=', $priceOffer->id)->sum('celkova_cena_bez_dph');
            $sum_ciastka_dph = PriceOfferDetail::where('price_offer_id', '=', $priceOffer->id)->sum('ciastka_dph');
            $sum_celkova = PriceOfferDetail::where('price_offer_id', '=', $priceOffer->id)->sum('celkova_cena');
            //

         //   $ip_adresa = $request->getClientIp();
         //   $cas = Carbon::now()->toDateTimeString();
        //    Log::channel('priceoffer')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' zobrazil cenovu ponuku s ID: '.$priceOffer->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return view('price_offers.show',['priceOffer'=>$priceOffer,'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
        }
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PriceOffer  $priceOffer
     * @return \Illuminate\Http\Response
     */
    public function edit(PriceOffer $priceOffer)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $clients=Client::all();

            $priceOffer = PriceOffer::find($priceOffer->id);

            $invoice_contractors=Contractor::all();

            return view('price_offers.edit',['priceoffer'=>$priceOffer ,'clients'=>$clients,'invoice_contractors'=>$invoice_contractors]);
        }
        return back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PriceOffer  $priceOffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PriceOffer $priceOffer)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){


            // statisticke funkcie
            $sum_celkova_bez_dph = PriceOfferDetail::where('price_offer_id', '=', $priceOffer->id)->sum('celkova_cena_bez_dph');
            $sum_ciastka_dph = PriceOfferDetail::where('price_offer_id', '=', $priceOffer->id)->sum('ciastka_dph');
            $sum_celkova = PriceOfferDetail::where('price_offer_id', '=', $priceOffer->id)->sum('celkova_cena');
            //

            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();

            if ($request->input('odoslat') == 1)

            {

                if ($priceOffer->email == null)
                {
                    return back()->withInput()->with('warning_message', 'Nastavte emalovú adresu v cenovej ponuke!');
                }

                $products = PriceOfferDetail::all();
                // Data pre emailovu spravu
                $data = array(

                    'name' => $priceOffer->obchodne_meno,
                    'detail'=> $priceOffer->cislo,
                   // 'vvariable' => 'Odosielatel'
                );
                // Generovanie PDF suboru pre email
                // DOMPDF
             //   $pdf = PDF::loadView('price_offers.showpdf', ['priceoffer'=>$priceOffer , 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);

                // TCPDF
                $view = \View::make('price_offers.showpdf', ['priceoffer'=>$priceOffer , 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
                $html_content = $view->render();
                $your_new_content = mb_convert_encoding($html_content , 'HTML-ENTITIES', 'UTF-8');
                $pdf = new PDF();
                $pdf::SetTitle('cenova-ponuka_'.$priceOffer->cislo.'.pdf');
                $pdf::AddPage();
                $pdf::writeHTML($your_new_content, true, false, true, false);

                $pdfdoc = $pdf::Output('cenova-ponuka_'.$priceOffer->cislo.'.pdf', "S");
                $attachment = chunk_split(base64_encode($pdfdoc));





                // Ulozenie pdf na server - plne funkcne avsak zatial nepouyita funkcia
                // Generovanie PDF suboru pre ulozenie
                //  $pdftosave = PDF::loadView('price_offers.showpdf', ['priceoffer'=>$priceOffer , 'products'=>$products,'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);

                /*
                $fileName = ("cenova-ponuka_".$priceOffer->cislo.".pdf");
                $file = $pdftosave->output();
                Storage::put('/cenove-ponuky/'.$fileName, $file);
                $urladresa = ('/storage/cenove-ponuky/'.$fileName);
                */

                //Koniec Ulozenie pdf na server

                // Odoslanie emailu
               Mail::send('price_offers.mail', $data, function($message) use($attachment,$priceOffer)
                {
                    $message->getHeaders()->addTextHeader('Content-Transfer-Encoding:', 'base64');
                    $message->from(env('MAIL_USERNAME'), 'BEDRICH spol. s r.o.');

                    $message->to($priceOffer->email)->subject('Cenová ponuka č.'.$priceOffer->cislo);
                    $message->cc ((env('MAIL_USERNAME')));

                   $message->attachData(base64_decode($attachment),'cenova-ponuka_'.$priceOffer->cislo.'.pdf',['mime'=>'application/pdf']);

                });

               // Kontrola odosielania emailu
                if (Mail::failures()) {
                    Log::channel('priceoffer')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' CHYBA: sa pokúsil odoslať cenovú ponuku, avšak odoslanie emailu skončilo chybou.  Cenová ponuka s ID: '.$priceOffer->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                    return back()->withInput()->with('danger_message', 'Pri odosielaní emailovej správy nastala chyba!');
                }

               // Ulozenie adresy
                $updated = $priceOffer->update([
                    'odoslane' => 1,
                    'cena_bez_dph' =>$sum_celkova_bez_dph,
                    'cena_s_dph' => $sum_celkova,
                    'ciastka_dph' => $sum_ciastka_dph ,
                ]);
                if (! $updated){
                    Log::channel('priceoffer')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' CHYBA: Email bol úspešné odoslaný, ale chyba nastala pri zápise do DB!  Cenová ponuka s ID: '.$priceOffer->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                    return back()->withInput()->with('danger_message', 'Email bol úspešné odoslaný, ale chyba nastala pri zápise do DB!');
                }

                Log::channel('priceoffer')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' Cenová ponuka bola úspešné odoslaná a uložená. Cenová ponuka s ID: '.$priceOffer->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('price_offers.show',['priceoffer'=>$priceOffer->id])
                    ->with('success_message','Cenová ponuka bola úspešne odoslaná.');

            }
            // Aktualizácia cenovej ponuky, bez odoslania emailu.
            $updated = $priceOffer->update([
                'client_id' => $request->input('client_id'),
                'email' => $request->input('email'),
                'odoslane' => $request->input('odoslane'),
                'poznamka' => $request->input('poznamka'),
                'datum_vystavenia' => $request->input('datum_vystavenia'),
                'datum_splatnosti' => $request->input('datum_splatnosti'),
                'contractor_id' => $request->input('contractor_id'),
                'cislo' => $request->input('poradove_cislo'),
                'cena_bez_dph' =>$sum_celkova_bez_dph,
                'cena_s_dph' => $sum_celkova,
                'ciastka_dph' => $sum_ciastka_dph ,
                'user_id' => Auth::id()
            ]);
            if (! $updated){
                Log::channel('priceoffer')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' CHYBA: aktualizacia cenovej ponuky sa nepodatila. Cenová ponuka s ID: '.$priceOffer->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return back()->withInput()->with('danger_message','Nastala chyba pri aktualizovaný cenovej ponuky.');
            }

            Log::channel('priceoffer')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval cenovu ponuku s ID: '.$priceOffer->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return redirect()->route('price_offers.show',['priceoffer'=>$priceOffer->id])
                ->with('success_message','Cenová ponuka bola úspešne aktualizovaá.');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PriceOffer  $priceOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(PriceOffer $priceOffer)
    {
        //
    }
    public function generatePDF(Request $request,$id)
    {
        // https://github.com/barryvdh/laravel-dompdf
        if((Auth::check())&&((Auth::user()->mod_10)==1)){

            $priceoffer = PriceOffer::find($id); //


            // statisticke funkcie
            $sum_celkova_bez_dph = PriceOfferDetail::where('price_offer_id', '=', $priceoffer->id)->sum('celkova_cena_bez_dph');
            $sum_ciastka_dph = PriceOfferDetail::where('price_offer_id', '=', $priceoffer->id)->sum('ciastka_dph');
            $sum_celkova = PriceOfferDetail::where('price_offer_id', '=', $priceoffer->id)->sum('celkova_cena');

          //  $pdf = PDF::loadView('price_offers.showpdf',['priceoffer'=>$priceoffer , 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);

            //TCPDF
            $view = \View::make('price_offers.showpdf', ['priceoffer'=>$priceoffer , 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
            $html_content = $view->render();
            $your_new_content = mb_convert_encoding($html_content , 'HTML-ENTITIES', 'UTF-8');
            PDF::SetTitle('cenova-ponuka_'.$priceoffer->cislo.'.pdf');
            PDF::AddPage();
            PDF::writeHTML($your_new_content, true, false, true, false);

            //LOG
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('priceoffer')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vygeneroval pdf ponuky s ID: '.$priceoffer->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
            //
            return  PDF::Output('cenova-ponuka_'.$priceoffer->cislo.'.pdf');
            // DOMPDF
           // return $pdf->stream('result.pdf', array('Attachment'=>0));

        }
        return back();

    }
}
