<?php

namespace App\Http\Controllers;

use App\Task;
use App\Contract;
use App\DefinedTask;
use App\Employees;
use App\User;
use Carbon\Carbon;
use App\SubdefinedTask;
use Illuminate\Support\Facades\Auth;
use Log;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


return view('contracts.show');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //FIX Rezijne zakazky
        $id_zakazky = $request->input('contract_id');
        $contract = Contract::find($id_zakazky);
        $typ_zakazky = $contract->contracts_type_id;

        $datum = $request->input('datum');
        if ($datum==null)
        {
            $datum=Carbon::now()->toDateString();
        }


        if (($typ_zakazky == 1)||($typ_zakazky == 4)) { //ak je zakazka standartna alebo podradena

            $id_def_cinnosti = $request->input('defined_task_id');
            $id_subdef_cinnosti = $request->input('subdefined_task_id');
            $ostatne_cena_za_ks = str_replace(",", ".",$request->input('ostatne_cena_za_ks'));

            $def_cinnost = DefinedTask::find($id_def_cinnosti);
            $cena_cinnosti = $def_cinnost->cena_cinnost_hod;

            $trvanie_hod = $request->input('trvanie_hod');
            $trvanie_min = $request->input('trvanie_min');
            $celkovy_cas = $trvanie_hod + ($trvanie_min / 60);
            $celkova_cena_cinnosti = $cena_cinnosti * $celkovy_cas;

            if (($id_subdef_cinnosti == "") && ($ostatne_cena_za_ks == "")) {
                $cena_za_kg_materialu = 0;
                $cena_za_kg_materialu = 0;
                $hmotnost_materialu = 0;
                $celkova_cena_material = 0;
                $celkova_cena = $celkova_cena_cinnosti;

            } else if (($id_subdef_cinnosti == "") && ($ostatne_cena_za_ks != "")) {
                $cena_za_kg_materialu = $ostatne_cena_za_ks;
                $pocet_ks = $request->input('pocet_ks');
                $celkova_cena_material = $cena_za_kg_materialu * $pocet_ks;
                $celkova_cena = $celkova_cena_cinnosti + $celkova_cena_material;

            } else {
                $subdef_cinnost = SubdefinedTask::find($id_subdef_cinnosti);
                $cena_za_kg_materialu = $subdef_cinnost->cena_material_kg;
                $hmotnost_materialu = $request->input('pocet_ks_material');
                $pocet_ks = $request->input('pocet_ks');
                $celkova_cena_material = $hmotnost_materialu * $cena_za_kg_materialu * $pocet_ks;
                $celkova_cena = $celkova_cena_cinnosti + $celkova_cena_material;

            }
            $stav_tasku = 1;
            $task = Task::create([
                'contract_id' => $request->input('contract_id'),
                'contracts_type_id' => 1,
                'employy_id' => $request->input('employy_id'),
                'defined_task_id' => $request->input('defined_task_id'),
                'subdefined_task_id' => $request->input('subdefined_task_id'),
                'pocet_ks' => $request->input('pocet_ks'),
                'pocet_ks_material' => $request->input('pocet_ks_material'),
                'trvanie_hod' => $request->input('trvanie_hod'),
                'trvanie_min' => $request->input('trvanie_min'),
                'cena_cinnost_hod' => $cena_cinnosti,
                'cena_material_kg' => $cena_za_kg_materialu,
                'celkova_cena_cinnost' => $celkova_cena_cinnosti,
                'celkova_cena_material' => $celkova_cena_material,
                'celkova_cena' => $celkova_cena,
                'stav' => $stav_tasku,
                'poznamka' => $request->input('poznamka'),
                'datum' => $datum,


            ]);

        } else // Ak je typ zakazky rezijna
        {
            $employy = Employees::find($request->input('employy_id'));
            $cena_cinnosti = $employy->cena_prace_rezia;


            $trvanie_hod = $request->input('trvanie_hod');
            $trvanie_min = $request->input('trvanie_min');
            $celkovy_cas = $trvanie_hod + ($trvanie_min / 60);
            $celkova_cena_cinnosti = $cena_cinnosti * $celkovy_cas;
            // Ostatné vložená cena nákupu materiálu 9.1.2019
            $id_def_cinnosti = $request->input('defined_task_id');
            $id_subdef_cinnosti = $request->input('subdefined_task_id');
            $ostatne_cena_za_ks = str_replace(",", ".",$request->input('ostatne_cena_za_ks'));
            if (($id_subdef_cinnosti == "") && ($ostatne_cena_za_ks == "")) {
                $cena_za_kg_materialu = 0;
                $cena_za_kg_materialu = 0;
                $hmotnost_materialu = 0;
                $celkova_cena_material = 0;
                $celkova_cena = $celkova_cena_cinnosti;

            } else if (($id_subdef_cinnosti == "") && ($ostatne_cena_za_ks != ""))
            {
                $cena_za_kg_materialu = $ostatne_cena_za_ks;
                $pocet_ks=$request->input('pocet_ks');
                $celkova_cena_material = $cena_za_kg_materialu*$pocet_ks;
                $celkova_cena = $celkova_cena_cinnosti + $celkova_cena_material;

            }
            else
            {
                $subdef_cinnost = SubdefinedTask::find($id_subdef_cinnosti);
                $cena_za_kg_materialu = $subdef_cinnost->cena_material_kg;
                $hmotnost_materialu = $request->input('pocet_ks_material');
                $pocet_ks=$request->input('pocet_ks');
                $celkova_cena_material = $hmotnost_materialu * $cena_za_kg_materialu*$pocet_ks;
                $celkova_cena = $celkova_cena_cinnosti + $celkova_cena_material;

            }



            $stav_tasku = 1;
            $task = Task::create([
                'contract_id' => $request->input('contract_id'),
                'contracts_type_id' => 2,
                'employy_id' => $request->input('employy_id'),
                'defined_task_id' => $request->input('defined_task_id'),
                'subdefined_task_id' => $request->input('subdefined_task_id'),
                'pocet_ks' => $request->input('pocet_ks'),
                'pocet_ks_material' => $request->input('pocet_ks_material'),
                'trvanie_hod' => $request->input('trvanie_hod'),
                'trvanie_min' => $request->input('trvanie_min'),
                'cena_cinnost_hod' => $cena_cinnosti,
                'cena_material_kg' => $cena_za_kg_materialu,
                'celkova_cena_cinnost' => $celkova_cena_cinnosti,
                'celkova_cena_material' => $celkova_cena_material,
                'celkova_cena' => $celkova_cena,
                'stav' => $stav_tasku,
                'poznamka' => $request->input('poznamka'),
                'datum' => $datum,

            ]);


        }


        if($task){

            return redirect('contracts/'.$request->contract_id);
        }
        return back()->withInput()->with('errors', 'Error creating new Task');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $taskfind= Task::find($id); // Najde id konktetnej task
        if (($taskfind->created_at->isToday())||((Auth::check())&&((Auth::user()->mod_1)==1))) {
        $contract = Contract::all();
        $defined_task= DefinedTask::all();
        $subdefined_task= SubdefinedTask::all();
        $employy= Employees::all();
        return view('tasks.edit',['contract'=>$contract,'task'=>$taskfind, 'employy'=>$employy,'defined_task'=>$defined_task,'subdefined_task'=>$subdefined_task]);
        }
        return back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $presmerovanie = $request->input('contract_id');
        $datum = $request->input('datum');
        if ($datum==null)
        {
            $datum=Carbon::now()->toDateString();
        }

        //FIX Rezijne zakazky
        $id_zakazky = $request->input('contract_id');
        $contract = Contract::find($id_zakazky);
        $typ_zakazky = $contract->contracts_type_id;

        if (($typ_zakazky == 1)||($typ_zakazky == 4)) { //ak je zakazka standartna alebo podradena

        $id_def_cinnosti = $request->input('defined_task_id');
        $id_subdef_cinnosti = $request->input('subdefined_task_id');
        $ostatne_cena_za_ks = str_replace(",", ".",$request->input('ostatne_cena_za_ks'));

        $def_cinnost = DefinedTask::find($id_def_cinnosti);
        $cena_cinnosti = $def_cinnost->cena_cinnost_hod;

        $trvanie_hod = $request->input('trvanie_hod');
        $trvanie_min = $request->input('trvanie_min');
        $celkovy_cas = $trvanie_hod + ($trvanie_min/60);
        $celkova_cena_cinnosti = $cena_cinnosti * $celkovy_cas;

        if (($id_subdef_cinnosti == "") && ($ostatne_cena_za_ks == "")) {
            $cena_za_kg_materialu = 0;
            $cena_za_kg_materialu = 0;
            $hmotnost_materialu = 0;
            $celkova_cena_material = 0;
            $celkova_cena = $celkova_cena_cinnosti;

        } else if (($id_subdef_cinnosti == "") && ($ostatne_cena_za_ks != ""))
        {
            $cena_za_kg_materialu = $ostatne_cena_za_ks;
            $pocet_ks=$request->input('pocet_ks');
            $celkova_cena_material = $cena_za_kg_materialu*$pocet_ks;
            $celkova_cena = $celkova_cena_cinnosti + $celkova_cena_material;

        }
        else
        {
            $subdef_cinnost = SubdefinedTask::find($id_subdef_cinnosti);
            $cena_za_kg_materialu = $subdef_cinnost->cena_material_kg;
            $hmotnost_materialu = $request->input('pocet_ks_material');
            $pocet_ks=$request->input('pocet_ks');
            $celkova_cena_material = $hmotnost_materialu * $cena_za_kg_materialu*$pocet_ks;
            $celkova_cena = $celkova_cena_cinnosti + $celkova_cena_material;

        }
        $updated = $task->update([
            'contract_id' => $request->input('contract_id'),
            'contracts_type_id' => 1, //editovat
            'employy_id' => $request->input('employy_id'),
            'defined_task_id' => $request->input('defined_task_id'),
            'subdefined_task_id' => $request->input('subdefined_task_id'),
            'pocet_ks' => $request->input('pocet_ks'),
            'pocet_ks_material' => $request->input('pocet_ks_material'),
            'trvanie_hod' => $request->input('trvanie_hod'),
            'trvanie_min' => $request->input('trvanie_min'),
            'cena_cinnost_hod' => $cena_cinnosti,
            'cena_material_kg' => $cena_za_kg_materialu,
            'celkova_cena_cinnost' => $celkova_cena_cinnosti,
            'celkova_cena_material' => $celkova_cena_material,
            'celkova_cena' => $celkova_cena,
            'poznamka' => $request->input('poznamka'),
            'datum' => $datum,

        ]);
        } else // Ak je typ zakazky rezijna
        {
            $employy = Employees::find($request->input('employy_id'));
            $cena_cinnosti = $employy->cena_prace_rezia;


            $trvanie_hod = $request->input('trvanie_hod');
            $trvanie_min = $request->input('trvanie_min');
            $celkovy_cas = $trvanie_hod + ($trvanie_min / 60);
            $celkova_cena_cinnosti = $cena_cinnosti * $celkovy_cas;
            // Ostatné vložená cena nákupu materiálu 9.1.2019
            $id_def_cinnosti = $request->input('defined_task_id');
            $id_subdef_cinnosti = $request->input('subdefined_task_id');
            $ostatne_cena_za_ks = str_replace(",", ".",$request->input('ostatne_cena_za_ks'));
            if (($id_subdef_cinnosti == "") && ($ostatne_cena_za_ks == "")) {
                $cena_za_kg_materialu = 0;
                $cena_za_kg_materialu = 0;
                $hmotnost_materialu = 0;
                $celkova_cena_material = 0;
                $celkova_cena = $celkova_cena_cinnosti;

            } else if (($id_subdef_cinnosti == "") && ($ostatne_cena_za_ks != ""))
            {
                $cena_za_kg_materialu = $ostatne_cena_za_ks;
                $pocet_ks=$request->input('pocet_ks');
                $celkova_cena_material = $cena_za_kg_materialu*$pocet_ks;
                $celkova_cena = $celkova_cena_cinnosti + $celkova_cena_material;

            }
            else
            {
                $subdef_cinnost = SubdefinedTask::find($id_subdef_cinnosti);
                $cena_za_kg_materialu = $subdef_cinnost->cena_material_kg;
                $hmotnost_materialu = $request->input('pocet_ks_material');
                $pocet_ks=$request->input('pocet_ks');
                $celkova_cena_material = $hmotnost_materialu * $cena_za_kg_materialu*$pocet_ks;
                $celkova_cena = $celkova_cena_cinnosti + $celkova_cena_material;

            }

            $updated = $task->update([
                'contract_id' => $request->input('contract_id'),
                'contracts_type_id' => 2, //editovat
                'employy_id' => $request->input('employy_id'),
                'defined_task_id' => $request->input('defined_task_id'),
                'subdefined_task_id' => $request->input('subdefined_task_id'),
                'pocet_ks' => $request->input('pocet_ks'),
                'pocet_ks_material' => $request->input('pocet_ks_material'),
                'trvanie_hod' => $request->input('trvanie_hod'),
                'trvanie_min' => $request->input('trvanie_min'),
                'cena_cinnost_hod' => $cena_cinnosti,
                'cena_material_kg' => $cena_za_kg_materialu,
                'celkova_cena_cinnost' => $celkova_cena_cinnosti,
                'celkova_cena_material' => $celkova_cena_material,
                'celkova_cena' => $celkova_cena,
                'poznamka' => $request->input('poznamka'),
                'datum' => $datum
            ]);


        }
        if (! $updated){
            return back()->withInput();
        }
        $ip_adresa = $request->getClientIp();
        $cas = Carbon::now()->toDateTimeString();
        Log::channel('taskedit')->info('V zakazke s id = '.$task->contract_id.' bola aktualizovana uloha s ID = '.$task->id.' z IP: '.$ip_adresa.' v čase: '.$cas);

        return redirect()->action(
            'ContractsController@show', ['id' => $presmerovanie]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
