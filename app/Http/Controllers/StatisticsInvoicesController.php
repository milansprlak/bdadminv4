<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;

class StatisticsInvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)) {


            if ($request->rok == null)  {
                $rok = date("Y");

            } else {
                $rok = $request->rok;

            }
            // 2 - Odoslana - este zostava zapltit
            // 3 - Uhradena - zaplatene
            // 4 - Po splatnosti
            $odoslane = Invoice::whereYear('datum_vystavenia', '=',$rok)
                ->where('invoice_status_id', 2)->sum('cena_s_dph');
            $uhhradene = Invoice::whereYear('datum_vystavenia', '=',$rok)
                ->where('invoice_status_id', 3)->sum('cena_s_dph');
            $po_splatnosti = Invoice::whereYear('datum_vystavenia', '=',$rok)
                ->where('invoice_status_id', 4)->sum('cena_s_dph');

            $neuhradene = $odoslane + $po_splatnosti;
            $vyfakturovane = $odoslane + $uhhradene; // vyfakturovanie za danne obdobie


            //
            $statistika = DB::table('invoices')->whereYear('datum_vystavenia', '=',$rok)
                ->select(DB::raw('SUM(cena_s_dph) as total_cena, MONTH(datum_vystavenia) as mesiac, YEAR(datum_vystavenia) as rok'))
                ->groupBy(DB::raw('YEAR(datum_vystavenia) ASC, MONTH(datum_vystavenia) ASC'))->get();

            $collection = collect($statistika);


            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' zobrazil statistiky faktur z IP: '.$ip_adresa.' v čase: '.$cas);


            return view('statistics.invoices', ['statistika'=>$collection,'rok'=>$rok,'vyfakturovane' =>$vyfakturovane, 'uhhradene' =>$uhhradene, 'neuhradene'=>$neuhradene]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }
}
