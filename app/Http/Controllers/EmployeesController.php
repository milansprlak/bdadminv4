<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Employees;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        if ($request->datum == null) {
            $date = date("Y-m-d", strtotime('-1 days'));
        }
        else
        {
            $date=$request->datum;

        }
        // Odpracovane hodiny
            $employyes = Employees::all();

            foreach ($employyes as $employy)
            {
                $id = $employy->id;
                $meno = $employy->meno;
                $cena_rezia = $employy->cena_prace_rezia;
                $fotka = $employy->url_fotka;

                $trvanie_hod= Task::whereDate('datum', $date)->where('employy_id', '=', $employy->id)->sum('trvanie_hod');
                $trvanie_min = Task::whereDate('datum', $date)->where('employy_id', '=', $employy->id)->sum('trvanie_min');

                if ($trvanie_min > 59) {
                    $pom_hod = floor($trvanie_min / 60);
                    $trvanie_hod = $trvanie_hod + $pom_hod;
                    $pom_min = $pom_hod * 60;
                    $trvanie_min = $trvanie_min - $pom_min;
                }
                $zamestnanec[] = ['id' => $id,'meno' => $meno,'cena_rezia' => $cena_rezia,'fotka' => $fotka, 'hod' => $trvanie_hod, 'min' => $trvanie_min];
            }
            //

        return view('employees.index',['zamestnanci'=>$zamestnanec,'datum'=>$date]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        return view('employees.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            $employy = Employees::create([
                'meno' => $request->input('meno'),
                'url_fotka' => $request->input('url_fotka'),
                'cena_prace_rezia' => $request->input('cena_prace_rezia')

            ]);
            if($employy){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('employees')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril zamestnanca s ID: '.$employy->id.' Meno zamestnanca: '.$employy->meno.' Režijná cena práce: '.$employy->cena_prace_rezia.' URL Fotka: '.$employy->url_fotka.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('employees.index');
            }
        }

        return back()->withInput()->with('errors', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employy  $employy
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $employy= Employees::find($id); // Najde id konktetnho zamestnanca

        if (($request->fromdatum == null) || ($request->todatum == null)) {
            $from = date("Y-m-d", strtotime('-31 days'));
            $to = date("Y-m-d");
        }
        else {

            $from = $request->fromdatum;
            $to = $request->todatum;
            }
       $tasks_employy = $employy->tasks; //vsetky ulohy konktetneho uzivatela

        // Pole zakazky
        foreach ($tasks_employy as $task_employy)
        {
            $zakazka[]=['id'=>$task_employy->zakazka->id,'poradove_cislo'=>$task_employy->zakazka->poradove_cislo,'meno'=>$task_employy->zakazka->nazov,'created_at'=>$task_employy->zakazka->created_at];
        }
        // Vymazanie rovnakych zaznamov
        $collection = collect($zakazka);
        $unique_data = $collection->where('created_at', '>=', $from)->where('created_at', '<=', $to)->unique()->values()->all();

        // Konicec vymazania rovn. zaznamov
        $pocet_zakazok = count($unique_data);

        return view('statistics.showemployy',['employy'=>$employy,'fromdatum'=>$from,'todatum'=>$to,'zakazky'=>$unique_data,'pocet_zakazok'=>$pocet_zakazok]);
        }

        return redirect()->back()->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employy  $employy
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $employy = Employees::find($id);
        return view('employees.edit',['employy'=>$employy]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employy  $employy
     * @return \Illuminate\Http\Response
     */
  /*  public function update(Request $request, Employees $employy)
    {
        $updated = $employy->update([
            'meno' => $request->input('meno'),
            'url_fotka' => $request->input('url_fotka')
        ]);
        if (! $updated){
            return back()->withInput();
        }
        return redirect()->route('employees.index')
            ->with('success','Defined task update successfully');
    }*/
    public function update($id, Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        try {
            $update = Employees::findOrFail($id);

            $this->validate($request, [
                'meno' => 'required',

            ]);

            $input = $request->all();

            $update->fill($input)->save();
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('employees')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval zamestnanca s ID: '.$update->id.' Meno zamestnanca: '.$update->meno.' Režijná cena práce: '.$update->cena_prace_rezia.' URL Fotka: '.$update->url_fotka.' z IP: '.$ip_adresa.' v čase: '.$cas);

            return redirect()->route('employees.index');

        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        }
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employy  $employy
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        try {
            $findemployy = Employees::find($id);

            $findemployy ->delete();
            return redirect()->route('employees.index');
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        }
        return back();
    }
}
