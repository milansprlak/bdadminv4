<?php

namespace App\Http\Controllers;

use App\InvoicePaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class InvoicePaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $invoice_payments = InvoicePaymentMethod::all();
            return view('invoice_payment.index',['invoice_payments'=> $invoice_payments]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            return view('invoice_payment.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $invoice_payment = InvoicePaymentMethod::create([
                'forma_uhrady' => $request->input('forma_uhrady'),


            ]);
            if($invoice_payment){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('invoicepayment')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril metodu platby faktury s ID: '.$invoice_payment->id.' s nazvom: '.$invoice_payment->forma_uhrady.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('invoice_payment.index')->with('success_message', 'Forma uhrady faktúry bola úspešne vytvorena.');
            }
        }

        return back()->withInput()->with('danger_message', 'Nastala chyba!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvoicePaymentMethod  $invoicePaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function show(InvoicePaymentMethod $invoicePaymentMethod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvoicePaymentMethod  $invoicePaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $invoice_payment = InvoicePaymentMethod::find($id);
            return view('invoice_payment.edit',['invoice_payment'=>$invoice_payment]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvoicePaymentMethod  $invoicePaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            try {
                $update = InvoicePaymentMethod::findOrFail($id);

                $this->validate($request, [
                    'forma_uhrady' => 'required',

                ]);

                $input = $request->all();

                $update->fill($input)->save();

                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('invoicepayment')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval metodu platby faktury s ID: '.$update->id.' na nazov: '.$update->forma_uhrady.' z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('invoice_payment.index');

            }
            catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvoicePaymentMethod  $invoicePaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoicePaymentMethod $invoicePaymentMethod)
    {
        //
    }
}
