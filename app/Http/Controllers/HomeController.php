<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use Log;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('contracts.index');
    }
    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }
    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Vaše aktuálne heslo sa nezhoduje so zadaným heslom. Prosím skúste znova.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Nové heslo nemôže byť rovnaké ako vaše aktuálne heslo. Vyberte iné heslo.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        $ip_adresa = $request->getClientIp();
        $cas = Carbon::now()->toDateTimeString();
        Log::channel('uzivatel')->info('Použivateľ : '.$user->name.' ID: '.$user->id.' si zmenil heslo z: '.$ip_adresa.' v čase: '.$cas);
        return redirect()->back()->with("success","Heslo bolo úspešné zmenené !");
    }
}
