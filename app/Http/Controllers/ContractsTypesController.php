<?php

namespace App\Http\Controllers;

use App\ContractsType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class ContractsTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            $contracts_types= ContractsType::all();
            return view('contractstypes.index',['contracts_types'=>$contracts_types]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            return view('contractstypes.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            $contracts_type = ContractsType::create([
                'nazov_typu' => $request->input('nazov_typu'),


            ]);
            if($contracts_type){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('contracttype')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril typ zakazky s ID: '.$contracts_type->id.' Názov typu: '.$contracts_type->nazov_typu.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('contracts_types.index');
            }
        }

        return back()->withInput()->with('errors', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContractsType  $contractsType
     * @return \Illuminate\Http\Response
     */
    public function show(ContractsType $contractsType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContractsType  $contractsType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            $contracts_types = ContractsType::find($id);
            return view('contractstypes.edit',['contracts_types'=>$contracts_types]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContractsType  $contractsType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContractsType $contractsType)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            $updated = $contractsType->update([
                'nazov_typu' => $request->input('nazov_typu')
            ]);
            if (! $updated){
                return back()->withInput();
            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('contracttype')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval typ zakazky s ID: '.$contractsType->id.' Názov typu: '.$contractsType->nazov_typu.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return redirect()->route('contracts_types.index')
                ->with('success','Defined contracts type update successfully');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContractsType  $contractsType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            try {
                $findcontractsType = ContractsType::find($id);

                $findcontractsType ->delete();
                return redirect()->route('contracts_types.index');
            }
            catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return back();

    }
}
