<?php

namespace App\Http\Controllers;

use App\SendStock;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Log;

class SendStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){

            $exist = SendStock::where('contract_id',  $request->input('contract_id'))->count();

            if($exist > 0)
            {
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::info('Uzivatel: '.Auth::user()->name.' sa pokusil o zapisanie zakazky s id: '.$request->input('contract_id').' do skladu: Zakazka uz v minulosti bola zapisana. IP: '.$ip_adresa.' v čase: '.$cas);



            }
            else
            {
                $product= SendStock::create([
                    'contract_id' => $request->input('contract_id'),
                    'stock_id' => $request->input('product_id'),
                    'pocet_ks' => $request->input('pocet_ks'),
                    'user_id' => Auth::id(),

                ]);
                if($product ){

                    $model = Stock::find($request->input('product_id'));
                    $model->pocet_ks += $request->input('pocet_ks');
                    $model->save();
                    $ip_adresa = $request->getClientIp();
                    $cas = Carbon::now()->toDateTimeString();
                    Log::channel('sendstock')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' uspesne pridal zakazku s ID: '.$product->contract_id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                    Log::channel('stock')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' uspesne vlozil do skladu s produktom id: '.$model->id.' zakazku s ID: '.$product->contract_id.' s pocet ks = '.$request->input('pocet_ks').' z IP: '.$ip_adresa.' v čase: '.$cas);

                    //return redirect()->route('stock.index')->with('success_message', 'Zákazka bola úspešne bola pridaná do skladu!');
                   return redirect()->route('contracts.edit',$request->input('contract_id'))->with('success_message', 'Zákazka bola úspešne bola pridaná do skladu!');
                }
            }


            // https://stackify.com/laravel-logging-tutorial/
        }
        return back()->withInput()->with('info_message', 'Zákazka už v minulosti bola pridaná do skladu!');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SendStock  $sendStock
     * @return \Illuminate\Http\Response
     */
    public function show(SendStock $sendStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SendStock  $sendStock
     * @return \Illuminate\Http\Response
     */
    public function edit(SendStock $sendStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SendStock  $sendStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SendStock $sendStock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SendStock  $sendStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(SendStock $sendStock)
    {
        //
    }
}
