<?php

namespace App\Http\Controllers;

use App\Contractor;
use App\Invoice;
use App\InvoiceDetail;
use App\StatusInvoice;
use App\InvoicePaymentMethod;
use App\Client;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Log;
use PDF;
use Mail;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $invoice_statuses = StatusInvoice::all();
            $clients = Client::all();

            if (($request->fromdatum == null) || ($request->todatum == null)) {
                $value_stav_faktury = 0;
                $value_klient = 0;
                $invoices = Invoice::orderBy('id', 'DESC')->paginate(100);

                // $from = date("Y-m-d", strtotime('-31 days'));
                $from = ('2018-06-01');
                $to = date("Y-m-d");
            } else {
                $value_stav_faktury = $request->stav_faktury;
                $value_klient = $request->klient;
                $from = $request->fromdatum;
                $to = $request->todatum;

                if (($request->klient == 0)&&($request->stav_faktury == 0)) {
                    $invoices = Invoice::whereDate('datum_vystavenia', '>=', $from)
                        ->whereDate('datum_vystavenia', '<=', $to)->orderBy('id', 'DESC')
                        ->paginate(100);
                }
                elseif (($request->klient == 0)&&($request->stav_faktury != 0)) {
                        $invoices = Invoice::where('invoice_status_id', $request->stav_faktury)->whereDate('datum_vystavenia', '>=', $from)
                            ->whereDate('datum_vystavenia', '<=', $to)->orderBy('id', 'DESC')
                            ->paginate(100);

                }
                elseif (($request->klient != 0)&&($request->stav_faktury == 0)) {
                    $invoices = Invoice::where('client_id', $request->klient)->whereDate('datum_vystavenia', '>=', $from)
                        ->whereDate('datum_vystavenia', '<=', $to)->orderBy('id', 'DESC')
                        ->paginate(100);

                } else {
                    $invoices = Invoice::where('invoice_status_id', $request->stav_faktury)->where('client_id', $request->klient)->whereDate('datum_vystavenia', '>=', $from)
                        ->whereDate('datum_vystavenia', '<=', $to)->orderBy('id', 'DESC')
                        ->paginate(100);
                }
            }

            // LOG
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' zobrazil vsetky faktury z IP: '.$ip_adresa.' v čase: '.$cas);
            //

            return view('invoice.index',['invoices'=>$invoices,'invoice_statuses'=>$invoice_statuses,'value_stav_faktury'=>$value_stav_faktury,'fromdatum'=>$from,'todatum'=>$to,'clients'=>$clients,'value_klient'=>$value_klient]);



        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)) {

            //Poradove cislo

            $invoice = Invoice::latest()->first();
            $date = date('y');
            if ($invoice==null)
            {
                $latest_number =0;
                $invoice_next_number= "3".$date."001";
            }
            else
            {
            $latest_number =$invoice->poradove_cislo;
               $test = substr($latest_number, 0, -3);
                if(stripos($test,$date)){
                    $invoice_next_number = $latest_number+1;
                }
                else
                {
                    $invoice_next_number= "3".$date."001";
                }
            }
            //END Poradove cislo
            $clients=Client::all();
            $invoice_statuses=StatusInvoice ::all();
            $invoice_payments=InvoicePaymentMethod::all();
            $invoice_contractors=Contractor::all();
            //Datumy
            $today = Carbon::now()->toDateString();
            $datumsplatnosti = Carbon::now()->addDays(14)->toDateString();
            //end Datumy
            return view('invoice.create',['clients'=>$clients,'invoice_statuses'=>$invoice_statuses,'invoice_payments'=>$invoice_payments,'today'=>$today, 'datum_dodania'=>$today, 'datumsplatnosti'=>$datumsplatnosti,'invoice_contractors'=>$invoice_contractors,'invoice_next_number'=>$invoice_next_number]);
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
            $invoice = Invoice::create([
                'client_id' => $request->input('client_id'),
                'contractor_id' => $request->input('contractor_id'),
                'poradove_cislo' => $request->input('poradove_cislo'),
                'cislo_objednavky' => $request->input('cislo_objednavky'),
                'datum_vystavenia' => $request->input('datum_vystavenia'),
                'datum_dodania' => $request->input('datum_dodania'),
                'datum_splatnosti' => $request->input('datum_splatnosti'),
                'invoice_payment_id' => $request->input('forma_uhrady'),
                'variabilny_symbol' => $request->input('variabilny_symbol'),
                'konstantny_symbol' => $request->input('konstantny_symbol'),
                'specificky_symbol' => $request->input('specificky_symbol'),
                'invoice_status_id' => $request->input('stav_faktury'),
                'poznamka' => $request->input('poznamka'),
                'user_id' => Auth::id()


            ]);
            if($invoice){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril fakturu s ID: '.$invoice->id.' z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('invoice.show', ['invoice'=> $invoice->id])->with('success_message', 'Faktúra bola úspešne vytvorená');
            }
        }

        return back()->withInput()->with('danger_message', 'Vytvorenie faktúry skončilo chybou.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice,Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){
        $invoice = Invoice::find($invoice->id); //
        $products = Stock::where('client_id', '=',$invoice->client_id)->orwhere('id','=',1)->get();
        // statisticke funkcie
            $sum_celkova_bez_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena_bez_dph');
            $sum_ciastka_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('ciastka_dph');
            $sum_celkova = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena');
        //

            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' zobrazil fakturu s ID: '.$invoice->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
        return view('invoice.show',['invoice'=>$invoice, 'products'=>$products,'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
        }
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {

        if((Auth::check())&&((Auth::user()->mod_10)==1)){

            $invoice = Invoice::find($invoice->id);

            // Overenie ci fa obsahuje polozky, kvoli editacii klienta

            $invoice_detail_overenie = InvoiceDetail::where('invoice_id',$invoice->id)->count();
           if ($invoice_detail_overenie == 0)
           {
               $overenie = true;
           }
           else
           {
               $overenie = false;
           }
            //

            $clients=Client::all();
            $invoice_statuses=StatusInvoice ::all();
            $invoice_payments=InvoicePaymentMethod::all();
            $invoice_contractors=Contractor::all();

            return view('invoice.edit',['invoice'=>$invoice ,'clients'=>$clients,'invoice_statuses'=>$invoice_statuses,'invoice_payments'=>$invoice_payments, 'invoice_contractors'=>$invoice_contractors,'overenie'=>$overenie]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        if((Auth::check())&&((Auth::user()->mod_10)==1)){

            if ($request->input('changestatus') == 1) // Iba meni stav na odoslane a uhradene
            {
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                $invoice = Invoice::find($invoice->id);
                // Statisticke vypocty
                $sum_celkova_bez_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena_bez_dph');
                $sum_ciastka_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('ciastka_dph');
                $sum_celkova = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena');
                // Statisticke vypocty

                // Odoslanie emailu
                $akcia = $request->input('status');

                if ($akcia == 2 )
                {
                    $email_oberatela = $invoice->klientfaktury->email;
                    if ($email_oberatela == null)
                    {
                        return back()->withInput()->with('warning_message', 'Nastavte emalovú adresu odberateľa!');
                    }


                    // Data pre emailovu spravu
                    $data = array(

                        'name' => $invoice->klientfaktury->nazov_firmy,
                        'detail'=> $invoice->poradove_cislo,
                        // 'vvariable' => 'Odosielatel'
                    );
                    // Generovanie PDF suboru pre email
                    // TCPDF
                    $view = \View::make('invoice.showpdf', ['invoice'=>$invoice, 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
                    $html_content = $view->render();
                    $your_new_content = mb_convert_encoding($html_content , 'HTML-ENTITIES', 'UTF-8');
                    $pdf = new PDF();
                    PDF::SetTitle('faktura_'.$invoice->poradove_cislo.'.pdf');

                     $pdfheslo = $invoice->klientfaktury->pdfheslo;
                     if ($pdfheslo != null)
                     {
                        PDF::setProtection(array(),$pdfheslo);    // Nastavenie pdf hesla
                     }

                    $pdf::AddPage();
                    $pdf::writeHTML($your_new_content, true, false, true, false);

                    $pdfdoc = $pdf::Output('faktura_'.$invoice->poradove_cislo.'.pdf', "S");
                    $attachment = chunk_split(base64_encode($pdfdoc));

                    // Dodaci list
                    $viewdodacilist = \View::make('invoice.dodaci-listpdf', ['invoice'=>$invoice, 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
                    $dodacilist = $viewdodacilist->render();
                    $your_new_dodacilist = mb_convert_encoding($dodacilist , 'HTML-ENTITIES', 'UTF-8');
                    \PDF::reset();
                    $pdfdodacilist = new PDF();

                    $pdfdodacilist::SetTitle('dodaci_list_'.$invoice->poradove_cislo.'.pdf');

                    $pdfdodacilist::AddPage();
                    $pdfdodacilist::writeHTML($your_new_dodacilist, true, false, true, false);

                    $pdfdodacilist =$pdfdodacilist::Output('dodaci_list_'.$invoice->poradove_cislo.'.pdf', "S");
                    $attachment2 = chunk_split(base64_encode($pdfdodacilist));

                    // Koniec dodacieho listu

                    // Odoslanie emailu
                    Mail::send('invoice.mail', $data, function($message) use($attachment,$attachment2,$invoice,$email_oberatela)
                    {
                        $message->getHeaders()->addTextHeader('Content-Transfer-Encoding:', 'base64');
                        $message->from(env('MAIL_USERNAME'), 'BEDRICH spol. s r.o.');

                        $message->to($email_oberatela)->subject('Faktúra č.'.$invoice->poradove_cislo);
                        $message->cc ((env('MAIL_USERNAME')));

                        $message->attachData(base64_decode($attachment),'faktura_'.$invoice->poradove_cislo.'.pdf',['mime'=>'application/pdf']);
                        $message->attachData(base64_decode($attachment2),'dodaci_list_'.$invoice->poradove_cislo.'.pdf',['mime'=>'application/pdf']);

                    });

                    // Kontrola odosielania emailu
                    if (Mail::failures()) {
                        Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' CHYBA: sa pokúsil odoslať faktúru, avšak odoslanie emailu skončilo chybou.  Cenová ponuka s ID: '.$invoice->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                        return back()->withInput()->with('danger_message', 'Pri odosielaní emailovej správy nastala chyba!');
                    }
                }
                // Koniec odoslania emailu

                // Ulozenie do DB
                $invoice->invoice_status_id = $request->input('status');
                $invoice->cena_bez_dph =  $sum_celkova_bez_dph;
                $invoice->cena_s_dph = $sum_celkova;
                $invoice->ciastka_dph = $sum_ciastka_dph;
                $invoice->save();


                Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval fakturu s ID: '.$invoice->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('invoice.index')->with('success_message', 'Stav faktúry bol úspešne zmenený.');

            }
            else{


            // statisticke funkcie
            $sum_celkova_bez_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena_bez_dph');
            $sum_ciastka_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('ciastka_dph');
            $sum_celkova = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena');
            //

            $updated = $invoice->update([
                'client_id' => $request->input('client_id'),
                'contractor_id' => $request->input('contractor_id'),
                'poradove_cislo' => $request->input('poradove_cislo'),
                'cislo_objednavky' => $request->input('cislo_objednavky'),
                'datum_vystavenia' => $request->input('datum_vystavenia'),
                'datum_dodania' => $request->input('datum_dodania'),
                'datum_splatnosti' => $request->input('datum_splatnosti'),
                'invoice_payment_id' => $request->input('forma_uhrady'),
                'variabilny_symbol' => $request->input('variabilny_symbol'),
                'konstantny_symbol' => $request->input('konstantny_symbol'),
                'specificky_symbol' => $request->input('specificky_symbol'),
                'invoice_status_id' => $request->input('stav_faktury'),
                'cena_bez_dph' => $sum_celkova_bez_dph,
                'cena_s_dph' => $sum_celkova,
                'ciastka_dph' => $sum_ciastka_dph,
                'poznamka' => $request->input('poznamka'),
                'user_id' => Auth::id()
            ]);
            if (! $updated){
                return back()->withInput();
            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval fakturu s ID: '.$invoice->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return redirect()->route('invoice.show',['invoice'=>$invoice->id])
                ->with('success_message','Faktúra bola úspešne aktualizovaá.');
            }
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }
    public function generatePDF(Request $request,$id)
    {
        // https://github.com/barryvdh/laravel-dompdf
        if((Auth::check())&&((Auth::user()->mod_10)==1)){

            $invoice = Invoice::find($id); //

            // statisticke funkcie
            $sum_celkova_bez_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena_bez_dph');
            $sum_ciastka_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('ciastka_dph');
            $sum_celkova = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena');
// DOMPDF
//            $pdf = PDF::loadView('invoice.showpdf',['invoice'=>$invoice, 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
//            $pdf->setPaper('A4');

            // TCPDF
            $view = \View::make('invoice.showpdf', ['invoice'=>$invoice, 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
            $html_content = $view->render();
            $your_new_content = mb_convert_encoding($html_content , 'HTML-ENTITIES', 'UTF-8');
           // PDF::setFontSubsetting(true);
            PDF::SetTitle('faktura_'.$invoice->poradove_cislo.'.pdf');
            // Heslo pdf suboru
           $pdfheslo = $invoice->klientfaktury->pdfheslo;
           if ($pdfheslo != null)
           {
               PDF::setProtection(array(),$pdfheslo);    // Nastavenie pdf hesla
           }
            //
            PDF::AddPage();
            PDF::writeHTML($your_new_content, true, false, true, false);

            //LOG
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vygeneroval pdf faktury s ID: '.$invoice->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
            //

          return  PDF::Output('faktura_'.$invoice->poradove_cislo.'.pdf');
             // DOMPDF
            // return $pdf->stream('faktura_'.$invoice->poradove_cislo.'.pdf', array('Attachment'=>0));

        }
        return back();

    }
    public function generatePDFdodaci(Request $request,$id)
    {
        // https://github.com/barryvdh/laravel-dompdf
        if((Auth::check())&&((Auth::user()->mod_10)==1)){

            $invoice = Invoice::find($id); //

            // statisticke funkcie
            $sum_celkova_bez_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena_bez_dph');
            $sum_ciastka_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('ciastka_dph');
            $sum_celkova = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena');


            // TCPDF
            $view = \View::make('invoice.dodaci-listpdf', ['invoice'=>$invoice, 'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
            $html_content = $view->render();
            $your_new_content = mb_convert_encoding($html_content , 'HTML-ENTITIES', 'UTF-8');
            // PDF::setFontSubsetting(true);
            PDF::SetTitle('dodaci_list_'.$invoice->poradove_cislo.'.pdf');
            PDF::AddPage();
            PDF::writeHTML($your_new_content, true, false, true, false);

            //LOG
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vygeneroval pdf dodaci list s ID: '.$invoice->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
            //

            return  PDF::Output('dodaci_list_'.$invoice->poradove_cislo.'.pdf');


        }
        return back();

    }
    /* Plne funkcna avsak zbytocna funkcia
    public function print(Request $request,$id)
    {

        if((Auth::check())&&((Auth::user()->mod_10)==1)){

            $invoice = Invoice::find($id); //
            $products = Stock::all();
            // statisticke funkcie
            $sum_celkova_bez_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena_bez_dph');
            $sum_ciastka_dph = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('ciastka_dph');
            $sum_celkova = InvoiceDetail::where('invoice_id', '=', $invoice->id)->sum('celkova_cena');
            //

            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('invoice')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytlacil fakturu s ID: '.$invoice->id.' z IP: '.$ip_adresa.' v čase: '.$cas);
            return view('invoice.showprint',['invoice'=>$invoice, 'products'=>$products,'sum_celkova_bez_dph'=>$sum_celkova_bez_dph,'sum_ciastka_dph'=>$sum_ciastka_dph,'sum_celkova'=>$sum_celkova]);
        }
        return back();
    }
    */

}
