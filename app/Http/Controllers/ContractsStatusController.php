<?php

namespace App\Http\Controllers;

use App\ContractsStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class ContractsStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $contracts_statuses= ContractsStatus::all();
        return view('contractsstatus.index',['contracts_statuses'=>$contracts_statuses]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        return view('contractsstatus.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            $contracts_status = ContractsStatus::create([
                'nazov_stavu' => $request->input('nazov_stavu'),


            ]);
            if($contracts_status){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('contractstatus')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril stav zakazky s ID: '.$contracts_status->id.' Názov stavu: '.$contracts_status->nazov_stavu.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('contracts_statuses.index');
            }
        }

        return back()->withInput()->with('errors', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContractsStatus  $contractsStatus
     * @return \Illuminate\Http\Response
     */
    public function show(ContractsStatus $contractsStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContractsStatus  $contractsStatus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $contracts_statuses = ContractsStatus::find($id);
        return view('contractsstatus.edit',['contracts_statuses'=>$contracts_statuses]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContractsStatus  $contractsStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContractsStatus $contractsStatus)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $updated = $contractsStatus->update([
            'nazov_stavu' => $request->input('nazov_stavu')
        ]);
        if (! $updated){
            return back()->withInput();
        }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('contractstatus')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval stav zakazky s ID: '.$contractsStatus->id.' Názov stavu: '.$contractsStatus->nazov_stavu.' z IP: '.$ip_adresa.' v čase: '.$cas);
        return redirect()->route('contracts_statuses.index')
            ->with('success','Defined task update successfully');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContractsStatus  $contractsStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        try {
            $findcontractsStatus = ContractsStatus::find($id);

            $findcontractsStatus ->delete();
            return redirect()->route('contracts_statuses.index');
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        }
        return back();
    }
}
