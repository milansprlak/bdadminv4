<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
        $clients= Client::all();
        return view('clients.index',['clients'=>$clients]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
        return view('clients.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
            $client = Client::create([
                'nazov_firmy' => $request->input('nazov_firmy'),
                'kontaktna_osoba' => $request->input('kontaktna_osoba'),
                'ico' => $request->input('ico'),
                'dic' => $request->input('dic'),
                'icdph' => $request->input('icdph'),
                'fa_adresa' => $request->input('fa_adresa'),
                'fa_mesto' => $request->input('fa_mesto'),
                'fa_psc' => $request->input('fa_psc'),
                'fa_krajina' => $request->input('fa_krajina'),
                'telefon' => $request->input('telefon'),
                'mobil' => $request->input('mobil'),
                'email' => $request->input('email'),
                'pdfheslo' => $request->input('pdfheslo'),

            ]);
            if($client){

                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('client')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril klienta s ID: '.$client->id.' Názov firmy: '.$client->nazov_firmy.' Kontaktná osoba: '.$client->kontaktna_osoba.' IČO: '.$client->ico.' DIČ: '.$client->dic.' IČDPH: '.$client->icdph.' Fakturačná adresa: '.$client->fa_adresa.' Mesto: '.$client->fa_mesto.' PSČ: '.$client->fa_psc.' Krajina: '.$client->fa_krajina.' Telefón: '.$client->telefon.' Mobil: '.$client->mobil.' E-MAIL: '.$client->email.' z IP: '.$ip_adresa.' v čase: '.$cas);

                return redirect()->route('clients.index')->with('success_message', 'Klient bol úspešne vytvorený');
            }
        }

        return back()->withInput()->with('danger_message', 'Vytvorenie klienta skončilo chybou.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
        $findclient = Client::find($id);
        return view('clients.edit',['client'=>$findclient]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        if((Auth::check())&&(((Auth::user()->mod_1)==1)||((Auth::user()->mod_10)==1))){
        $updated = $client->update([
            'nazov_firmy' => $request->input('nazov_firmy'),
            'kontaktna_osoba' => $request->input('kontaktna_osoba'),
            'ico' => $request->input('ico'),
            'dic' => $request->input('dic'),
            'icdph' => $request->input('icdph'),
            'fa_adresa' => $request->input('fa_adresa'),
            'fa_mesto' => $request->input('fa_mesto'),
            'fa_psc' => $request->input('fa_psc'),
            'fa_krajina' => $request->input('fa_krajina'),
            'telefon' => $request->input('telefon'),
            'mobil' => $request->input('mobil'),
            'email' => $request->input('email'),
            'pdfheslo' => $request->input('pdfheslo')
        ]);
        if (! $updated){
            return back()->withInput();
        }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('client')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval klienta s ID: '.$client->id.' Názov firmy: '.$client->nazov_firmy.' Kontaktná osoba: '.$client->kontaktna_osoba.' IČO: '.$client->ico.' DIČ: '.$client->dic.' IČDPH: '.$client->icdph.' Fakturačná adresa: '.$client->fa_adresa.' Mesto: '.$client->fa_mesto.' PSČ: '.$client->fa_psc.' Krajina: '.$client->fa_krajina.' Telefón: '.$client->telefon.' Mobil: '.$client->mobil.' E-MAIL: '.$client->email.' z IP: '.$ip_adresa.' v čase: '.$cas);
        return redirect()->route('clients.index')
            ->with('success_message','Klient bol úspešne aktualizovaý.');
        }
        return back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }
}
