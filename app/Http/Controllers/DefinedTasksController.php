<?php

namespace App\Http\Controllers;

use App\DefinedTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class DefinedTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $definedTasks= DefinedTask::all();
        return view('defined_tasks.index',['definedTasks'=>$definedTasks]);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        return view('defined_tasks.create');
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
            $definedtask = DefinedTask::create([
                'nazov' => $request->input('nazov'),
                'cena_cinnost_hod' => $request->input('cena_cinnost_hod')

            ]);
            if($definedtask){
                $ip_adresa = $request->getClientIp();
                $cas = Carbon::now()->toDateTimeString();
                Log::channel('definedtask')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' vytvoril definovany task s ID: '.$definedtask->id.' Názov kategórie: '.$definedtask->nazov.' Cena činnosti za 1 hod: '.$definedtask->cena_cinnost_hod.' z IP: '.$ip_adresa.' v čase: '.$cas);
                return redirect()->route('definedtasks.index');
            }
        }

        return back()->withInput()->with('errors', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DefinedTask  $definedTask
     * @return \Illuminate\Http\Response
     */
    public function show(DefinedTask $definedTask)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DefinedTask  $definedTask
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $finddefinedTask= DefinedTask::find($id);
        return view('defined_tasks.edit',['definedtask'=>$finddefinedTask]);
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DefinedTask  $definedTask
     * @return \Illuminate\Http\Response
     */
   // public function update(Request $request, DefinedTask $definedTask)
    public function update(Request $request, DefinedTask $definedtask)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        $updated = $definedtask->update([
            'nazov' => $request->input('nazov'),
            'cena_cinnost_hod' => $request->input('cena_cinnost_hod')
        ]);
        if (! $updated){
            return back()->withInput();
        }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('definedtask')->info('Používateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' aktualizoval definovany task s ID: '.$definedtask->id.' Názov kategórie: '.$definedtask->nazov.' Cena činnosti za 1 hod: '.$definedtask->cena_cinnost_hod.' z IP: '.$ip_adresa.' v čase: '.$cas);
        return redirect()->route('definedtasks.index')
            ->with('success','Defined task update successfully');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DefinedTask  $definedTask
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if((Auth::check())&&((Auth::user()->mod_1)==1)){
        try {
            $finddefinedTask = DefinedTask::find($id);

            $finddefinedTask ->delete();
            return redirect()->route('definedtasks.index');
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        }
        return back();
    }
}
