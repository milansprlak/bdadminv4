<?php

namespace App\Http\Controllers;


use App\Task;
use App\Employees;
use App\ContractStatistic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Carbon\Carbon;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ((Auth::check())&&((Auth::user()->mod_3)==1)) {
            $employyers = Employees::all();
            $value_typ_zakazky = 1;
            $rezijne_value_typ_zakazky = 2;

            if (($request->fromdatum == null) || ($request->todatum == null)) {

                $value_stav_tasku = 2;

                $from = date("Y-m-d", strtotime('-31 days'));
                $to = date("Y-m-d");

                // Celkove firemne statistiky
                $statistiky_pocet = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->count();
                $statistiky_zisk = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->sum('zisk');
                $statistiky_zisk_firma_pred = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->sum('zisk_firmu');
                $statistiky_zisk_zamestnanci = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->sum('zisk_zamestnanci');

                // Podiel pre auto/frezu
                $auto_freza_perc = 0;
                $auto_freza_value = $statistiky_zisk_firma_pred * ($auto_freza_perc/100);
                $statistiky_zisk_firma = $statistiky_zisk_firma_pred - $auto_freza_value;
                //

                // Celkove rezijne statistiky
                $rezijne_statistiky_pocet = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $rezijne_value_typ_zakazky)->count();
                $rezijne_celkova_hodnota = Task::whereDate('updated_at', '>=', $from)
                    ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $rezijne_value_typ_zakazky)->where('stav', $value_stav_tasku)->sum('hodnota_tasku');
                $rezijne_trvanie_hod = Task::whereDate('updated_at', '>=', $from)
                    ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $rezijne_value_typ_zakazky)->where('stav', $value_stav_tasku)->sum('trvanie_hod');
                $rezijne_trvanie_min = Task::whereDate('updated_at', '>=', $from)
                    ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $rezijne_value_typ_zakazky)->where('stav', $value_stav_tasku)->sum('trvanie_min');
                if ($rezijne_trvanie_min > 59) {
                    $pom_hod = floor($rezijne_trvanie_min / 60);
                    $rezijne_trvanie_hod = $rezijne_trvanie_hod + $pom_hod;
                    $pom_min = $pom_hod * 60;
                    $rezijne_trvanie_min = $rezijne_trvanie_min - $pom_min;
                }
                $rezijne_statistiky[] = ['pocet' => $rezijne_statistiky_pocet,'hodnota' => $rezijne_celkova_hodnota,'hod' => $rezijne_trvanie_hod,'min' => $rezijne_trvanie_min];
                //KONIEC REZIJNYCH STATISTIK



                foreach ($employyers as $employy) {
                    $meno = $employy->meno;
                    $idmeno= $employy->id;
                    $hodnota = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('hodnota_tasku');
                    $trvanie_hod = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('trvanie_hod');
                    $trvanie_min = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('trvanie_min');
                    // odmena za zodpovednu osobu pri zakazke
                    $bonus = ContractStatistic::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('zodpovedna_osoba_id', '=', $employy->id)->sum('podiel_zodp_osoba_hodnota');
                    //

                    if ($trvanie_min > 59) {
                        $pom_hod = floor($trvanie_min / 60);
                        $trvanie_hod = $trvanie_hod + $pom_hod;
                        $pom_min = $pom_hod * 60;
                        $trvanie_min = $trvanie_min - $pom_min;
                    }

                    // Rezijne statistiky zamestnancov
                    $rezhodnota = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', 2)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('hodnota_tasku');
                    $reztrvanie_hod = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', 2)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('trvanie_hod');
                    $reztrvanie_min = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', 2)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('trvanie_min');
                    $bonus = ContractStatistic::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('zodpovedna_osoba_id', '=', $employy->id)->sum('podiel_zodp_osoba_hodnota');
                    if ($reztrvanie_min > 59) {
                        $pom_hod = floor($reztrvanie_min / 60);
                        $reztrvanie_hod = $reztrvanie_hod + $pom_hod;
                        $pom_min = $pom_hod * 60;
                        $reztrvanie_min = $reztrvanie_min - $pom_min;
                    }
                    //

                    $zamestnanec[] = ['meno' => $meno,'idmeno' => $idmeno, 'hodnota' => $hodnota,'bonus' => $bonus, 'hod' => $trvanie_hod, 'min' => $trvanie_min,'rezhodnota' => $rezhodnota, 'rezhod' => $reztrvanie_hod, 'rezmin' => $reztrvanie_min];
                }


            } else {
                $value_stav_tasku = 2;
                $from = $request->fromdatum;
                $to = $request->todatum;

                // Celkove firemne statistiky
                $statistiky_pocet = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->count();
                $statistiky_zisk = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->sum('zisk');
                $statistiky_zisk_firma_pred = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->sum('zisk_firmu');
                $statistiky_zisk_zamestnanci = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->sum('zisk_zamestnanci');

                // Podiel pre auto/frezu
                $auto_freza_perc = $request->auto_freza;
                $auto_freza_value = $statistiky_zisk_firma_pred * ($auto_freza_perc/100);
                $statistiky_zisk_firma = $statistiky_zisk_firma_pred - $auto_freza_value;
                //

                // Celkove rezijne statistiky
                $rezijne_statistiky_pocet = ContractStatistic::whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to)->where('contracts_type_id', $rezijne_value_typ_zakazky)->count();
                $rezijne_celkova_hodnota = Task::whereDate('updated_at', '>=', $from)
                    ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $rezijne_value_typ_zakazky)->where('stav', $value_stav_tasku)->sum('hodnota_tasku');
                $rezijne_trvanie_hod = Task::whereDate('updated_at', '>=', $from)
                    ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $rezijne_value_typ_zakazky)->where('stav', $value_stav_tasku)->sum('trvanie_hod');
                $rezijne_trvanie_min = Task::whereDate('updated_at', '>=', $from)
                    ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $rezijne_value_typ_zakazky)->where('stav', $value_stav_tasku)->sum('trvanie_min');
                if ($rezijne_trvanie_min > 59) {
                    $pom_hod = floor($rezijne_trvanie_min / 60);
                    $rezijne_trvanie_hod = $rezijne_trvanie_hod + $pom_hod;
                    $pom_min = $pom_hod * 60;
                    $rezijne_trvanie_min = $rezijne_trvanie_min - $pom_min;
                }
                $rezijne_statistiky[] = ['pocet' => $rezijne_statistiky_pocet,'hodnota' => $rezijne_celkova_hodnota,'hod' => $rezijne_trvanie_hod,'min' => $rezijne_trvanie_min];
                //KONIEC REZIJNYCH STATISTIK
                foreach ($employyers as $employy) {
                    $meno = $employy->meno;
                    $idmeno= $employy->id;
                    $hodnota = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('hodnota_tasku');
                    $trvanie_hod = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('trvanie_hod');
                    $trvanie_min = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('trvanie_min');
                    // odmena za zodpovednu osobu pri zakazke
                    $bonus = ContractStatistic::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', $value_typ_zakazky)->where('zodpovedna_osoba_id', '=', $employy->id)->sum('podiel_zodp_osoba_hodnota');
                    //
                    if ($trvanie_min > 59) {
                        $pom_hod = floor($trvanie_min / 60);
                        $trvanie_hod = $trvanie_hod + $pom_hod;
                        $pom_min = $pom_hod * 60;
                        $trvanie_min = $trvanie_min - $pom_min;
                    }
                    // Rezijne statistiky zamestnancov
                    $rezhodnota = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', 2)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('hodnota_tasku');
                    $reztrvanie_hod = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', 2)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('trvanie_hod');
                    $reztrvanie_min = Task::whereDate('updated_at', '>=', $from)
                        ->whereDate('updated_at', '<=', $to)->where('contracts_type_id', 2)->where('stav', $value_stav_tasku)->where('employy_id', '=', $employy->id)->sum('trvanie_min');
                    if ($reztrvanie_min > 59) {
                        $pom_hod = floor($reztrvanie_min / 60);
                        $reztrvanie_hod = $reztrvanie_hod + $pom_hod;
                        $pom_min = $pom_hod * 60;
                        $reztrvanie_min = $reztrvanie_min - $pom_min;
                    }
                    //

                    $zamestnanec[] = ['meno' => $meno,'idmeno' => $idmeno, 'hodnota' => $hodnota,'bonus' => $bonus, 'hod' => $trvanie_hod, 'min' => $trvanie_min,'rezhodnota' => $rezhodnota, 'rezhod' => $reztrvanie_hod, 'rezmin' => $reztrvanie_min];
                }

            }
            $ip_adresa = $request->getClientIp();
            $cas = Carbon::now()->toDateTimeString();
            Log::channel('statisticsemployees')->info('Použivateľ : '.Auth::user()->name.' ID: '.Auth::user()->id.' zobrazil statistiku firmy a zamestnancov z IP: '.$ip_adresa.' v čase: '.$cas);
            return view('statistics.index', ['zamestnanec' => $zamestnanec,'rezijne_statistiky' => $rezijne_statistiky, 'fromdatum' => $from, 'todatum' => $to, 'statistiky_pocet' => $statistiky_pocet, 'statistiky_zisk' => $statistiky_zisk, 'statistiky_zisk_firma' => $statistiky_zisk_firma, 'statistiky_zisk_zamestnanci' => $statistiky_zisk_zamestnanci, 'auto_freza_perc' => $auto_freza_perc, 'auto_freza_value' => $auto_freza_value, 'statistiky_zisk_firma_pred' => $statistiky_zisk_firma_pred]);
        }
        return back();

    }
}
