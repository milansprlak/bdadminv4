<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DownloadFile extends Model
{
    protected $fillable = [
        'id',
        'nazov',
        'url',
        'contract_id',
        'employy_id',
    ];
    public function contract()
    {
        return $this->belongsTo('App\Contract','contract_id');
    }
    public function employy()
    {
        return $this->belongsTo('App\Employees','employy_id');
    }
}
