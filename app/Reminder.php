<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $fillable = [
        'id',
        'pripomienka',
        'datum',
        'opakovanie_num',
        'opakovanie_val',
        'pripomenutie_num',
        'pripomenutie_val',
        'stav',
        'user_id',
    ];
}
