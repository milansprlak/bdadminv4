<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceOffer extends Model
{
    protected $fillable = [
        'id',
        'client_id',
        'contractor_id',
        'odoslane',
        'poznamka',
        'datum_vystavenia',
        'datum_splatnosti',
        'cislo',
        'cena_bez_dph',
        'cena_s_dph',
        'ciastka_dph',
        'email',
        'user_id',

    ];
    public function polozky()
    {
        return $this->hasMany('App\PriceOfferDetail');
    }
    public function contractor()
    {
        return $this->belongsTo('App\Contractor','contractor_id');
    }
    public function klient()
    {
        return $this->belongsTo('App\Client','client_id');
    }

}
