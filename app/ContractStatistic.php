<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractStatistic extends Model
{
    protected $fillable = [
        'id',
        'contract_id',
        'client_id',
        'contracts_type_id',
        'zodpovedna_osoba_id',
        'pocet_ks',
        'podiel_zodp_osoba_perc',
        'podiel_zodp_osoba_hodnota',
        'hodnota_zakazky_ks',
        'celkovy_cas',
        'celkova_cena_cinnost',
        'celkova_cena_material',
        'celkova_vyrobna_cena',
        'celkova_predajna_cena_ks',
        'celkova_predajna_cena',
        'zisk',
        'zisk_firmu',
        'zisk_zamestnanci',
        'podiel_zamestnanci',

        ];
}
