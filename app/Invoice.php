<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'id',
        'client_id',
        'contractor_id',
        'invoice_status_id',
        'invoice_payment_id',
        'poradove_cislo',
        'cislo_objednavky',
        'variabilny_symbol',
        'specificky_symbol',
        'konstantny_symbol',
        'datum_vystavenia',
        'datum_splatnosti',
        'datum_dodania',
        'cena_bez_dph',
        'cena_s_dph',
        'ciastka_dph',
        'poznamka',
        'user_id',
        ];
    public function items()
    {
        return $this->hasMany('App\InvoiceDetail');
    }
    public function klientfaktury()
    {
        return $this->belongsTo('App\Client','client_id');
    }
    public function invoicesstatus()
    {
        return $this->belongsTo('App\StatusInvoice','invoice_status_id');
    }
    public function contractor()
    {
        return $this->belongsTo('App\Contractor','contractor_id');
    }
    public function forma_uhrady()
    {
        return $this->belongsTo('App\InvoicePaymentMethod','invoice_payment_id');
    }

}
