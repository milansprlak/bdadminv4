<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diary extends Model
{
    protected $fillable = [
        'id',
        'poznamka',
        'user_id',
    ];
}
