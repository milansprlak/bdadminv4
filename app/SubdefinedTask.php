<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubdefinedTask extends Model
{
    protected $fillable = [
        'id',
        'nazov',
        'nadradena_kat_id',
        'cena_material_kg',

    ];
    public function user(){
        return $this->belongsToMany('App\User');
    }
    public function defined_task(){
        return $this->hasMany('App\DefinedTask');

    }
    public function subdefined_task()
    {
        return $this->belongsTo('App\Contract');
    }

}
