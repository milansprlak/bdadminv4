<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'id',
        'sprava',
        'stav',
        'user_id',

    ];

    public function uzivatel()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
