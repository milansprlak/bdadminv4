<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'id',
        'nazov_produktu',
        'pocet_ks',
        'min_mnozstvo',
        'dph',
        'cena__bez_dph',
        'client_id',
        'user_id',
    ];
    public function klient()
    {
        return $this->belongsTo('App\Client','client_id');
    }

}
