<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'id',
        'nazov_firmy',
        'kontaktna_osoba',
        'email',
        'telefon',
        'mobil',
        'ico',
        'dic',
        'icdph',
        'fa_adresa',
        'fa_mesto',
        'fa_psc',
        'fa_krajina',
        'pdfheslo',

    ];
    public function user(){
        return $this->belongsToMany('App\User');
    }

    public function contract(){
        return $this->belongsTo('App\Contract');

    }
}
