<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Reminder;
use App\Message;
use Carbon\Carbon;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); //
        View::composer('*', function ($view) {

            if ((Auth::check())&&((Auth::user()->mod_7)==1)) {
                $pocet = 0;
                $today = Carbon::now()->toDateString();
                $opakujuce_pripomienky = null;
                $neopakujuce_pripomienky = null;


                // Neopakujuce sa pripomienky
                $reminders = Reminder::where('stav', '1')->where('opakovanie_val', '=', '0')
                    ->whereDate('datum', '>=', $today)->get();

                foreach ($reminders as $reminder) {
                    $pripomienka_val = $reminder->pripomenutie_val;
                    $pripomienka_num = $reminder->pripomenutie_num;
                    //PRIPOMIENKA 1: dni; 2: tyzdne; 3:mesiace
                    switch ($pripomienka_val) {
                        case 1:
                            $pripomienka_dni = $pripomienka_num;
                            break;
                        case 2:
                            $pripomienka_dni = $pripomienka_num * 7;
                            break;
                        case 3:
                            $pripomienka_dni = $pripomienka_num * 30;
                            break;
                    }

                    //  $date =date("Y-m-d", strtotime("$reminder->datum - $pripomienka_dni day"));
                    $date_start = Carbon::parse($reminder->datum);
                    $date = $date_start->subDays($pripomienka_dni);
                    $datum = $reminder->datum;


                    if ($date <= $today) {
                        $pocet++;
                        //$neopakujuce_pripomienky[] = $reminder;
                        $neopakujuce_pripomienky[] = ['id' => $reminder->id, 'pripomienka' => $reminder->pripomienka, 'datum' => $datum, 'datumod' => $date, 'opakovanie_val' => $reminder->opakovanie_val, 'opakovanie_num' => $reminder->opakovanie_num, 'pripomenutie_num' => $reminder->pripomenutie_num, 'pripomenutie_val' => $reminder->pripomenutie_val, 'stav' => $reminder->stav, 'created_at' => $reminder->created_at];

                    }
                }
                // Koniec neopakujucich pripomienok
                // Začiatok opakovacích
                $reminders_opakujuce = Reminder::where('stav', '1')->where('opakovanie_val', '!=', '0')->get();

                foreach ($reminders_opakujuce as $reminder_opak) {
                    $opkaovanie_val = $reminder_opak->opakovanie_val;
                    $opakovanie_num = $reminder_opak->opakovanie_num;

                    $pripomienka_val = $reminder_opak->pripomenutie_val;
                    $pripomienka_num = $reminder_opak->pripomenutie_num;


                    $start = Carbon::parse($reminder_opak->datum);
                    $datenew = Carbon::parse($reminder_opak->datum);
                    $end = Carbon::now()->toDateString();


                    //OPAKOVANIE 1: dni; 2: tyzdne; 3: mesiace; 4: roky
                    // https://carbon.nesbot.com/docs/
                    switch ($opkaovanie_val) {
                        case 1: // Pridať dni
                            while ($start < $end) {

                                $start->addDays($opakovanie_num);
                                $datenew->addDays($opakovanie_num);
                            }

                            break;
                        case 2: // Pridať týždne

                            while ($start < $end) {

                                $start->addWeeks($opakovanie_num);
                                $datenew->addWeeks($opakovanie_num);
                            }
                            break;
                        case 3: // Pridať mesiace
                            while ($start < $end) {

                                $start->addMonths($opakovanie_num);
                                $datenew->addMonths($opakovanie_num);
                            }

                            break;
                        case 4: // Pridať roky
                            while ($start < $end) {

                                $start->addYears($opakovanie_num);
                                $datenew->addYears($opakovanie_num);
                            }
                            break;

                    }


                    // Pripomienka switch
                    switch ($pripomienka_val) {
                        case 1:
                            $pripomienka_dni = $pripomienka_num;

                            break;
                        case 2:
                            $pripomienka_dni = $pripomienka_num * 7;

                            break;
                        case 3:
                            $pripomienka_dni = $pripomienka_num * 30;
                            break;
                    }

                    $date = $start->subDays($pripomienka_dni);
                    $datum = $reminder_opak->datum;

                    if ($date <= $today) {
                        $pocet++;
                        $opakujuce_pripomienky[] = ['id' => $reminder_opak->id, 'pripomienka' => $reminder_opak->pripomienka, 'datum' => $datum, 'datumod' => $date, 'datumnew' => $datenew, 'opakovanie_val' => $reminder_opak->opakovanie_val, 'opakovanie_num' => $reminder_opak->opakovanie_num, 'pripomenutie_num' => $reminder_opak->pripomenutie_num, 'pripomenutie_val' => $reminder_opak->pripomenutie_val, 'stav' => $reminder_opak->stav, 'created_at' => $reminder_opak->created_at];
                    }

                }
                // Koniec opakovacích pripomienok

                //  return view('reminders.index',['reminders'=>$neopakujuce_pripomienky, 'reminders_rep'=>$opakujuce_pripomienky]);


                View::share('remindersh', $neopakujuce_pripomienky);
                View::share('reminders_reph', $opakujuce_pripomienky);
                View::share('pocet', $pocet);
            }
            // Messages
            $messages = Message::where('stav', '=', 1)->orderBy('id', 'DESC')->get();
            $messages_pocetnost = Message::where('stav', '=', 1)->count();
            //
            View::share('pubmessages',$messages);
            View::share('messages_pocetnost',$messages_pocetnost);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
