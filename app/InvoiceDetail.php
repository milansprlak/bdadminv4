<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $fillable = [
        'id',
        'invoice_id',
        'stock_id',
        'nazov_polozky',
        'pocet_ks',
        'cena_bez_dph_ks',
        'celkova_cena_bez_dph',
        'celkova_cena',
        'ciastka_dph',
        'vyska_dph',
        'jednotka',
        'user_id',

    ];
}
