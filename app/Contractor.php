<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contractor extends Model
{
    protected $fillable = [
        'id',
        'nazov_firmy',
        'kontaktna_osoba',
        'email',
        'telefon',
        'mobil',
        'ico',
        'dic',
        'icdph',
        'fa_adresa',
        'fa_mesto',
        'fa_psc',
        'fa_krajina',
        'www_site',
        'nazov_banky',
        'iban',
        'swift',

    ];
}
