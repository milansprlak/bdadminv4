<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceOfferDetail extends Model
{
    protected $fillable = [
        'id',
        'price_offer_id',
        'nazov_polozky',
        'jednotka',
        'pocet_ks',
        'cena_bez_dph_ks',
        'celkova_cena_bez_dph',
        'celkova_cena',
        'ciastka_dph',
        'vyska_dph',
        'user_id',

    ];
}
