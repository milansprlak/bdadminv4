<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractsStatus extends Model
{
    protected $fillable = [
        'id',
        'nazov_stavu',

    ];
    public function contract(){
        return $this->belongsTo('App\Contract');

    }
}
