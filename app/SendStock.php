<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendStock extends Model
{
    protected $fillable = [
        'id',
        'contract_id',
        'stock_id',
        'pocet_ks',
        'user_id',
    ];
}
