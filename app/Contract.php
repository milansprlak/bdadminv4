<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
        'id',
        'product_id',
        'popis',
        'client_id',
        'contracts_type_id',
        'nadradena_id',
        'pocet_ks',
        'poradove_cislo',
        'cislo_objednavky',
        'cislo_faktury',
        'zodpovedna_osoba_id',
        'podiel_zodp_osoba_perc',
        'odhadovany_cas',
        'stav_zakazky',
        'hodnota_zakazky_za_ks',
        'datumodovzdania',
        'user_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
     public function tasks()
    {
        return $this->hasMany('App\Task');
    }
    public function clients()
    {
        return $this->hasMany('App\Client');
    }
    public function defined_task()
    {
        return $this->hasMany('App\DefinedTask');
    }
    public function subdefined_task()
    {
        return $this->hasMany('App\SubdefinedTask');
    }
    public function employees()
    {
        return $this->hasMany('App\Employees');
    }
    public function zodpovednaosoba()
    {
        return $this->belongsTo('App\Employees','zodpovedna_osoba_id');
    }
    public function typzakazky()
    {
        return $this->belongsTo('App\ContractsType','contracts_type_id');
    }
    public function stavzakazky()
    {
        return $this->belongsTo('App\ContractsStatus','stav_zakazky');
    }
    public function klientzakazky()
    {
        return $this->belongsTo('App\Client','client_id');
    }
    public function produktzakazky()
    {
        return $this->belongsTo('App\Stock','product_id');
    }
    public function contractsstatus()
    {
        return $this->hasMany('App\ContractsStatus');
    }


}
