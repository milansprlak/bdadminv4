<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
Route::resource('/','ContractsController');
Route::resource('contracts','ContractsController');
Route::get('tasks/create/{contract_id?}', 'TasksController@create');
Route::resource('tasks', 'TasksController');
Route::resource('stock', 'StockController');
Route::resource('definedtasks', 'DefinedTasksController');
Route::resource('subdefinedtasks', 'SubdefinedTasksController');
Route::resource('employees', 'EmployeesController');
Route::resource('clients', 'ClientsController');
Route::resource('contracts_statuses', 'ContractsStatusController');
Route::resource('contracts_types', 'ContractsTypesController');
Route::resource('upload', 'DownloadFilesController');
Route::resource('statistics', 'StatisticsController');
Route::resource('statisticsclient', 'StatisticsClientController');
Route::resource('diary', 'DiaryController');
Route::resource('cash', 'CashController');
Route::get('/contracts/getPDF/{id}','ContractsController@generatePDF');
Route::resource('deadline', 'ContractsDeadlineesController');
Route::get('reminders/all', 'ReminderController@all');
Route::resource('reminders', 'ReminderController');
Route::resource('taskstatistics', 'TaskStatisticsController');
Route::resource('sendstock', 'SendStockController');
Route::resource('contractors', 'ContractorController');
Route::resource('status_invoice', 'StatusInvoiceController');
Route::resource('invoice_payment', 'InvoicePaymentMethodController');
Route::resource('invoice', 'InvoiceController');
Route::resource('invoice_detail', 'InvoiceDetailController');
Route::get('/invoice/getPDF/{id}','InvoiceController@generatePDF');
Route::get('/invoice/dodaci-list/{id}','InvoiceController@generatePDFdodaci');
// Route::get('/invoice/print/{id}','InvoiceController@print');
Route::resource('messages', 'MessageController');
Route::resource('price_offers', 'PriceOfferController');
Route::get('/price_offers/getPDF/{id}','PriceOfferController@generatePDF');
Route::resource('users', 'UserController');
Route::resource('statisticsinvoice', 'StatisticsInvoicesController');
Route::resource('priceoffer_detail', 'PriceOfferDetailController');
Route::resource('client_export', 'ClientExportController');






