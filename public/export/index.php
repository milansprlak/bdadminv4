<?php
/*
Author: Javed Ur Rehman
Website: http://www.allphptricks.com/
*/

require('db.php');
include("auth.php"); //include auth.php file on all secure pages ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bedrich, s.r.o.</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">BEDRICH spol. s r.o.</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Domov</a></li>
            <li><a href="http://bedrich.sk/">Kontakt</a></li>
            <li><a href="logout.php">Odhlásiť sa</a></li>
        </ul>
    </div>
</nav>
<div class="container">

    <h2>Produkty na sklade</h2>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Názov produktu</th>
            <th>Počet ks na sklade</th>
            <th>Cena bez DPH</th>
        </tr>
        </thead>
        <tbody>
        <?php
        // Nastavenie db na utf-8
        mysqli_set_charset($con,"utf8");
        // Zisti podla prihlasenia klienta
        $username = $_SESSION["username"];

        $sql0= "SELECT client_id FROM client_exports WHERE username='$username'";
        $data= $con->query($sql0);

        $client = mysqli_fetch_assoc($data);
        $client_id = $client["client_id"];


        $sql = "SELECT id, nazov_produktu, pocet_ks, cena__bez_dph FROM stocks WHERE client_id ='$client_id' and ID !='1'";
        $result = $con->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc())
            {
                echo"<tr>";
                echo"<td>".$row["nazov_produktu"]."</td>";
                echo"<td>".$row["pocet_ks"]."</td>";
                echo"<td>".$row["cena__bez_dph"]." € </td>";
                echo"</tr>";
            }
        } else {
            echo "Žiadne produkty na sklade";}
        $con->close();
        ?>


        </tbody>
    </table>
</div>




</body>
</html>
