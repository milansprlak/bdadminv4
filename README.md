# bADMIN

IS vyvyjany pre spolocnost Bedrich, s.r.o.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Local php server
Local Mysql server
Composer
GIT

### Installing

### 1. Cloning z bitbucketu

```
https://aureli@bitbucket.org/vvojovteamaurelo/badmin.git
```

### 2. Composer Update

V zlozke kde sa projekt naklonoval treba updatovat composer
```
$ composer update
```

### 3. php composer.phar install
Iba ak krok c.2 nefunguje
Stiahnut a vlozit do rootu app subor: composer.phar https://getcomposer.org/doc/01-basic-usage.md

```
php composer.phar install
```
### 4. Nastavenie spojenia MYSQL

Otvorit a editovat .env.example

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=your_username
DB_PASSWORD=your_password
```
Ulozit ako: .env

### 5. Nastavenie kluca 
V zlozke kde sa projekt naklonoval treba vygenerovat novy key. 
```
$ php artisan key:generate
```

### 4. Nakrmenie a vytvorenie MYSQL  table 
```
php artisan migrate:refresh --seed
```
### 5. Spustenie servera
```
php artisan serve
```
## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [LARAVEL](https://laravel.com) - The web framework used
* [AdminLTE](https://adminlte.io) - Dashboard

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [GIT](https://bitbucket.org) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/vvojovteamaurelo/badmin). 

## Authors

* **Aurel Sprlak** - *CEO, programmer* - [Aurelo, s.r.o](https://aurelo.sk)
* **Milan Sprlak** - *CEO, programmer* - [Aurelo, s.r.o](https://aurelo.sk)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
